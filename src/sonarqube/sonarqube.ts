import axios, { AxiosRequestConfig } from "axios";
import { readFile, writeFile } from 'fs/promises';
import { Action, Example, SystemInfo, WebService } from "./api";
export class SonarQube {
  private constructor (
    private axiosDefaultConfig: AxiosRequestConfig,
    private cache: string | null
  ) { }

  public static newFromToken (url: string, token: string, cache: string | null = null) {
    return new SonarQube(SonarQube.axiosConfig(url, token, ''), cache);
  }

  public static newFromPassword (url: string, username: string, password: string, cache: string | null = null) {
    return new SonarQube(SonarQube.axiosConfig(url, username, password), cache);
  }

  public static axiosConfig (url: string, username: string, password: string): AxiosRequestConfig {
    return {
      baseURL: url,
      auth: { username, password },
      headers: { Accept: 'application/json' }
    }
  }

  public async call<T> (filename: string, fetcher: () => Promise<T>): Promise<T> {
    const cacheFunc = async (): Promise<T> => {
      const filepath = `${this.cache}/${filename}.json`;
      try {
        const content = await readFile(filepath);
        return JSON.parse(content.toString()) as T;
      } catch (e) {
        const data = await fetcher();
        await writeFile(filepath, JSON.stringify(data));
        return data;
      }
    }

    if (this.cache === null) {
      return await fetcher();
    }
    return await cacheFunc();
  }

  public async getSystemInfo (): Promise<SystemInfo> {
    return await this.call<SystemInfo>('system-info', async () => {
      const { data } = await axios.get('/api/system/info', this.axiosDefaultConfig);
      return data;
    });
  }

  public async getWebServices (internal = false): Promise<WebService[]> {
    return await this.call<WebService[]>('webservices-list', async () => {
      const { data } = await axios.get('/api/webservices/list', {
        ...this.axiosDefaultConfig,
        params: {
          include_internals: internal ? 'yes' : 'no'
        }
      });
      return data.webServices;
    });
  }

  public async getExample (webservice: WebService, action: Action): Promise<Example | null> {
    if (!action.hasResponseExample) {
      return null;
    }
    return await this.call<Example>(`example-${webservice.path.replace('api/', '')}-${action.key}`, async () => {
      const { data } = await axios.get('/api/webservices/response_example', {
        ...this.axiosDefaultConfig,
        params: {
          controller: webservice.path,
          action: action.key
        }
      });
      return data;
    });
  }
}
