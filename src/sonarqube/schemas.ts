import { OpenAPIV3 as OpenAPI } from "openapi-types";

export const schemas: Record<string, OpenAPI.SchemaObject> = {
  'DuplicationBlock': {
    type: 'object',
    properties: {
      'from': {
        type: 'integer',
      },
      'size': {
        type: 'integer',
      },
      '_ref': {
        type: 'string',
      },
    },
  },
  'DuplicationFile': {
    type: 'object',
    properties: {
      key: {
        type: 'string',
      },
      name: {
        type: 'string',
      },
      projectName: {
        type: 'string',
      },
    },
  }
};
