export const contentTypes: Record<string, string> = {
  'json': 'application/json',
  'log': 'text/plain',
  'proto': 'application/x-protobuf',
  'svg': 'image/svg+xml',
  'txt': 'text/plain',
  'xml': 'application/xml',
};
