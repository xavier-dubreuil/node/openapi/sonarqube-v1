import { OpenAPIV3 } from 'openapi-types';
import { MappingType, SchemaMapping } from '@openapi/container';

function name (path: string, name: Names): Record<string, MappingType> {
  return {
    [path]: {
      name
    }
  };
}

function paging (): Record<string, MappingType> {
  return {
    '/paging': {
      name: 'Paging',
      schema: {
        type: 'object',
        properties: {
          pageIndex: {
            type: 'integer'
          },
          pageSize: {
            type: 'integer'
          },
          total: {
            type: 'integer'
          }
        }
      }
    }
  };
}

function schema (type: OpenAPIV3.NonArraySchemaObjectType): OpenAPIV3.SchemaObject {
  return { type };
}

function ref (type: Names): OpenAPIV3.ReferenceObject {
  return {
    $ref: `#/components/schemas/${type}`
  };
}

function obj (path: string, type: OpenAPIV3.SchemaObject | OpenAPIV3.ReferenceObject): Record<string, MappingType> {
  return {
    [path]: {
      schema: {
        type: 'object',
        additionalProperties: true,
        allOf: [type]
      }
    }
  };
}

function duplicationFiles (path: string): Record<string, MappingType> {
  return {
    [path]: {
      schema: {
        type: 'object',
        additionalProperties: true,
        allOf: [{
          $ref: '#/components/schemas/DuplicationFile'
        }]
      }
    }
  }
}

enum Names {
  action = 'Action',
  activeRule = 'ActiveRule',
  activeRuleParam = 'ActiveRuleParam',
  almSetting = 'AlmSetting',
  almSettingGithub = 'AlmSettingGithub',
  analysis = 'Analysis',
  application = 'Application',
  auditLog = 'AuditLog',
  auditLogValue = 'AuditLogValue',
  azureProject = 'AzureProject',
  azureRepostiory = 'AzureRepostiory',
  bitbucketCloudRepository = 'BitbucketCloudRepository',
  bitbucketServerProject = 'BitbucketServerProject',
  bitbucketServerRepository = 'BitbucketServerRepository',
  branch = 'Branch',
  branchStatus = 'BranchStatus',
  cause = 'Cause',
  changelogDiff = 'ChangelogDiff',
  component = 'Component',
  componentPeriod = 'ComponentPeriod',
  condition = 'Condition',
  context = 'Context',
  definitionField = 'DefinitionField',
  definitions = 'Definitions',
  delivery = 'Delivery',
  duplicationBlock = 'DuplicationBlock',
  duplicationBlocks = 'DuplicationBlocks',
  duplicationFile = 'DuplicationFile',
  error = 'Error',
  event = 'Event',
  eventQualityGate = 'EventQualityGate',
  eventQualityGateFail = 'EventQualityGateFail',
  facet = 'Facet',
  facetValue = 'FacetValue',
  finding = 'Finding',
  flow = 'Flow',
  gitlabRepository = 'GitlabRepository',
  hotspot = 'Hotspot',
  hotspotChangelog = 'HotspotChangelog',
  hotspotComment = 'HotspotComment',
  impact = 'Impact',
  issue = 'Issue',
  issueChangelog = 'IssueChangelog',
  issueComent = 'IssueComent',
  issueFacet = 'IssueFacet',
  language = 'Language',
  licenceUsage = 'LicenceUsage',
  link = 'Link',
  location = 'Location',
  measure = 'Measure',
  measureHistory = 'MeasureHistory',
  measurePeriod = 'MeasurePeriod',
  messageFormatting = 'MessageFormatting',
  metric = 'Metric',
  newCodePeriod = 'NewCodePeriod',
  node = 'Node',
  notification = 'Notification',
  paging = 'Paging',
  period = 'Period',
  plugin = 'Plugin',
  pluginRelease = 'PluginRelease',
  pluginRequirement = 'PluginRequirement',
  pluginUpdate = 'PluginUpdate',
  portfolio = 'Portfolio',
  project = 'Project',
  projectStatus = 'ProjectStatus',
  pullRequest = 'PullRequest',
  pullRequestStatus = 'PullRequestStatus',
  qualityGate = 'QualityGate',
  qualityProfile = 'QualityProfile',
  qualityProfileEvent = 'QualityProfileEvent',
  qualityProfileFormat = 'QualityProfileFormat',
  rule = 'Rule',
  ruleDescriptionSection = 'RuleDescriptionSection',
  ruleParam = 'RuleParam',
  ruleRepository = 'RuleRepository',
  task = 'Task',
  template = 'Template',
  templatePermission = 'TemplatePermission',
  textRange = 'TextRange',
  upgradePlugins = 'UpgradePlugins',
  user = 'User',
  userGroup = 'UserGroup',
  userToken = 'UserToken',
  version = 'Version',
  view = 'View',
  webhook = 'Webhook',
  webservice = 'Webservice',
  webserviceAction = 'WebserviceAction',
  webserviceActionChangelog = 'WebserviceActionChangelog',
  webserviceActionParams = 'WebserviceActionParams',
}

export const mapping: SchemaMapping = {
  'global': { ...paging() },
  '/api/alm_integrations/list_azure_projects': { ...name('/projects', Names.azureProject) },
  '/api/alm_integrations/list_bitbucketserver_projects': { ...name('/projects', Names.bitbucketServerProject) },
  '/api/alm_integrations/search_azure_repos': { ...name('/repositories', Names.azureRepostiory) },
  '/api/alm_integrations/search_bitbucketcloud_repos': { ...name('/repositories', Names.bitbucketCloudRepository) },
  '/api/alm_integrations/search_bitbucketserver_repos': { ...name('/repositories', Names.bitbucketServerRepository) },
  '/api/alm_integrations/search_gitlab_repos': { ...name('/repositories', Names.gitlabRepository) },
  '/api/alm_settings/list': { ...name('/almSettings', Names.almSetting) },
  '/api/alm_settings/list_definitions': { ...name('/github', Names.almSettingGithub) },
  '/api/alm_settings/validate': { ...name('/errors', Names.error) },
  '/api/applications/create': { ...name('/application', Names.application) },
  '/api/applications/show': { ...name('/application', Names.application), ...name('/application/branches', Names.branch), ...name('/application/projects', Names.project) },
  '/api/audit_logs/download': { ...name('/audit_logs', Names.auditLog), ...name('/audit_logs/newValue', Names.auditLogValue), ...name('/audit_logs/previousValue', Names.auditLogValue) },
  '/api/ce/activity': { ...name('/tasks', Names.task) },
  '/api/ce/component': { ...name('/current', Names.task), ...name('/queue', Names.task) },
  '/api/ce/task': { ...name('/task', Names.task) },
  '/api/components/search': { ...name('/components', Names.component) },
  '/api/components/show': { ...name('/ancestors', Names.component), ...name('/component', Names.component) },
  '/api/components/tree': { ...name('/baseComponent', Names.component), ...name('/components', Names.component) },
  '/api/duplications/show': { ...name('/duplications', Names.duplicationBlocks), ...duplicationFiles('/files'), ...name('/duplications/blocks', Names.duplicationBlock), ...name('/duplications/files', Names.duplicationFile) },
  '/api/favorites/search': { ...name('/favorites', Names.component) },
  '/api/hotspots/search': { ...name('/components', Names.component), ...name('/hotspots', Names.hotspot) },
  '/api/hotspots/show': { ...name('/changelog', Names.hotspotChangelog), ...name('/changelog/diffs', Names.changelogDiff), ...name('/comment', Names.hotspotComment), ...name('/component', Names.component), ...name('/messageFormattings', Names.messageFormatting), ...name('/project', Names.project), ...name('/rule', Names.rule), ...name('/users', Names.user) },
  '/api/issues/add_comment': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/issues/assign': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/issues/changelog': { ...name('/changelog', Names.issueChangelog), ...name('/changelog/diffs', Names.changelogDiff) },
  '/api/issues/delete_comment': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/issues/do_transition': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/issues/edit_comment': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/issues/search': { ...name('/components', Names.component), ...name('/issues', Names.issue), ...obj('/issues/attr', schema('string')), ...name('/issues/comments', Names.issueComent), ...name('/issues/flows', Names.flow), ...name('/issues/flows/locations', Names.location), ...name('/issues/flows/locations/msgFormattings', Names.messageFormatting), ...name('/issues/flows/locations/textRange', Names.textRange), ...name('/issues/messageFormattings', Names.messageFormatting), ...name('/issues/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issues/impacts', Names.impact), ...name('/facets', Names.issueFacet), ...name('/facets/values', Names.facetValue) },
  '/api/issues/set_severity': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/issues/set_tags': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/issues/set_type': { ...name('/components', Names.component), ...name('/issue', Names.issue), ...name('/issue/comments', Names.issueComent), ...name('/issue/textRange', Names.textRange), ...name('/rules', Names.rule), ...name('/users', Names.user), ...name('/issue/impacts', Names.impact) },
  '/api/languages/list': { ...name('/languages', Names.language) },
  '/api/measures/component': { ...name('/component', Names.component), ...name('/component/measures', Names.measure), ...name('/metrics', Names.metric), ...name('/period', Names.componentPeriod), ...name('/component/measures/period', Names.measurePeriod) },
  '/api/measures/component_tree': { ...name('/baseComponent', Names.component), ...name('/baseComponent/measures', Names.measure), ...name('/baseComponent/measures/period', Names.measurePeriod), ...name('/components', Names.component), ...name('/components/measures', Names.measure), ...name('/components/measures/period', Names.measurePeriod), ...name('/metrics', Names.metric), ...name('/period', Names.componentPeriod) },
  '/api/measures/search_history': { ...name('/measures', Names.measure), ...name('/measures/history', Names.measureHistory) },
  '/api/metrics/search': { ...name('/metrics', Names.metric) },
  '/api/new_code_periods/list': { ...name('/newCodePeriods', Names.newCodePeriod) },
  '/api/notifications/list': { ...name('/notifications', Names.notification) },
  '/api/permissions/create_template': { ...name('/permissionTemplate', Names.template) },
  '/api/permissions/search_templates': { ...name('/defaultTemplates', Names.template), ...name('/permissionTemplates', Names.template), ...name('/permissionTemplates/permissions', Names.templatePermission) },
  '/api/permissions/update_template': { ...name('/permissionTemplate', Names.template) },
  '/api/plugins/available': { ...name('/plugins', Names.plugin), ...name('/plugins/release', Names.pluginRelease), ...name('/plugins/update', Names.pluginUpdate), ...name('/plugins/update/requires', Names.pluginRequirement) },
  '/api/plugins/installed': { ...name('/plugins', Names.plugin) },
  '/api/plugins/pending': { ...name('/installing', Names.plugin), ...name('/removing', Names.plugin), ...name('/updating', Names.plugin) },
  '/api/plugins/updates': { ...name('/plugins', Names.plugin), ...name('/plugins/updates', Names.pluginUpdate), ...name('/plugins/updates/release', Names.pluginRelease), ...name('/plugins/updates/requires', Names.pluginRequirement) },
  '/api/project_analyses/create_event': { ...name('/event', Names.event) },
  '/api/project_analyses/search': { ...name('/analyses', Names.analysis), ...name('/analyses/events', Names.event), ...name('/analyses/events/qualityProfile', Names.qualityProfile), ...name('/analyses/events/qualityGate', Names.eventQualityGate), ...name('/analyses/events/qualityGate/failing', Names.eventQualityGateFail) },
  '/api/project_analyses/update_event': { ...name('/event', Names.event) },
  '/api/project_branches/list': { ...name('/branches', Names.branch), ...name('/branches/status', Names.branchStatus) },
  '/api/project_links/create': { ...name('/link', Names.link) },
  '/api/project_links/search': { ...name('/links', Names.link) },
  '/api/project_pull_requests/list': { ...name('/pullRequests', Names.pullRequest), ...name('/pullRequests/status', Names.pullRequestStatus) },
  '/api/projects/create': { ...name('/project', Names.project) },
  '/api/projects/export_findings': { ...name('/export_findings', Names.finding) },
  '/api/projects/license_usage': { ...name('/projects', Names.licenceUsage) },
  '/api/projects/search': { ...name('/components', Names.component) },
  '/api/qualitygates/get_by_project': { ...name('/qualityGate', Names.qualityGate) },
  '/api/qualitygates/list': { ...name('/actions', Names.action), ...name('/qualitygates', Names.qualityGate), ...name('/qualitygates/actions', Names.action) },
  '/api/qualitygates/project_status': { ...name('/projectStatus', Names.projectStatus), ...name('/projectStatus/conditions', Names.condition), ...name('/projectStatus/period', Names.period), ...name('/projectStatus/periods', Names.period) },
  '/api/qualitygates/search': { ...name('/results', Names.project) },
  '/api/qualitygates/show': { ...name('/actions', Names.action), ...name('/conditions', Names.condition) },
  '/api/qualityprofiles/changelog': { ...name('/events', Names.qualityProfileEvent), ...obj('/events/params', schema('string')), ...name('/events/impacts', Names.impact) },
  '/api/qualityprofiles/create': { ...name('/profile', Names.qualityProfile) },
  '/api/qualityprofiles/exporters': { ...name('/exporters', Names.qualityProfileFormat) },
  '/api/qualityprofiles/importers': { ...name('/importers', Names.qualityProfileFormat) },
  '/api/qualityprofiles/inheritance': { ...name('/ancestors', Names.qualityGate), ...name('/children', Names.qualityGate), ...name('/profile', Names.qualityProfile) },
  '/api/qualityprofiles/projects': { ...name('/results', Names.project) },
  '/api/qualityprofiles/search': { ...name('/actions', Names.action), ...name('/profiles', Names.qualityProfile), ...name('/profiles/actions', Names.action) },
  '/api/rules/create': { ...name('/rule', Names.rule), ...name('/rule/params', Names.ruleParam), ...name('/rule/impacts', Names.impact) },
  '/api/rules/repositories': { ...name('/repositories', Names.ruleRepository) },
  '/api/rules/search': {...obj('/actives', ref(Names.activeRule)), ...name('/facets', Names.facet), ...name('/facets/values', Names.facetValue), ...name('/rules', Names.rule), ...name('/rules/descriptionSections', Names.ruleDescriptionSection), ...name('/rules/params', Names.ruleParam), ...name('/rules/impacts', Names.impact), ...name('/rules/descriptionSections/context', Names.context)  },
  '/api/rules/show': { ...name('/actives', Names.activeRule), ...name('/actives/params', Names.activeRuleParam), ...name('/rule', Names.rule), ...name('/rule/descriptionSections', Names.ruleDescriptionSection), ...name('/rule/params', Names.ruleParam), ...name('/rule/impacts', Names.impact), ...name('/rule/descriptionSections/context', Names.context) },
  '/api/rules/update': { ...name('/rule', Names.rule), ...name('/rule/params', Names.ruleParam), ...name('/rule/impacts', Names.impact) },
  '/api/settings/list_definitions': { ...name('/definitions', Names.definitions), ...name('/definitions/fields', Names.definitionField) },
  '/api/settings/values': {
    '/settings': {
      'schema': {
        'type': 'object',
        'additionalProperties': true
      }
    }
  },
  '/api/system/health': { ...name('/causes', Names.cause), ...name('/nodes', Names.node), ...name('/nodes/causes', Names.cause) },
  '/api/system/info': { ...obj('/ALMs', schema('string')), ...obj('/Bundled', schema('string')), ...obj('/Compute Engine Database Connection', schema('string')), ...obj('/Compute Engine JVM Properties', schema('string')), ...obj('/Compute Engine JVM State', schema('string')), ...obj('/Compute Engine Logging', schema('string')), ...obj('/Compute Engine Tasks', schema('string')), ...obj('/Database', schema('string')), ...obj('/Plugins', schema('string')), ...obj('/Search Indexes', schema('string')), ...obj('/Search State', schema('string')), ...obj('/Server Push Connections', schema('string')), ...obj('/Settings', schema('string')), ...obj('/System', schema('string')), ...obj('/Web Database Connection', schema('string')), ...obj('/Web JVM Properties', schema('string')), ...obj('/Web JVM State', schema('string')), ...obj('/Web Logging', schema('string')) },
  '/api/system/upgrades': { ...name('/upgrades/plugins', Names.upgradePlugins), ...name('/upgrades/plugins/incompatible', Names.plugin), ...name('/upgrades/plugins/requireUpdate', Names.plugin), ...name('/upgrades', Names.version) },
  '/api/user_groups/create': { ...name('/group', Names.userGroup) },
  '/api/user_groups/search': { ...name('/groups', Names.userGroup) },
  '/api/user_groups/users': { ...name('/users', Names.user) },
  '/api/user_tokens/search': { ...name('/userTokens', Names.userToken), ...name('/userTokens/project', Names.project) },
  '/api/users/create': { ...name('/user', Names.user) },
  '/api/users/deactivate': { ...name('/user', Names.user) },
  '/api/users/groups': { ...name('/groups', Names.userGroup) },
  '/api/users/search': { ...name('/users', Names.user) },
  '/api/users/update': { ...name('/user', Names.user) },
  '/api/views/applications': { ...name('/applications', Names.application) },
  '/api/views/list': { ...name('/views', Names.view) },
  '/api/views/local_views': { ...name('/views', Names.view) },
  '/api/views/move_options': { ...name('/views', Names.view) },
  '/api/views/portfolios': { ...name('/portfolios', Names.portfolio) },
  '/api/webhooks/create': { ...name('/webhook', Names.webhook) },
  '/api/webhooks/deliveries': { ...name('/deliveries', Names.delivery) },
  '/api/webhooks/delivery': { ...name('/delivery', Names.delivery) },
  '/api/webhooks/list': { ...name('/webhooks', Names.webhook) },
  '/api/webservices/list': { ...name('/webServices', Names.webservice), ...name('/webServices/actions', Names.webserviceAction), ...name('/webServices/actions/changelog', Names.webserviceActionChangelog), ...name('/webServices/actions/params', Names.webserviceActionParams) },
  '/api/webservices/response_example': {
    '/example': {
      'schema': {
        'type': 'string'
      }
    }, ...name('/example/issues/textRange', Names.textRange)
  },
  '/api/qualitygates/search_groups': { ...name('/groups', Names.userGroup) },
  '/api/qualitygates/search_users': { ...name('/users', Names.user) }
}