export interface SystemInfo {
  System: {
    Version: string
  }
}

export interface Parameter {
  key: string
  required: boolean
  internal: boolean
  description?: string
  defaultValue?: string
  exampleValue?: string
  since?: string
  deprecatedSince?: string
  deprecatedKey?: string
  deprecatedKeySince?: string
  possibleValues?: string[]
}

export interface Action {
  key: string
  since: string
  internal: boolean
  post: boolean
  hasResponseExample: boolean
  description?: string
  deprecatedSince?: string
  params?: Parameter[]
}

export interface WebService {
  path: string
  since: string
  description: string
  actions: Action[]
}

export interface Example {
  format: string,
  example: string
}
