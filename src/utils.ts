export function toCamel (s: string): string {
  return s.toLowerCase().replace(/([-_][a-z0-9])/ig, ($1) => {
    return $1.toUpperCase()
      .replace('-', '')
      .replace('_', '');
  });
}
