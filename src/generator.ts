import { OpenAPIV3 as OpenAPI, OpenAPIV3 } from 'openapi-types';
import { Generator } from '@openapi/container';
import { schemas } from './sonarqube/schemas';
import { SonarQube } from './sonarqube/sonarqube';
import { Action, Parameter, WebService } from './sonarqube/api';
import { toCamel } from './utils';
import { contentTypes } from './sonarqube/content-types';
import { mapping } from './sonarqube/mapping';
import { writeFile } from 'fs/promises';
import { dirname } from 'path';
import { exit } from 'process';

export class SonarQubeGenerator {
  private openapi: Generator;

  private constructor (private sonarqube: SonarQube) {
    this.openapi = new Generator();
  }

  private async initialize (): Promise<void> {
    this.openapi.setMapping(mapping);
    const systemInfo = await this.sonarqube.getSystemInfo();

    this.openapi.setInfo({
      title: 'SonarQube',
      version: systemInfo.System.Version.replace(/^(\d*\.\d*)\..*/, '$1'),
    });
  }

  public static async new (sonarqube: SonarQube): Promise<SonarQubeGenerator> {
    const instance = new SonarQubeGenerator(sonarqube);
    await instance.initialize();
    return instance;
  }

  public getParameterSchema (parameter: Parameter): OpenAPI.SchemaObject {
    const paramType = parameter.exampleValue !== undefined && !isNaN(Number(parameter.exampleValue.toString())) ? 'number' : 'string';
    const schema: OpenAPI.SchemaObject = {
      type: paramType,
    };
    if (parameter.defaultValue !== undefined) {
      schema.default = parameter.defaultValue;
    }
    if (parameter.possibleValues !== undefined) {
      schema.enum = parameter.possibleValues!;
    }
    if ((parameter.description ?? '').startsWith('Comma-separated list')) {
      return {
        type: 'array',
        items: schema,
      };
    }
    return schema;
  }

  public generateParameter (param: Parameter): OpenAPI.ParameterObject {
    const parameter: OpenAPI.ParameterObject = {
      in: 'query',
      name: param.key,
      schema: this.getParameterSchema(param),
    };

    if (param.deprecatedSince !== undefined) {
      parameter.deprecated = true;
    }
    if (param.description !== undefined) {
      parameter.description = param.description;
    }
    if (param.required) {
      parameter.required = param.required
    }

    return parameter;
  }

  protected generateRequestBody (operationId: string, action: Action): OpenAPI.RequestBodyObject {
    const properties: Record<string, OpenAPI.SchemaObject> = {};
    const required: string[] = [];

    for (const parameter of action.params ?? []) {
      properties[action.key] = this.getParameterSchema(parameter);
      if (parameter.required) {
        required.push(action.key);
      }
    }

    return {
      content: {
        'application/json': {
          schema: this.openapi.addComponentSchema(`${operationId}Body`, { type: 'object', properties, required }),
        },
      },
      required: true,
    }
  }

  protected async generateResponses (webservice: WebService, action: Action, operationId: string): Promise<OpenAPI.ResponsesObject> {
    const example = await this.sonarqube.getExample(webservice, action);
    const responseType: OpenAPI.MediaTypeObject = {}
    if (example === null) {
      return { '204': { description: '' } };
    }
    if (example.example.length > 0) {
      responseType.example = example.example
    }
    if (example.format === 'json') {
      try {
        const schema = this.openapi.generateSchema(JSON.parse(example.example), `/${webservice.path}/${action.key}`, ['']) as OpenAPI.SchemaObject;
        responseType.schema = this.openapi.addComponentSchema(`${operationId}Response`, schema);
      } catch (e) {
        /* istanbul ignore next */
        console.log(`Cannot generate schema for: ${webservice.path}/${action.key}`)
      }
    }
    return {
      '200': {
        description: '',
        content: {
          [contentTypes[example.format]]: responseType,
        }
      }
    }
  }


  public generateParameters (action: Action): OpenAPI.ParameterObject[] {
    const parameters: OpenAPI.ParameterObject[] = [];
    for (const param of action.params ?? []) {
      parameters.push(this.generateParameter(param));
    }
    return parameters;
  }

  public async generateOperation (webservice: WebService, action: Action): Promise<OpenAPI.OperationObject> {
    const actionName = action.key.replace('_', '-');
    const operationId = toCamel(`${action.post ? 'post' : 'get'}_${webservice.path.replace(/^api\//, '')}_${actionName}`);

    const operation: OpenAPI.OperationObject = {
      operationId,
      responses: {}
    };

    if (action.deprecatedSince !== undefined) {
      operation['deprecated'] = true;
    }
    if (action.description !== undefined) {
      operation['description'] = action.description;
    }
    if (action.post) {
      operation.requestBody = this.generateRequestBody(operationId, action);
    } else {
      operation.parameters = this.generateParameters(action);
    }
    operation.responses = await this.generateResponses(webservice, action, operationId);

    return operation;
  }

  public async generate (): Promise<OpenAPI.Document> {
    for (const [name, schema] of Object.entries(schemas)) {
      this.openapi.addSchema(name, schema);
    }

    const webServices = await this.sonarqube.getWebServices();
    for (const webservice of webServices) {
      for (const action of webservice.actions) {
        const endpoint = `/${webservice.path}/${action.key}`;
        const method = action.post ? OpenAPI.HttpMethods.POST : OpenAPI.HttpMethods.GET;
        this.openapi.addOperation(endpoint, method, await this.generateOperation(webservice, action))
      }
    }
    return this.openapi.getOpenApi();
  }

  public getOpenApi (): OpenAPIV3.Document {
    return this.openapi.getOpenApi();
  }
}

/* istanbul ignore if */
if (require.main === module) {
  const rootFolder = dirname(__dirname);
  const main = async (): Promise<void> => {
    const version = process.argv[2].replace('.', '-');
    const sonarqube = SonarQube.newFromPassword(`http://localhost:9000`, 'admin', 'admin', `${rootFolder}/cache/${version}`);
    const generator = await SonarQubeGenerator.new(sonarqube);
    const openapi = await generator.generate();
    await writeFile(`${rootFolder}/assets/openapi-${version}.json`, JSON.stringify(openapi));
  }
  main()
    .then()
    .catch(e => {
      console.log(e.message);
      exit(1);
    });
}