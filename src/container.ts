import { Container as OpenAPIContainer, OpenApiClientDefault } from '@openapi/container';
import { OpenAPIV3 } from 'openapi-client-axios';
import axios, { AxiosRequestConfig, AxiosResponse, CustomParamsSerializer } from 'axios';
import { dirname } from 'path';
import Qs from 'qs';

export const paramsSerializer: CustomParamsSerializer = (params) => Qs.stringify(params, { arrayFormat: 'comma' });

export async function getDefinition (version: string): Promise<OpenAPIV3.Document> {
  const assetPath = `${dirname(__dirname)}/assets/openapi-${version.replace('.', '-')}.json`;
  return OpenAPIContainer.readDefinition(assetPath);
}

export function axiosDefaults (domain: string, token: string): AxiosRequestConfig {
  return {
    baseURL: `${domain}`,
    auth: { username: token, password: '' },
    paramsSerializer,
  } as AxiosRequestConfig;
}

export class AbstractContainer<O extends OpenApiClientDefault> extends OpenAPIContainer<O> {
  public static async abstractNew<O extends OpenApiClientDefault> (
    assetPath: string,
    domain: string,
    token: string
  ): Promise<AbstractContainer<O>> {
    return await super.instantiate<AbstractContainer<O>, O>(
      AbstractContainer<O>,
      await getDefinition(assetPath),
      axiosDefaults(domain, token),
    );
  }

  public call<T> (axiosRequestConfig: AxiosRequestConfig): Promise<AxiosResponse<T>> {
    return axios.request({ ...this.axiosRequestConfig, ...axiosRequestConfig });
  }
}
