import { AbstractContainer } from '../../container';
import { Client } from './openapi.d';

export class Container extends AbstractContainer<Client> {
  public static async new (domain: string, token: string): Promise<Container> {
    return super.abstractNew('9.9', domain, token);
  }
}
