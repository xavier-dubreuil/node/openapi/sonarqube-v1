import type {
  OpenAPIClient,
  Parameters,
  UnknownParamsObject,
  OperationResponse,
  AxiosRequestConfig,
} from 'openapi-client-axios';

export declare namespace Components {
    namespace Schemas {
        export interface Action {
            create?: string;
        }
        export interface ActiveRule {
            qProfile?: string;
            inherit?: string;
            severity?: string;
            params?: ActiveRuleParam[];
        }
        export interface ActiveRuleParam {
            key?: string;
            value?: string;
        }
        export interface AlmSetting {
            key?: string;
            alm?: string;
            url?: string;
        }
        export interface AlmSettingGithub {
            key?: string;
            url?: string;
            appId?: string;
            clientId?: string;
        }
        export interface Analysis {
            key?: string;
            date?: string;
            projectVersion?: string;
            buildString?: string;
            manualNewCodePeriodBaseline?: string;
            detectedCI?: string;
            events?: Event[];
        }
        export interface AzureProject {
            name?: string;
            description?: string;
        }
        export interface AzureRepostiory {
            name?: string;
            projectName?: string;
        }
        export interface BitbucketCloudRepository {
            slug?: string;
            uuid?: string;
            name?: string;
            sqProjectKey?: string;
            projectKey?: string;
            workspace?: string;
        }
        export interface BitbucketServerProject {
            key?: string;
            name?: string;
        }
        export interface BitbucketServerRepository {
            slug?: string;
            uuid?: string;
            name?: string;
            sqProjectKey?: string;
            projectKey?: string;
            workspace?: string;
        }
        export interface Branch {
            name?: string;
            isMain?: string;
            type?: string;
            status?: BranchStatus;
            analysisDate?: string;
            excludedFromPurge?: string;
        }
        export interface BranchStatus {
            qualityGateStatus?: string;
        }
        export interface Cause {
            message?: string;
        }
        export interface ChangelogDiff {
            key?: string;
            newValue?: string;
        }
        export interface Component {
            key?: string;
            name?: string;
            qualifier?: string;
            visibility?: string;
            lastAnalysisDate?: string;
            revision?: string;
            managed?: string;
        }
        export interface ComponentPeriod {
            mode?: string;
            date?: string;
            parameter?: string;
        }
        export interface Condition {
            id?: string;
            metric?: string;
            op?: string;
            error?: string;
        }
        export interface Context {
            displayName?: string;
            key?: string;
        }
        export interface DefinitionField {
            key?: string;
            name?: string;
            description?: string;
            type?: string;
            options?: string[];
        }
        export interface Definitions {
            key?: string;
            name?: string;
            description?: string;
            type?: string;
            category?: string;
            subCategory?: string;
            multiValues?: string;
            options?: any[];
            fields?: DefinitionField[];
        }
        export interface Delivery {
            id?: string;
            componentKey?: string;
            ceTaskId?: string;
            name?: string;
            url?: string;
            at?: string;
            success?: string;
            httpStatus?: number;
            durationMs?: number;
            payload?: string;
        }
        export interface DuplicationBlock {
            from?: number;
            size?: number;
            _ref?: string;
        }
        export interface DuplicationBlocks {
            blocks?: DuplicationBlock[];
        }
        export interface DuplicationFile {
            key?: string;
            name?: string;
            projectName?: string;
        }
        export interface Error {
            msg?: string;
        }
        export interface Event {
            analysis?: string;
            key?: string;
            category?: string;
            name?: string;
        }
        export interface EventQualityGate {
            status?: string;
            stillFailing?: string;
            failing?: EventQualityGateFail[];
        }
        export interface EventQualityGateFail {
            key?: string;
            name?: string;
            branch?: string;
        }
        export interface Facet {
            name?: string;
            values?: FacetValue[];
        }
        export interface FacetValue {
            val?: string;
            count?: number;
        }
        export interface Flow {
            locations?: Location[];
        }
        export interface GetAlmIntegrationsListAzureProjectsResponse {
            projects?: AzureProject[];
        }
        export interface GetAlmIntegrationsListBitbucketserverProjectsResponse {
            projects?: BitbucketServerProject[];
        }
        export interface GetAlmIntegrationsSearchAzureReposResponse {
            repositories?: AzureRepostiory[];
        }
        export interface GetAlmIntegrationsSearchBitbucketcloudReposResponse {
            paging?: Paging;
            isLastPage?: string;
            repositories?: BitbucketCloudRepository[];
        }
        export interface GetAlmIntegrationsSearchBitbucketserverReposResponse {
            isLastPage?: string;
            repositories?: BitbucketServerRepository[];
        }
        export interface GetAlmIntegrationsSearchGitlabReposResponse {
            paging?: Paging;
            repositories?: GitlabRepository[];
        }
        export interface GetAlmSettingsCountBindingResponse {
            key?: string;
            projects?: number;
        }
        export interface GetAlmSettingsGetBindingResponse {
            key?: string;
            alm?: string;
            repository?: string;
            url?: string;
            summaryCommentEnabled?: string;
            monorepo?: string;
        }
        export interface GetAlmSettingsListDefinitionsResponse {
            github?: AlmSettingGithub[];
            azure?: any[];
            bitbucket?: any[];
            gitlab?: any[];
            bitbucketcloud?: any[];
        }
        export interface GetAlmSettingsListResponse {
            almSettings?: AlmSetting[];
        }
        export interface GetAlmSettingsValidateResponse {
            errors?: Error[];
        }
        export interface GetAuthenticationValidateResponse {
            valid?: string;
        }
        export interface GetCeActivityResponse {
            tasks?: Task[];
            paging?: Paging;
        }
        export interface GetCeActivityStatusResponse {
            pending?: number;
            inProgress?: number;
            failing?: number;
            pendingTime?: number;
        }
        export interface GetCeComponentResponse {
            queue?: Task[];
            current?: Task;
        }
        export interface GetCeTaskResponse {
            task?: Task;
        }
        export interface GetComponentsSearchResponse {
            paging?: Paging;
            components?: Component[];
        }
        export interface GetComponentsShowResponse {
            component?: Component;
            ancestors?: Component[];
        }
        export interface GetComponentsTreeResponse {
            paging?: Paging;
            baseComponent?: Component;
            components?: Component[];
        }
        export interface GetDuplicationsShowResponse {
            duplications?: DuplicationBlocks[];
            files?: {
                [name: string]: any;
                key?: string;
                name?: string;
                projectName?: string;
            };
        }
        export interface GetFavoritesSearchResponse {
            paging?: Paging;
            favorites?: Component[];
        }
        export interface GetHotspotsSearchResponse {
            paging?: Paging;
            hotspots?: Hotspot[];
            components?: Component[];
        }
        export interface GetHotspotsShowResponse {
            key?: string;
            component?: Component;
            project?: Project;
            rule?: Rule;
            status?: string;
            line?: number;
            hash?: string;
            message?: string;
            messageFormattings?: MessageFormatting[];
            assignee?: string;
            author?: string;
            creationDate?: string;
            updateDate?: string;
            changelog?: HotspotChangelog[];
            comment?: HotspotComment[];
            users?: User[];
            canChangeStatus?: string;
            codeVariants?: string[];
        }
        export interface GetIssuesAuthorsResponse {
            authors?: string[];
        }
        export interface GetIssuesChangelogResponse {
            changelog?: IssueChangelog[];
        }
        export interface GetIssuesSearchResponse {
            paging?: Paging;
            issues?: Issue[];
            components?: Component[];
            rules?: Rule[];
            users?: User[];
            facets?: IssueFacet[];
        }
        export interface GetIssuesTagsResponse {
            tags?: string[];
        }
        export interface GetLanguagesListResponse {
            languages?: Language[];
        }
        export interface GetMeasuresComponentResponse {
            component?: Component;
            metrics?: Metric[];
            period?: ComponentPeriod;
        }
        export interface GetMeasuresComponentTreeResponse {
            paging?: Paging;
            baseComponent?: Component;
            components?: Component[];
            metrics?: Metric[];
            period?: ComponentPeriod;
        }
        export interface GetMeasuresSearchHistoryResponse {
            paging?: Paging;
            measures?: Measure[];
        }
        export interface GetMetricsSearchResponse {
            metrics?: Metric[];
            paging?: Paging;
        }
        export interface GetMetricsTypesResponse {
            types?: string[];
        }
        export interface GetNewCodePeriodsListResponse {
            newCodePeriods?: NewCodePeriod[];
        }
        export interface GetNewCodePeriodsShowResponse {
            projectKey?: string;
            branchKey?: string;
            type?: string;
            inherited?: string;
        }
        export interface GetNotificationsListResponse {
            notifications?: Notification[];
            channels?: string[];
            globalTypes?: string[];
            perProjectTypes?: string[];
        }
        export interface GetPermissionsSearchTemplatesResponse {
            permissionTemplates?: Template[];
            defaultTemplates?: Template[];
        }
        export interface GetPluginsAvailableResponse {
            plugins?: Plugin[];
            updateCenterRefresh?: string;
        }
        export interface GetPluginsInstalledResponse {
            plugins?: Plugin[];
        }
        export interface GetPluginsPendingResponse {
            installing?: Plugin[];
            updating?: Plugin[];
            removing?: Plugin[];
        }
        export interface GetPluginsUpdatesResponse {
            plugins?: Plugin[];
        }
        export interface GetProjectAnalysesSearchResponse {
            paging?: Paging;
            analyses?: Analysis[];
        }
        export interface GetProjectBadgesTokenResponse {
            token?: string;
        }
        export interface GetProjectBranchesListResponse {
            branches?: Branch[];
        }
        export interface GetProjectLinksSearchResponse {
            links?: Link[];
        }
        export interface GetProjectTagsSearchResponse {
            tags?: string[];
        }
        export interface GetProjectsSearchResponse {
            paging?: Paging;
            components?: Component[];
        }
        export interface GetQualitygatesGetByProjectResponse {
            qualityGate?: QualityGate;
        }
        export interface GetQualitygatesListResponse {
            qualitygates?: QualityGate[];
            actions?: Action;
        }
        export interface GetQualitygatesProjectStatusResponse {
            projectStatus?: ProjectStatus;
        }
        export interface GetQualitygatesSearchGroupsResponse {
            paging?: Paging;
            groups?: UserGroup[];
        }
        export interface GetQualitygatesSearchResponse {
            paging?: Paging;
            results?: Project[];
        }
        export interface GetQualitygatesSearchUsersResponse {
            paging?: Paging;
            users?: User[];
        }
        export interface GetQualitygatesShowResponse {
            name?: string;
            conditions?: Condition[];
            isBuiltIn?: string;
            isDefault?: string;
            actions?: Action;
            caycStatus?: string;
        }
        export interface GetQualityprofilesChangelogResponse {
            paging?: Paging;
            events?: QualityProfileEvent[];
        }
        export interface GetQualityprofilesExportersResponse {
            exporters?: QualityProfileFormat[];
        }
        export interface GetQualityprofilesImportersResponse {
            importers?: QualityProfileFormat[];
        }
        export interface GetQualityprofilesInheritanceResponse {
            profile?: QualityProfile;
            ancestors?: QualityGate[];
            children?: QualityGate[];
        }
        export interface GetQualityprofilesProjectsResponse {
            paging?: Paging;
            results?: Project[];
        }
        export interface GetQualityprofilesSearchResponse {
            profiles?: QualityProfile[];
            actions?: Action;
        }
        export interface GetRulesRepositoriesResponse {
            repositories?: RuleRepository[];
        }
        export interface GetRulesSearchResponse {
            paging?: Paging;
            rules?: Rule[];
            actives?: {
                [name: string]: any;
                qProfile?: string;
                inherit?: string;
                severity?: string;
                params?: ActiveRuleParam[];
            };
            facets?: Facet[];
        }
        export interface GetRulesShowResponse {
            rule?: Rule;
            actives?: ActiveRule[];
        }
        export interface GetRulesTagsResponse {
            tags?: string[];
        }
        export interface GetSettingsListDefinitionsResponse {
            definitions?: Definitions[];
        }
        export interface GetSettingsValuesResponse {
            settings?: {
                [name: string]: any;
            }[];
            setSecuredSettings?: string[];
        }
        export interface GetSourcesScmResponse {
            scm?: number[][];
        }
        export interface GetSourcesShowResponse {
            sources?: number[][];
        }
        export interface GetSystemDbMigrationStatusResponse {
            state?: string;
            message?: string;
            startedAt?: string;
        }
        export interface GetSystemHealthResponse {
            health?: string;
            causes?: Cause[];
            nodes?: Node[];
        }
        export interface GetSystemInfoResponse {
            Health?: string;
            "Health Causes"?: any[];
            System?: {
                [name: string]: any;
            };
            Database?: {
                [name: string]: any;
            };
            Bundled?: {
                [name: string]: any;
            };
            Plugins?: {
                [name: string]: any;
            };
            "Web JVM State"?: {
                [name: string]: any;
            };
            "Web Database Connection"?: {
                [name: string]: any;
            };
            "Web Logging"?: {
                [name: string]: any;
            };
            "Web JVM Properties"?: {
                [name: string]: any;
            };
            "Compute Engine Tasks"?: {
                [name: string]: any;
            };
            "Compute Engine JVM State"?: {
                [name: string]: any;
            };
            "Compute Engine Database Connection"?: {
                [name: string]: any;
            };
            "Compute Engine Logging"?: {
                [name: string]: any;
            };
            "Compute Engine JVM Properties"?: {
                [name: string]: any;
            };
            "Search State"?: {
                [name: string]: any;
            };
            "Search Indexes"?: {
                [name: string]: any;
            };
            ALMs?: {
                [name: string]: any;
            };
            "Server Push Connections"?: {
                [name: string]: any;
            };
            Settings?: {
                [name: string]: any;
            };
        }
        export interface GetSystemStatusResponse {
            id?: string;
            version?: string;
            status?: string;
        }
        export interface GetSystemUpgradesResponse {
            upgrades?: Version[];
            latestLTS?: string;
            updateCenterRefresh?: string;
        }
        export interface GetUserGroupsSearchResponse {
            paging?: Paging;
            groups?: UserGroup[];
        }
        export interface GetUserGroupsUsersResponse {
            users?: User[];
            paging?: Paging;
        }
        export interface GetUserTokensSearchResponse {
            login?: string;
            userTokens?: UserToken[];
        }
        export interface GetUsersGroupsResponse {
            paging?: Paging;
            groups?: UserGroup[];
        }
        export interface GetUsersSearchResponse {
            paging?: Paging;
            users?: User[];
        }
        export interface GetWebhooksDeliveriesResponse {
            paging?: Paging;
            deliveries?: Delivery[];
        }
        export interface GetWebhooksDeliveryResponse {
            delivery?: Delivery;
        }
        export interface GetWebhooksListResponse {
            webhooks?: Webhook[];
        }
        export interface GetWebservicesListResponse {
            webServices?: Webservice[];
        }
        export interface GetWebservicesResponseExampleResponse {
            format?: string;
            example?: string;
        }
        export interface GitlabRepository {
            id?: number;
            name?: string;
            pathName?: string;
            slug?: string;
            pathSlug?: string;
            url?: string;
        }
        export interface Hotspot {
            key?: string;
            component?: string;
            project?: string;
            securityCategory?: string;
            vulnerabilityProbability?: string;
            status?: string;
            line?: number;
            message?: string;
            messageFormattings?: any[];
            assignee?: string;
            author?: string;
            creationDate?: string;
            updateDate?: string;
            flows?: any[];
            ruleKey?: string;
        }
        export interface HotspotChangelog {
            user?: string;
            userName?: string;
            creationDate?: string;
            diffs?: ChangelogDiff[];
            avatar?: string;
            isUserActive?: string;
        }
        export interface HotspotComment {
            key?: string;
            login?: string;
            htmlText?: string;
            markdown?: string;
            createdAt?: string;
        }
        export interface Impact {
            softwareQuality?: string;
            severity?: string;
        }
        export interface Issue {
            key?: string;
            rule?: string;
            severity?: string;
            cleanCodeAttribute?: string;
            cleanCodeAttributeCategory?: string;
            impacts?: Impact[];
            component?: string;
            project?: string;
            line?: number;
            textRange?: TextRange;
            flows?: any[];
            status?: string;
            issueStatus?: string;
            message?: string;
            effort?: string;
            debt?: string;
            assignee?: string;
            author?: string;
            tags?: string[];
            transitions?: string[];
            actions?: string[];
            comments?: IssueComent[];
            creationDate?: string;
            updateDate?: string;
            type?: string;
            ruleDescriptionContextKey?: string;
        }
        export interface IssueChangelog {
            creationDate?: string;
            diffs?: ChangelogDiff[];
        }
        export interface IssueComent {
            key?: string;
            login?: string;
            htmlText?: string;
            markdown?: string;
            updatable?: string;
            createdAt?: string;
        }
        export interface IssueFacet {
            property?: string;
            values?: FacetValue[];
        }
        export interface Language {
            key?: string;
            name?: string;
        }
        export interface Link {
            id?: string;
            name?: string;
            type?: string;
            url?: string;
        }
        export interface Location {
            textRange?: TextRange;
            msg?: string;
            msgFormattings?: any[];
        }
        export interface Measure {
            metric?: string;
            history?: MeasureHistory[];
        }
        export interface MeasureHistory {
            date?: string;
            value?: string;
        }
        export interface MeasurePeriod {
            value?: string;
        }
        export interface MessageFormatting {
            start?: number;
            end?: number;
            type?: string;
        }
        export interface Metric {
            id?: string;
            key?: string;
            name?: string;
            description?: string;
            domain?: string;
            type?: string;
            direction?: number;
            qualitative?: string;
            hidden?: string;
            custom?: string;
        }
        export interface NewCodePeriod {
            projectKey?: string;
            branchKey?: string;
            inherited?: string;
        }
        export interface Node {
            name?: string;
            type?: string;
            host?: string;
            port?: number;
            startedAt?: string;
            health?: string;
            causes?: Cause[];
        }
        export interface Notification {
            channel?: string;
            type?: string;
            organization?: string;
            project?: string;
            projectName?: string;
        }
        export interface Paging {
            pageIndex?: number;
            pageSize?: number;
            total?: number;
        }
        export interface Period {
            mode?: string;
            date?: string;
            parameter?: string;
        }
        export interface Plugin {
            key?: string;
            name?: string;
            category?: string;
            description?: string;
            license?: string;
            organizationName?: string;
            organizationUrl?: string;
            editionBundled?: string;
        }
        export interface PluginRelease {
            version?: string;
            date?: string;
            description?: string;
            changeLogUrl?: string;
        }
        export interface PluginRequirement {
            key?: string;
            name?: string;
            description?: string;
        }
        export interface PluginUpdate {
            release?: PluginRelease;
            status?: string;
            requires?: PluginRequirement[];
        }
        export interface PostAlmIntegrationsImportAzureProjectBody {
            import_azure_project: string;
        }
        export interface PostAlmIntegrationsImportBitbucketcloudRepoBody {
            import_bitbucketcloud_repo: string;
        }
        export interface PostAlmIntegrationsImportBitbucketserverProjectBody {
            import_bitbucketserver_project: string;
        }
        export interface PostAlmIntegrationsImportGithubProjectBody {
            import_github_project: string;
        }
        export interface PostAlmIntegrationsImportGitlabProjectBody {
            import_gitlab_project: string;
        }
        export interface PostAlmIntegrationsSetPatBody {
            set_pat: string;
        }
        export interface PostAlmSettingsCreateAzureBody {
            create_azure: string;
        }
        export interface PostAlmSettingsCreateBitbucketBody {
            create_bitbucket: string;
        }
        export interface PostAlmSettingsCreateBitbucketcloudBody {
            create_bitbucketcloud: string;
        }
        export interface PostAlmSettingsCreateGithubBody {
            create_github: string;
        }
        export interface PostAlmSettingsCreateGitlabBody {
            create_gitlab: string;
        }
        export interface PostAlmSettingsDeleteBody {
            delete: string;
        }
        export interface PostAlmSettingsUpdateAzureBody {
            update_azure: string;
        }
        export interface PostAlmSettingsUpdateBitbucketBody {
            update_bitbucket: string;
        }
        export interface PostAlmSettingsUpdateBitbucketcloudBody {
            update_bitbucketcloud: string;
        }
        export interface PostAlmSettingsUpdateGithubBody {
            update_github: string;
        }
        export interface PostAlmSettingsUpdateGitlabBody {
            update_gitlab: string;
        }
        export interface PostAuthenticationLoginBody {
            login: string;
        }
        export interface PostAuthenticationLogoutBody {
        }
        export interface PostFavoritesAddBody {
            add: string;
        }
        export interface PostFavoritesRemoveBody {
            remove: string;
        }
        export interface PostHotspotsChangeStatusBody {
            change_status: "TO_REVIEW" | "REVIEWED";
        }
        export interface PostIssuesAddCommentBody {
            add_comment: string;
        }
        export interface PostIssuesAddCommentResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostIssuesAssignBody {
            assign: string;
        }
        export interface PostIssuesAssignResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostIssuesBulkChangeBody {
            bulk_change: "CODE_SMELL" | "BUG" | "VULNERABILITY" | "SECURITY_HOTSPOT";
        }
        export interface PostIssuesBulkChangeResponse {
            total?: number;
            success?: number;
            ignored?: number;
            failures?: number;
        }
        export interface PostIssuesDeleteCommentBody {
            delete_comment: string;
        }
        export interface PostIssuesDeleteCommentResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostIssuesDoTransitionBody {
            do_transition: "confirm" | "unconfirm" | "reopen" | "resolve" | "falsepositive" | "wontfix" | "close" | "setinreview" | "resolveasreviewed" | "resetastoreview" | "accept";
        }
        export interface PostIssuesDoTransitionResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostIssuesEditCommentBody {
            edit_comment: string;
        }
        export interface PostIssuesEditCommentResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostIssuesReindexBody {
            reindex: string;
        }
        export interface PostIssuesSetSeverityBody {
            set_severity: "INFO" | "MINOR" | "MAJOR" | "CRITICAL" | "BLOCKER";
        }
        export interface PostIssuesSetSeverityResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostIssuesSetTagsBody {
            set_tags: string[];
        }
        export interface PostIssuesSetTagsResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostIssuesSetTypeBody {
            set_type: "CODE_SMELL" | "BUG" | "VULNERABILITY";
        }
        export interface PostIssuesSetTypeResponse {
            issue?: Issue;
            components?: Component[];
            rules?: Rule[];
            users?: User[];
        }
        export interface PostNewCodePeriodsSetBody {
            set: string;
        }
        export interface PostNewCodePeriodsUnsetBody {
            unset?: string;
        }
        export interface PostNotificationsAddBody {
            add: string;
        }
        export interface PostNotificationsRemoveBody {
            remove: string;
        }
        export interface PostPermissionsAddGroupBody {
            add_group: string;
        }
        export interface PostPermissionsAddGroupToTemplateBody {
            add_group_to_template: string;
        }
        export interface PostPermissionsAddProjectCreatorToTemplateBody {
            add_project_creator_to_template: string;
        }
        export interface PostPermissionsAddUserBody {
            add_user: string;
        }
        export interface PostPermissionsAddUserToTemplateBody {
            add_user_to_template: string;
        }
        export interface PostPermissionsApplyTemplateBody {
            apply_template?: string;
        }
        export interface PostPermissionsBulkApplyTemplateBody {
            bulk_apply_template?: string;
        }
        export interface PostPermissionsCreateTemplateBody {
            create_template: string;
        }
        export interface PostPermissionsCreateTemplateResponse {
            permissionTemplate?: Template;
        }
        export interface PostPermissionsDeleteTemplateBody {
            delete_template?: string;
        }
        export interface PostPermissionsRemoveGroupBody {
            remove_group: string;
        }
        export interface PostPermissionsRemoveGroupFromTemplateBody {
            remove_group_from_template: string;
        }
        export interface PostPermissionsRemoveProjectCreatorFromTemplateBody {
            remove_project_creator_from_template: string;
        }
        export interface PostPermissionsRemoveUserBody {
            remove_user: string;
        }
        export interface PostPermissionsRemoveUserFromTemplateBody {
            remove_user_from_template: string;
        }
        export interface PostPermissionsSetDefaultTemplateBody {
            set_default_template?: string;
        }
        export interface PostPermissionsUpdateTemplateBody {
            update_template: string;
        }
        export interface PostPermissionsUpdateTemplateResponse {
            permissionTemplate?: Template;
        }
        export interface PostPluginsCancelAllBody {
        }
        export interface PostPluginsInstallBody {
            install: string;
        }
        export interface PostPluginsUninstallBody {
            uninstall: string;
        }
        export interface PostPluginsUpdateBody {
            update: string;
        }
        export interface PostProjectAnalysesCreateEventBody {
            create_event: number;
        }
        export interface PostProjectAnalysesCreateEventResponse {
            event?: Event;
        }
        export interface PostProjectAnalysesDeleteBody {
            delete: string;
        }
        export interface PostProjectAnalysesDeleteEventBody {
            delete_event: string;
        }
        export interface PostProjectAnalysesUpdateEventBody {
            update_event: number;
        }
        export interface PostProjectAnalysesUpdateEventResponse {
            event?: Event;
        }
        export interface PostProjectBadgesRenewTokenBody {
            renew_token: string;
        }
        export interface PostProjectBranchesDeleteBody {
            delete: string;
        }
        export interface PostProjectBranchesRenameBody {
            rename: string;
        }
        export interface PostProjectBranchesSetAutomaticDeletionProtectionBody {
            set_automatic_deletion_protection: "true" | "false" | "yes" | "no";
        }
        export interface PostProjectBranchesSetMainBody {
            set_main: string;
        }
        export interface PostProjectDumpExportBody {
            export: string;
        }
        export interface PostProjectDumpExportResponse {
            taskId?: string;
            projectId?: string;
            projectKey?: string;
            projectName?: string;
        }
        export interface PostProjectLinksCreateBody {
            create: string;
        }
        export interface PostProjectLinksCreateResponse {
            link?: Link;
        }
        export interface PostProjectLinksDeleteBody {
            delete: number;
        }
        export interface PostProjectTagsSetBody {
            set: string[];
        }
        export interface PostProjectsBulkDeleteBody {
            bulk_delete?: ("TRK" | "VW" | "APP")[];
        }
        export interface PostProjectsCreateBody {
            create: "private" | "public";
        }
        export interface PostProjectsCreateResponse {
            project?: Project;
        }
        export interface PostProjectsDeleteBody {
            delete: string;
        }
        export interface PostProjectsUpdateKeyBody {
            update_key: string;
        }
        export interface PostProjectsUpdateVisibilityBody {
            update_visibility: "private" | "public";
        }
        export interface PostQualitygatesAddGroupBody {
            add_group: string;
        }
        export interface PostQualitygatesAddUserBody {
            add_user: string;
        }
        export interface PostQualitygatesCopyBody {
            copy: string;
        }
        export interface PostQualitygatesCreateBody {
            create: string;
        }
        export interface PostQualitygatesCreateConditionBody {
            create_condition: "LT" | "GT";
        }
        export interface PostQualitygatesCreateConditionResponse {
            id?: string;
            metric?: string;
            op?: string;
            error?: string;
            warning?: string;
        }
        export interface PostQualitygatesCreateResponse {
            id?: string;
            name?: string;
        }
        export interface PostQualitygatesDeleteConditionBody {
            delete_condition: number;
        }
        export interface PostQualitygatesDeselectBody {
            deselect: string;
        }
        export interface PostQualitygatesDestroyBody {
            destroy: string;
        }
        export interface PostQualitygatesRemoveGroupBody {
            remove_group: string;
        }
        export interface PostQualitygatesRemoveUserBody {
            remove_user: string;
        }
        export interface PostQualitygatesRenameBody {
            rename: string;
        }
        export interface PostQualitygatesSelectBody {
            select: string;
        }
        export interface PostQualitygatesSetAsDefaultBody {
            set_as_default: string;
        }
        export interface PostQualitygatesUpdateConditionBody {
            update_condition: "LT" | "GT";
        }
        export interface PostQualityprofilesActivateRuleBody {
            activate_rule: "INFO" | "MINOR" | "MAJOR" | "CRITICAL" | "BLOCKER";
        }
        export interface PostQualityprofilesActivateRulesBody {
            activate_rules: ("CODE_SMELL" | "BUG" | "VULNERABILITY" | "SECURITY_HOTSPOT")[];
        }
        export interface PostQualityprofilesAddProjectBody {
            add_project: string;
        }
        export interface PostQualityprofilesChangeParentBody {
            change_parent: string;
        }
        export interface PostQualityprofilesCopyBody {
            copy: string;
        }
        export interface PostQualityprofilesCopyResponse {
            key?: string;
            name?: string;
            language?: string;
            isDefault?: string;
            isInherited?: string;
            parentKey?: string;
        }
        export interface PostQualityprofilesCreateBody {
            create: string;
        }
        export interface PostQualityprofilesCreateResponse {
            profile?: QualityProfile;
            warnings?: string[];
        }
        export interface PostQualityprofilesDeactivateRuleBody {
            deactivate_rule: string;
        }
        export interface PostQualityprofilesDeactivateRulesBody {
            deactivate_rules: ("CODE_SMELL" | "BUG" | "VULNERABILITY" | "SECURITY_HOTSPOT")[];
        }
        export interface PostQualityprofilesDeleteBody {
            delete: string;
        }
        export interface PostQualityprofilesRemoveProjectBody {
            remove_project: string;
        }
        export interface PostQualityprofilesRenameBody {
            rename: string;
        }
        export interface PostQualityprofilesRestoreBody {
            restore: string;
        }
        export interface PostQualityprofilesSetDefaultBody {
            set_default: string;
        }
        export interface PostRulesCreateBody {
            create: "CODE_SMELL" | "BUG" | "VULNERABILITY" | "SECURITY_HOTSPOT";
        }
        export interface PostRulesCreateResponse {
            rule?: Rule;
        }
        export interface PostRulesDeleteBody {
            delete: string;
        }
        export interface PostRulesUpdateBody {
            update: string;
        }
        export interface PostRulesUpdateResponse {
            rule?: Rule;
        }
        export interface PostSettingsResetBody {
            reset: string[];
        }
        export interface PostSettingsSetBody {
            set: string;
        }
        export interface PostSystemChangeLogLevelBody {
            change_log_level: "TRACE" | "DEBUG" | "INFO";
        }
        export interface PostSystemMigrateDbBody {
        }
        export interface PostSystemMigrateDbResponse {
            state?: string;
            message?: string;
            startedAt?: string;
        }
        export interface PostSystemRestartBody {
        }
        export interface PostUserGroupsAddUserBody {
            add_user: string;
        }
        export interface PostUserGroupsCreateBody {
            create: string;
        }
        export interface PostUserGroupsCreateResponse {
            group?: UserGroup;
        }
        export interface PostUserGroupsDeleteBody {
            delete: string;
        }
        export interface PostUserGroupsRemoveUserBody {
            remove_user: string;
        }
        export interface PostUserGroupsUpdateBody {
            update: string;
        }
        export interface PostUserTokensGenerateBody {
            generate: "USER_TOKEN" | "GLOBAL_ANALYSIS_TOKEN" | "PROJECT_ANALYSIS_TOKEN";
        }
        export interface PostUserTokensGenerateResponse {
            login?: string;
            name?: string;
            createdAt?: string;
            expirationDate?: string;
            token?: string;
            type?: string;
        }
        export interface PostUserTokensRevokeBody {
            revoke: string;
        }
        export interface PostUsersAnonymizeBody {
            anonymize: string;
        }
        export interface PostUsersChangePasswordBody {
            change_password: string;
        }
        export interface PostUsersCreateBody {
            create: string;
        }
        export interface PostUsersCreateResponse {
            user?: User;
        }
        export interface PostUsersDeactivateBody {
            deactivate: string;
        }
        export interface PostUsersDeactivateResponse {
            user?: User;
        }
        export interface PostUsersUpdateBody {
            update: string;
        }
        export interface PostUsersUpdateIdentityProviderBody {
            update_identity_provider: string;
        }
        export interface PostUsersUpdateLoginBody {
            update_login: string;
        }
        export interface PostUsersUpdateResponse {
            user?: User;
        }
        export interface PostWebhooksCreateBody {
            create: string;
        }
        export interface PostWebhooksCreateResponse {
            webhook?: Webhook;
        }
        export interface PostWebhooksDeleteBody {
            delete: string;
        }
        export interface PostWebhooksUpdateBody {
            update: string;
        }
        export interface Project {
            key?: string;
            name?: string;
        }
        export interface ProjectStatus {
            status?: string;
            ignoredConditions?: string;
            caycStatus?: string;
            conditions?: Condition[];
            period?: Period;
        }
        export interface QualityGate {
            key?: string;
            name?: string;
            activeRuleCount?: number;
            overridingRuleCount?: number;
            isBuiltIn?: string;
        }
        export interface QualityProfile {
            key?: string;
            name?: string;
            language?: string;
            languageName?: string;
            isInherited?: string;
            isBuiltIn?: string;
            activeRuleCount?: number;
            activeDeprecatedRuleCount?: number;
            isDefault?: string;
            ruleUpdatedAt?: string;
            actions?: Action;
        }
        export interface QualityProfileEvent {
            action?: string;
            authorLogin?: string;
            authorName?: string;
            sonarQubeVersion?: string;
            ruleKey?: string;
            ruleName?: string;
            cleanCodeAttributeCategory?: string;
            impacts?: Impact[];
            date?: string;
            params?: {
                [name: string]: any;
            };
        }
        export interface QualityProfileFormat {
            key?: string;
            name?: string;
            languages?: string[];
        }
        export interface Rule {
            key?: string;
            repo?: string;
            name?: string;
            createdAt?: string;
            htmlDesc?: string;
            mdDesc?: string;
            severity?: string;
            status?: string;
            isTemplate?: string;
            templateKey?: string;
            tags?: any[];
            sysTags?: any[];
            lang?: string;
            langName?: string;
            params?: RuleParam[];
            remFnOverloaded?: string;
            scope?: string;
            isExternal?: string;
            type?: string;
            cleanCodeAttributeCategory?: string;
            cleanCodeAttribute?: string;
            impacts?: Impact[];
        }
        export interface RuleDescriptionSection {
            key?: string;
            content?: string;
            context?: Context;
        }
        export interface RuleParam {
            key?: string;
            htmlDesc?: string;
            defaultValue?: string;
            type?: string;
        }
        export interface RuleRepository {
            key?: string;
            name?: string;
            language?: string;
        }
        export interface Task {
            id?: string;
            type?: string;
            componentId?: string;
            componentKey?: string;
            componentName?: string;
            componentQualifier?: string;
            analysisId?: string;
            status?: string;
            submittedAt?: string;
            submitterLogin?: string;
            startedAt?: string;
            executedAt?: string;
            executionTimeMs?: number;
            hasScannerContext?: string;
            warningCount?: number;
            warnings?: string[];
        }
        export interface Template {
            id?: string;
            name?: string;
            description?: string;
            projectKeyPattern?: string;
            createdAt?: string;
            updatedAt?: string;
        }
        export interface TemplatePermission {
            key?: string;
            usersCount?: number;
            groupsCount?: number;
            withProjectCreator?: string;
        }
        export interface TextRange {
            startLine?: number;
            endLine?: number;
            startOffset?: number;
            endOffset?: number;
        }
        export interface UpgradePlugins {
            requireUpdate?: Plugin[];
            incompatible?: Plugin[];
        }
        export interface User {
            login?: string;
            name?: string;
            email?: string;
            scmAccounts?: string[];
            active?: string;
            local?: string;
        }
        export interface UserGroup {
            id?: number;
            name?: string;
            description?: string;
            selected?: string;
            default?: string;
        }
        export interface UserToken {
            name?: string;
            createdAt?: string;
            type?: string;
        }
        export interface Version {
            version?: string;
            description?: string;
            releaseDate?: string;
            changeLogUrl?: string;
            downloadUrl?: string;
            plugins?: UpgradePlugins;
        }
        export interface Webhook {
            key?: string;
            name?: string;
            url?: string;
            hasSecret?: string;
        }
        export interface Webservice {
            path?: string;
            description?: string;
            since?: string;
            actions?: WebserviceAction[];
        }
        export interface WebserviceAction {
            key?: string;
            since?: string;
            description?: string;
            internal?: string;
            post?: string;
            hasResponseExample?: string;
            params?: WebserviceActionParams[];
        }
        export interface WebserviceActionChangelog {
            description?: string;
            version?: string;
        }
        export interface WebserviceActionParams {
            key?: string;
            required?: string;
            description?: string;
            exampleValue?: string;
        }
    }
}
export declare namespace Paths {
    namespace GetAlmIntegrationsListAzureProjects {
        namespace Parameters {
            export type AlmSetting = string;
        }
        export interface QueryParameters {
            almSetting: Parameters.AlmSetting;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmIntegrationsListAzureProjectsResponse;
        }
    }
    namespace GetAlmIntegrationsListBitbucketserverProjects {
        namespace Parameters {
            export type AlmSetting = string;
        }
        export interface QueryParameters {
            almSetting: Parameters.AlmSetting;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmIntegrationsListBitbucketserverProjectsResponse;
        }
    }
    namespace GetAlmIntegrationsSearchAzureRepos {
        namespace Parameters {
            export type AlmSetting = string;
            export type ProjectName = string;
            export type SearchQuery = string;
        }
        export interface QueryParameters {
            almSetting: Parameters.AlmSetting;
            projectName?: Parameters.ProjectName;
            searchQuery?: Parameters.SearchQuery;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmIntegrationsSearchAzureReposResponse;
        }
    }
    namespace GetAlmIntegrationsSearchBitbucketcloudRepos {
        namespace Parameters {
            export type AlmSetting = string;
            export type P = number;
            export type Ps = number;
            export type RepositoryName = string;
        }
        export interface QueryParameters {
            almSetting: Parameters.AlmSetting;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            repositoryName?: Parameters.RepositoryName;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmIntegrationsSearchBitbucketcloudReposResponse;
        }
    }
    namespace GetAlmIntegrationsSearchBitbucketserverRepos {
        namespace Parameters {
            export type AlmSetting = string;
            export type ProjectName = string;
            export type RepositoryName = string;
        }
        export interface QueryParameters {
            almSetting: Parameters.AlmSetting;
            projectName?: Parameters.ProjectName;
            repositoryName?: Parameters.RepositoryName;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmIntegrationsSearchBitbucketserverReposResponse;
        }
    }
    namespace GetAlmIntegrationsSearchGitlabRepos {
        namespace Parameters {
            export type AlmSetting = string;
            export type P = number;
            export type ProjectName = string;
            export type Ps = number;
        }
        export interface QueryParameters {
            almSetting: Parameters.AlmSetting;
            p?: Parameters.P;
            projectName?: Parameters.ProjectName;
            ps?: Parameters.Ps;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmIntegrationsSearchGitlabReposResponse;
        }
    }
    namespace GetAlmSettingsCountBinding {
        namespace Parameters {
            export type AlmSetting = string;
        }
        export interface QueryParameters {
            almSetting: Parameters.AlmSetting;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmSettingsCountBindingResponse;
        }
    }
    namespace GetAlmSettingsGetBinding {
        namespace Parameters {
            export type Project = string;
        }
        export interface QueryParameters {
            project: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmSettingsGetBindingResponse;
        }
    }
    namespace GetAlmSettingsList {
        namespace Parameters {
            export type Project = string;
        }
        export interface QueryParameters {
            project?: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmSettingsListResponse;
        }
    }
    namespace GetAlmSettingsListDefinitions {
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmSettingsListDefinitionsResponse;
        }
    }
    namespace GetAlmSettingsValidate {
        namespace Parameters {
            export type Key = string;
        }
        export interface QueryParameters {
            key: Parameters.Key;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetAlmSettingsValidateResponse;
        }
    }
    namespace GetAnalysisCacheGet {
        namespace Parameters {
            export type Branch = string;
            export type Project = string;
        }
        export interface QueryParameters {
            branch?: Parameters.Branch;
            project: Parameters.Project;
        }
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace GetAuthenticationValidate {
        namespace Responses {
            export type $200 = Components.Schemas.GetAuthenticationValidateResponse;
        }
    }
    namespace GetCeActivity {
        namespace Parameters {
            export type Component = string;
            export type MaxExecutedAt = string;
            export type MinSubmittedAt = string;
            export type OnlyCurrents = "true" | "false" | "yes" | "no";
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Status = "SUCCESS" | "FAILED" | "CANCELED" | "PENDING" | "IN_PROGRESS";
            export type Type = "REPORT" | "ISSUE_SYNC" | "AUDIT_PURGE" | "PROJECT_EXPORT";
        }
        export interface QueryParameters {
            component?: Parameters.Component;
            maxExecutedAt?: Parameters.MaxExecutedAt;
            minSubmittedAt?: Parameters.MinSubmittedAt;
            onlyCurrents?: Parameters.OnlyCurrents;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            status?: Parameters.Status;
            type?: Parameters.Type;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetCeActivityResponse;
        }
    }
    namespace GetCeActivityStatus {
        namespace Parameters {
            export type Component = string;
        }
        export interface QueryParameters {
            component?: Parameters.Component;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetCeActivityStatusResponse;
        }
    }
    namespace GetCeComponent {
        namespace Parameters {
            export type Component = string;
        }
        export interface QueryParameters {
            component: Parameters.Component;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetCeComponentResponse;
        }
    }
    namespace GetCeTask {
        namespace Parameters {
            export type AdditionalFields = ("stacktrace" | "scannerContext" | "warnings")[];
            export type Id = string;
        }
        export interface QueryParameters {
            additionalFields?: Parameters.AdditionalFields;
            id: Parameters.Id;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetCeTaskResponse;
        }
    }
    namespace GetComponentsSearch {
        namespace Parameters {
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Qualifiers = ("TRK")[];
        }
        export interface QueryParameters {
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            qualifiers: Parameters.Qualifiers;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetComponentsSearchResponse;
        }
    }
    namespace GetComponentsShow {
        namespace Parameters {
            export type Branch = string;
            export type Component = string;
            export type PullRequest = number;
        }
        export interface QueryParameters {
            branch?: Parameters.Branch;
            component: Parameters.Component;
            pullRequest?: Parameters.PullRequest;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetComponentsShowResponse;
        }
    }
    namespace GetComponentsTree {
        namespace Parameters {
            export type Asc = "true" | "false" | "yes" | "no";
            export type Branch = string;
            export type Component = string;
            export type P = number;
            export type Ps = number;
            export type PullRequest = number;
            export type Q = string;
            export type Qualifiers = ("UTS" | "FIL" | "DIR" | "TRK")[];
            export type S = ("name" | "path" | "qualifier")[];
            export type Strategy = "all" | "children" | "leaves";
        }
        export interface QueryParameters {
            asc?: Parameters.Asc;
            branch?: Parameters.Branch;
            component: Parameters.Component;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            pullRequest?: Parameters.PullRequest;
            q?: Parameters.Q;
            qualifiers?: Parameters.Qualifiers;
            s?: Parameters.S;
            strategy?: Parameters.Strategy;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetComponentsTreeResponse;
        }
    }
    namespace GetDuplicationsShow {
        namespace Parameters {
            export type Key = string;
        }
        export interface QueryParameters {
            key: Parameters.Key;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetDuplicationsShowResponse;
        }
    }
    namespace GetFavoritesSearch {
        namespace Parameters {
            export type P = number;
            export type Ps = number;
        }
        export interface QueryParameters {
            p?: Parameters.P;
            ps?: Parameters.Ps;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetFavoritesSearchResponse;
        }
    }
    namespace GetHotspotsSearch {
        namespace Parameters {
            export type Branch = string;
            export type Cwe = string[];
            export type Files = string[];
            export type Hotspots = string[];
            export type InNewCodePeriod = "true" | "false" | "yes" | "no";
            export type OnlyMine = "true" | "false" | "yes" | "no";
            export type OwaspAsvs40 = string[];
            export type OwaspAsvsLevel = 1 | 2 | 3;
            export type OwaspTop10 = ("a1" | "a2" | "a3" | "a4" | "a5" | "a6" | "a7" | "a8" | "a9" | "a10")[];
            export type OwaspTop102021 = ("a1" | "a2" | "a3" | "a4" | "a5" | "a6" | "a7" | "a8" | "a9" | "a10")[];
            export type P = number;
            export type PciDss32 = string[];
            export type PciDss40 = string[];
            export type Project = string;
            export type Ps = number;
            export type PullRequest = number;
            export type Resolution = "FIXED" | "SAFE" | "ACKNOWLEDGED";
            export type SansTop25 = ("insecure-interaction" | "risky-resource" | "porous-defenses")[];
            export type SonarsourceSecurity = ("buffer-overflow" | "sql-injection" | "rce" | "object-injection" | "command-injection" | "path-traversal-injection" | "ldap-injection" | "xpath-injection" | "log-injection" | "xxe" | "xss" | "dos" | "ssrf" | "csrf" | "http-response-splitting" | "open-redirect" | "weak-cryptography" | "auth" | "insecure-conf" | "file-manipulation" | "encrypt-data" | "traceability" | "permission" | "others")[];
            export type Status = "TO_REVIEW" | "REVIEWED";
        }
        export interface QueryParameters {
            branch?: Parameters.Branch;
            cwe?: Parameters.Cwe;
            files?: Parameters.Files;
            hotspots?: Parameters.Hotspots;
            inNewCodePeriod?: Parameters.InNewCodePeriod;
            onlyMine?: Parameters.OnlyMine;
            "owaspAsvs-4.0"?: Parameters.OwaspAsvs40;
            owaspAsvsLevel?: Parameters.OwaspAsvsLevel;
            owaspTop10?: Parameters.OwaspTop10;
            "owaspTop10-2021"?: Parameters.OwaspTop102021;
            p?: Parameters.P;
            "pciDss-3.2"?: Parameters.PciDss32;
            "pciDss-4.0"?: Parameters.PciDss40;
            project?: Parameters.Project;
            ps?: Parameters.Ps;
            pullRequest?: Parameters.PullRequest;
            resolution?: Parameters.Resolution;
            sansTop25?: Parameters.SansTop25;
            sonarsourceSecurity?: Parameters.SonarsourceSecurity;
            status?: Parameters.Status;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetHotspotsSearchResponse;
        }
    }
    namespace GetHotspotsShow {
        namespace Parameters {
            export type Hotspot = string;
        }
        export interface QueryParameters {
            hotspot: Parameters.Hotspot;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetHotspotsShowResponse;
        }
    }
    namespace GetIssuesAuthors {
        namespace Parameters {
            export type Project = string;
            export type Ps = number;
            export type Q = string;
        }
        export interface QueryParameters {
            project?: Parameters.Project;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetIssuesAuthorsResponse;
        }
    }
    namespace GetIssuesChangelog {
        namespace Parameters {
            export type Issue = string;
        }
        export interface QueryParameters {
            issue: Parameters.Issue;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetIssuesChangelogResponse;
        }
    }
    namespace GetIssuesSearch {
        namespace Parameters {
            export type AdditionalFields = ("_all" | "comments" | "languages" | "rules" | "ruleDescriptionContextKey" | "transitions" | "actions" | "users")[];
            export type Asc = "true" | "false" | "yes" | "no";
            export type Assigned = "true" | "false" | "yes" | "no";
            export type Assignees = string[];
            export type Author = string;
            export type Branch = string;
            export type CleanCodeAttributeCategories = ("ADAPTABLE" | "CONSISTENT" | "INTENTIONAL" | "RESPONSIBLE")[];
            export type CodeVariants = string[];
            export type Components = string[];
            export type CreatedAfter = string;
            export type CreatedAt = string;
            export type CreatedBefore = string;
            export type CreatedInLast = string;
            export type Cwe = string[];
            export type Facets = ("projects" | "files" | "assigned_to_me" | "severities" | "statuses" | "resolutions" | "rules" | "assignees" | "author" | "directories" | "scopes" | "languages" | "tags" | "types" | "pciDss-3.2" | "pciDss-4.0" | "owaspAsvs-4.0" | "owaspTop10" | "owaspTop10-2021" | "sansTop25" | "cwe" | "createdAt" | "sonarsourceSecurity" | "codeVariants" | "cleanCodeAttributeCategories" | "impactSoftwareQualities" | "impactSeverities" | "issueStatuses")[];
            export type FixedInPullRequest = number;
            export type ImpactSeverities = ("LOW" | "MEDIUM" | "HIGH")[];
            export type ImpactSoftwareQualities = ("MAINTAINABILITY" | "RELIABILITY" | "SECURITY")[];
            export type InNewCodePeriod = "true" | "false" | "yes" | "no";
            export type IssueStatuses = "OPEN" | "CONFIRMED" | "FALSE_POSITIVE" | "ACCEPTED" | "FIXED";
            export type Issues = string[];
            export type Languages = string[];
            export type OnComponentOnly = "true" | "false" | "yes" | "no";
            export type OwaspAsvs40 = string[];
            export type OwaspAsvsLevel = "1" | "2" | "3";
            export type OwaspTop10 = ("a1" | "a2" | "a3" | "a4" | "a5" | "a6" | "a7" | "a8" | "a9" | "a10")[];
            export type OwaspTop102021 = ("a1" | "a2" | "a3" | "a4" | "a5" | "a6" | "a7" | "a8" | "a9" | "a10")[];
            export type P = number;
            export type PciDss32 = string[];
            export type PciDss40 = string[];
            export type Ps = number;
            export type PullRequest = number;
            export type Resolutions = ("FALSE-POSITIVE" | "WONTFIX" | "FIXED" | "REMOVED")[];
            export type Resolved = "true" | "false" | "yes" | "no";
            export type Rules = string[];
            export type S = "UPDATE_DATE" | "HOTSPOTS" | "FILE_LINE" | "STATUS" | "SEVERITY" | "CLOSE_DATE" | "CREATION_DATE";
            export type SansTop25 = ("insecure-interaction" | "risky-resource" | "porous-defenses")[];
            export type Scopes = ("MAIN" | "TEST")[];
            export type Severities = ("INFO" | "MINOR" | "MAJOR" | "CRITICAL" | "BLOCKER")[];
            export type SonarsourceSecurity = ("buffer-overflow" | "sql-injection" | "rce" | "object-injection" | "command-injection" | "path-traversal-injection" | "ldap-injection" | "xpath-injection" | "log-injection" | "xxe" | "xss" | "dos" | "ssrf" | "csrf" | "http-response-splitting" | "open-redirect" | "weak-cryptography" | "auth" | "insecure-conf" | "file-manipulation" | "encrypt-data" | "traceability" | "permission" | "others")[];
            export type Statuses = ("OPEN" | "CONFIRMED" | "REOPENED" | "RESOLVED" | "CLOSED")[];
            export type Tags = string[];
            export type TimeZone = string;
            export type Types = ("CODE_SMELL" | "BUG" | "VULNERABILITY")[];
        }
        export interface QueryParameters {
            additionalFields?: Parameters.AdditionalFields;
            asc?: Parameters.Asc;
            assigned?: Parameters.Assigned;
            assignees?: Parameters.Assignees;
            author?: Parameters.Author;
            branch?: Parameters.Branch;
            cleanCodeAttributeCategories?: Parameters.CleanCodeAttributeCategories;
            codeVariants?: Parameters.CodeVariants;
            components?: Parameters.Components;
            createdAfter?: Parameters.CreatedAfter;
            createdAt?: Parameters.CreatedAt;
            createdBefore?: Parameters.CreatedBefore;
            createdInLast?: Parameters.CreatedInLast;
            cwe?: Parameters.Cwe;
            facets?: Parameters.Facets;
            fixedInPullRequest?: Parameters.FixedInPullRequest;
            impactSeverities?: Parameters.ImpactSeverities;
            impactSoftwareQualities?: Parameters.ImpactSoftwareQualities;
            inNewCodePeriod?: Parameters.InNewCodePeriod;
            issueStatuses?: Parameters.IssueStatuses;
            issues?: Parameters.Issues;
            languages?: Parameters.Languages;
            onComponentOnly?: Parameters.OnComponentOnly;
            "owaspAsvs-4.0"?: Parameters.OwaspAsvs40;
            owaspAsvsLevel?: Parameters.OwaspAsvsLevel;
            owaspTop10?: Parameters.OwaspTop10;
            "owaspTop10-2021"?: Parameters.OwaspTop102021;
            p?: Parameters.P;
            "pciDss-3.2"?: Parameters.PciDss32;
            "pciDss-4.0"?: Parameters.PciDss40;
            ps?: Parameters.Ps;
            pullRequest?: Parameters.PullRequest;
            resolutions?: Parameters.Resolutions;
            resolved?: Parameters.Resolved;
            rules?: Parameters.Rules;
            s?: Parameters.S;
            sansTop25?: Parameters.SansTop25;
            scopes?: Parameters.Scopes;
            severities?: Parameters.Severities;
            sonarsourceSecurity?: Parameters.SonarsourceSecurity;
            statuses?: Parameters.Statuses;
            tags?: Parameters.Tags;
            timeZone?: Parameters.TimeZone;
            types?: Parameters.Types;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetIssuesSearchResponse;
        }
    }
    namespace GetIssuesTags {
        namespace Parameters {
            export type All = "true" | "false";
            export type Branch = string;
            export type Project = string;
            export type Ps = number;
            export type Q = string;
        }
        export interface QueryParameters {
            all?: Parameters.All;
            branch?: Parameters.Branch;
            project?: Parameters.Project;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetIssuesTagsResponse;
        }
    }
    namespace GetLanguagesList {
        namespace Parameters {
            export type Ps = number;
            export type Q = string;
        }
        export interface QueryParameters {
            ps?: Parameters.Ps;
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetLanguagesListResponse;
        }
    }
    namespace GetMeasuresComponent {
        namespace Parameters {
            export type AdditionalFields = ("metrics" | "period")[];
            export type Branch = string;
            export type Component = string;
            export type MetricKeys = string[];
            export type PullRequest = number;
        }
        export interface QueryParameters {
            additionalFields?: Parameters.AdditionalFields;
            branch?: Parameters.Branch;
            component: Parameters.Component;
            metricKeys: Parameters.MetricKeys;
            pullRequest?: Parameters.PullRequest;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetMeasuresComponentResponse;
        }
    }
    namespace GetMeasuresComponentTree {
        namespace Parameters {
            export type AdditionalFields = ("metrics" | "period")[];
            export type Asc = "true" | "false" | "yes" | "no";
            export type Branch = string;
            export type Component = string;
            export type MetricKeys = string[];
            export type MetricPeriodSort = "1";
            export type MetricSort = string;
            export type MetricSortFilter = "all" | "withMeasuresOnly";
            export type P = number;
            export type Ps = number;
            export type PullRequest = number;
            export type Q = string;
            export type Qualifiers = ("UTS" | "FIL" | "DIR" | "TRK")[];
            export type S = ("metric" | "metricPeriod" | "name" | "path" | "qualifier")[];
            export type Strategy = "all" | "leaves" | "children";
        }
        export interface QueryParameters {
            additionalFields?: Parameters.AdditionalFields;
            asc?: Parameters.Asc;
            branch?: Parameters.Branch;
            component: Parameters.Component;
            metricKeys: Parameters.MetricKeys;
            metricPeriodSort?: Parameters.MetricPeriodSort;
            metricSort?: Parameters.MetricSort;
            metricSortFilter?: Parameters.MetricSortFilter;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            pullRequest?: Parameters.PullRequest;
            q?: Parameters.Q;
            qualifiers?: Parameters.Qualifiers;
            s?: Parameters.S;
            strategy?: Parameters.Strategy;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetMeasuresComponentTreeResponse;
        }
    }
    namespace GetMeasuresSearchHistory {
        namespace Parameters {
            export type Branch = string;
            export type Component = string;
            export type From = string;
            export type Metrics = string[];
            export type P = number;
            export type Ps = number;
            export type PullRequest = number;
            export type To = string;
        }
        export interface QueryParameters {
            branch?: Parameters.Branch;
            component: Parameters.Component;
            from?: Parameters.From;
            metrics: Parameters.Metrics;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            pullRequest?: Parameters.PullRequest;
            to?: Parameters.To;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetMeasuresSearchHistoryResponse;
        }
    }
    namespace GetMetricsSearch {
        namespace Parameters {
            export type P = number;
            export type Ps = number;
        }
        export interface QueryParameters {
            p?: Parameters.P;
            ps?: Parameters.Ps;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetMetricsSearchResponse;
        }
    }
    namespace GetMetricsTypes {
        namespace Responses {
            export type $200 = Components.Schemas.GetMetricsTypesResponse;
        }
    }
    namespace GetNewCodePeriodsList {
        namespace Parameters {
            export type Project = string;
        }
        export interface QueryParameters {
            project: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetNewCodePeriodsListResponse;
        }
    }
    namespace GetNewCodePeriodsShow {
        namespace Parameters {
            export type Branch = string;
            export type Project = string;
        }
        export interface QueryParameters {
            branch?: Parameters.Branch;
            project?: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetNewCodePeriodsShowResponse;
        }
    }
    namespace GetNotificationsList {
        namespace Parameters {
            export type Login = string;
        }
        export interface QueryParameters {
            login?: Parameters.Login;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetNotificationsListResponse;
        }
    }
    namespace GetPermissionsSearchTemplates {
        namespace Parameters {
            export type Q = string;
        }
        export interface QueryParameters {
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetPermissionsSearchTemplatesResponse;
        }
    }
    namespace GetPluginsAvailable {
        namespace Responses {
            export type $200 = Components.Schemas.GetPluginsAvailableResponse;
        }
    }
    namespace GetPluginsInstalled {
        namespace Parameters {
            export type F = ("category")[];
        }
        export interface QueryParameters {
            f?: Parameters.F;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetPluginsInstalledResponse;
        }
    }
    namespace GetPluginsPending {
        namespace Responses {
            export type $200 = Components.Schemas.GetPluginsPendingResponse;
        }
    }
    namespace GetPluginsUpdates {
        namespace Responses {
            export type $200 = Components.Schemas.GetPluginsUpdatesResponse;
        }
    }
    namespace GetProjectAnalysesSearch {
        namespace Parameters {
            export type Category = "VERSION" | "OTHER" | "QUALITY_PROFILE" | "QUALITY_GATE" | "DEFINITION_CHANGE" | "ISSUE_DETECTION" | "SQ_UPGRADE";
            export type From = string;
            export type P = number;
            export type Project = string;
            export type Ps = number;
            export type To = string;
        }
        export interface QueryParameters {
            category?: Parameters.Category;
            from?: Parameters.From;
            p?: Parameters.P;
            project: Parameters.Project;
            ps?: Parameters.Ps;
            to?: Parameters.To;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetProjectAnalysesSearchResponse;
        }
    }
    namespace GetProjectBadgesMeasure {
        namespace Parameters {
            export type Branch = string;
            export type Metric = "bugs" | "code_smells" | "coverage" | "duplicated_lines_density" | "ncloc" | "sqale_rating" | "alert_status" | "reliability_rating" | "security_hotspots" | "security_rating" | "sqale_index" | "vulnerabilities";
            export type Project = string;
            export type Token = string;
        }
        export interface QueryParameters {
            branch?: Parameters.Branch;
            metric: Parameters.Metric;
            project: Parameters.Project;
            token?: Parameters.Token;
        }
    }
    namespace GetProjectBadgesQualityGate {
        namespace Parameters {
            export type Branch = string;
            export type Project = string;
            export type Token = string;
        }
        export interface QueryParameters {
            branch?: Parameters.Branch;
            project: Parameters.Project;
            token?: Parameters.Token;
        }
    }
    namespace GetProjectBadgesToken {
        namespace Parameters {
            export type Project = string;
        }
        export interface QueryParameters {
            project: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetProjectBadgesTokenResponse;
        }
    }
    namespace GetProjectBranchesList {
        namespace Parameters {
            export type Project = string;
        }
        export interface QueryParameters {
            project: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetProjectBranchesListResponse;
        }
    }
    namespace GetProjectLinksSearch {
        namespace Parameters {
            export type ProjectId = string;
            export type ProjectKey = string;
        }
        export interface QueryParameters {
            projectId?: Parameters.ProjectId;
            projectKey?: Parameters.ProjectKey;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetProjectLinksSearchResponse;
        }
    }
    namespace GetProjectTagsSearch {
        namespace Parameters {
            export type P = number;
            export type Ps = number;
            export type Q = string;
        }
        export interface QueryParameters {
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetProjectTagsSearchResponse;
        }
    }
    namespace GetProjectsSearch {
        namespace Parameters {
            export type AnalyzedBefore = string;
            export type OnProvisionedOnly = "true" | "false" | "yes" | "no";
            export type P = number;
            export type Projects = string[];
            export type Ps = number;
            export type Q = string;
            export type Qualifiers = ("TRK" | "VW" | "APP")[];
        }
        export interface QueryParameters {
            analyzedBefore?: Parameters.AnalyzedBefore;
            onProvisionedOnly?: Parameters.OnProvisionedOnly;
            p?: Parameters.P;
            projects?: Parameters.Projects;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            qualifiers?: Parameters.Qualifiers;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetProjectsSearchResponse;
        }
    }
    namespace GetQualitygatesGetByProject {
        namespace Parameters {
            export type Project = string;
        }
        export interface QueryParameters {
            project: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualitygatesGetByProjectResponse;
        }
    }
    namespace GetQualitygatesList {
        namespace Responses {
            export type $200 = Components.Schemas.GetQualitygatesListResponse;
        }
    }
    namespace GetQualitygatesProjectStatus {
        namespace Parameters {
            export type AnalysisId = string;
            export type Branch = string;
            export type ProjectId = string;
            export type ProjectKey = string;
            export type PullRequest = number;
        }
        export interface QueryParameters {
            analysisId?: Parameters.AnalysisId;
            branch?: Parameters.Branch;
            projectId?: Parameters.ProjectId;
            projectKey?: Parameters.ProjectKey;
            pullRequest?: Parameters.PullRequest;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualitygatesProjectStatusResponse;
        }
    }
    namespace GetQualitygatesSearch {
        namespace Parameters {
            export type GateName = string;
            export type Page = number;
            export type PageSize = number;
            export type Query = string;
            export type Selected = "all" | "deselected" | "selected";
        }
        export interface QueryParameters {
            gateName: Parameters.GateName;
            page?: Parameters.Page;
            pageSize?: Parameters.PageSize;
            query?: Parameters.Query;
            selected?: Parameters.Selected;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualitygatesSearchResponse;
        }
    }
    namespace GetQualitygatesSearchGroups {
        namespace Parameters {
            export type GateName = string;
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Selected = "all" | "deselected" | "selected";
        }
        export interface QueryParameters {
            gateName: Parameters.GateName;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            selected?: Parameters.Selected;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualitygatesSearchGroupsResponse;
        }
    }
    namespace GetQualitygatesSearchUsers {
        namespace Parameters {
            export type GateName = string;
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Selected = "all" | "deselected" | "selected";
        }
        export interface QueryParameters {
            gateName: Parameters.GateName;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            selected?: Parameters.Selected;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualitygatesSearchUsersResponse;
        }
    }
    namespace GetQualitygatesShow {
        namespace Parameters {
            export type Name = string;
        }
        export interface QueryParameters {
            name: Parameters.Name;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualitygatesShowResponse;
        }
    }
    namespace GetQualityprofilesBackup {
        namespace Parameters {
            export type Language = "kubernetes" | "css" | "scala" | "jsp" | "py" | "js" | "docker" | "java" | "web" | "flex" | "xml" | "json" | "text" | "vbnet" | "cloudformation" | "yaml" | "kotlin" | "go" | "secrets" | "ruby" | "cs" | "php" | "terraform" | "azureresourcemanager" | "ts";
            export type QualityProfile = string;
        }
        export interface QueryParameters {
            language: Parameters.Language;
            qualityProfile: Parameters.QualityProfile;
        }
    }
    namespace GetQualityprofilesChangelog {
        namespace Parameters {
            export type Language = "kubernetes" | "css" | "scala" | "jsp" | "py" | "js" | "docker" | "java" | "web" | "flex" | "xml" | "json" | "text" | "vbnet" | "cloudformation" | "yaml" | "kotlin" | "go" | "secrets" | "ruby" | "cs" | "php" | "terraform" | "azureresourcemanager" | "ts";
            export type P = number;
            export type Ps = number;
            export type QualityProfile = string;
            export type Since = string;
            export type To = string;
        }
        export interface QueryParameters {
            language: Parameters.Language;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            qualityProfile: Parameters.QualityProfile;
            since?: Parameters.Since;
            to?: Parameters.To;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualityprofilesChangelogResponse;
        }
    }
    namespace GetQualityprofilesExport {
        namespace Parameters {
            export type ExporterKey = "sonarlint-vs-vbnet" | "sonarlint-vs-cs" | "roslyn-vbnet" | "roslyn-cs";
            export type Language = "azureresourcemanager" | "cloudformation" | "cs" | "css" | "docker" | "flex" | "go" | "java" | "js" | "json" | "jsp" | "kotlin" | "kubernetes" | "php" | "py" | "ruby" | "scala" | "secrets" | "terraform" | "text" | "ts" | "vbnet" | "web" | "xml" | "yaml";
            export type QualityProfile = string;
        }
        export interface QueryParameters {
            exporterKey?: Parameters.ExporterKey;
            language: Parameters.Language;
            qualityProfile?: Parameters.QualityProfile;
        }
    }
    namespace GetQualityprofilesExporters {
        namespace Responses {
            export type $200 = Components.Schemas.GetQualityprofilesExportersResponse;
        }
    }
    namespace GetQualityprofilesImporters {
        namespace Responses {
            export type $200 = Components.Schemas.GetQualityprofilesImportersResponse;
        }
    }
    namespace GetQualityprofilesInheritance {
        namespace Parameters {
            export type Language = "kubernetes" | "css" | "scala" | "jsp" | "py" | "js" | "docker" | "java" | "web" | "flex" | "xml" | "json" | "text" | "vbnet" | "cloudformation" | "yaml" | "kotlin" | "go" | "secrets" | "ruby" | "cs" | "php" | "terraform" | "azureresourcemanager" | "ts";
            export type QualityProfile = string;
        }
        export interface QueryParameters {
            language: Parameters.Language;
            qualityProfile: Parameters.QualityProfile;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualityprofilesInheritanceResponse;
        }
    }
    namespace GetQualityprofilesProjects {
        namespace Parameters {
            export type Key = string;
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Selected = "all" | "deselected" | "selected";
        }
        export interface QueryParameters {
            key: Parameters.Key;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            selected?: Parameters.Selected;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualityprofilesProjectsResponse;
        }
    }
    namespace GetQualityprofilesSearch {
        namespace Parameters {
            export type Defaults = "true" | "false" | "yes" | "no";
            export type Language = "azureresourcemanager" | "cloudformation" | "cs" | "css" | "docker" | "flex" | "go" | "java" | "js" | "json" | "jsp" | "kotlin" | "kubernetes" | "php" | "py" | "ruby" | "scala" | "secrets" | "terraform" | "text" | "ts" | "vbnet" | "web" | "xml" | "yaml";
            export type Project = string;
            export type QualityProfile = string;
        }
        export interface QueryParameters {
            defaults?: Parameters.Defaults;
            language?: Parameters.Language;
            project?: Parameters.Project;
            qualityProfile?: Parameters.QualityProfile;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetQualityprofilesSearchResponse;
        }
    }
    namespace GetRulesRepositories {
        namespace Parameters {
            export type Language = string;
            export type Q = string;
        }
        export interface QueryParameters {
            language?: Parameters.Language;
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetRulesRepositoriesResponse;
        }
    }
    namespace GetRulesSearch {
        namespace Parameters {
            export type Activation = "true" | "false" | "yes" | "no";
            export type ActiveSeverities = ("INFO" | "MINOR" | "MAJOR" | "CRITICAL" | "BLOCKER")[];
            export type Asc = "true" | "false" | "yes" | "no";
            export type AvailableSince = string;
            export type CleanCodeAttributeCategories = ("ADAPTABLE" | "CONSISTENT" | "INTENTIONAL" | "RESPONSIBLE")[];
            export type Cwe = string[];
            export type F = ("actives" | "cleanCodeAttribute" | "createdAt" | "debtRemFn" | "defaultDebtRemFn" | "defaultRemFn" | "deprecatedKeys" | "descriptionSections" | "educationPrinciples" | "gapDescription" | "htmlDesc" | "htmlNote" | "internalKey" | "isExternal" | "isTemplate" | "lang" | "langName" | "mdDesc" | "mdNote" | "name" | "noteLogin" | "params" | "remFn" | "remFnOverloaded" | "repo" | "scope" | "severity" | "status" | "sysTags" | "tags" | "templateKey" | "updatedAt")[];
            export type Facets = ("languages" | "repositories" | "tags" | "severities" | "active_severities" | "statuses" | "types" | "true" | "cwe" | "owaspTop10" | "owaspTop10-2021" | "sansTop25" | "sonarsourceSecurity" | "cleanCodeAttributeCategories" | "impactSeverities" | "impactSoftwareQualities")[];
            export type ImpactSeverities = ("LOW" | "MEDIUM" | "HIGH")[];
            export type ImpactSoftwareQualities = ("MAINTAINABILITY" | "RELIABILITY" | "SECURITY")[];
            export type IncludeExternal = "true" | "false" | "yes" | "no";
            export type Inheritance = ("NONE" | "INHERITED" | "OVERRIDES")[];
            export type IsTemplate = "true" | "false" | "yes" | "no";
            export type Languages = string[];
            export type OwaspTop10 = ("a1" | "a2" | "a3" | "a4" | "a5" | "a6" | "a7" | "a8" | "a9" | "a10")[];
            export type OwaspTop102021 = ("a1" | "a2" | "a3" | "a4" | "a5" | "a6" | "a7" | "a8" | "a9" | "a10")[];
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Qprofile = string;
            export type Repositories = string[];
            export type RuleKey = string;
            export type S = "name" | "key" | "updatedAt" | "createdAt";
            export type SansTop25 = ("insecure-interaction" | "risky-resource" | "porous-defenses")[];
            export type Severities = ("INFO" | "MINOR" | "MAJOR" | "CRITICAL" | "BLOCKER")[];
            export type SonarsourceSecurity = ("buffer-overflow" | "sql-injection" | "rce" | "object-injection" | "command-injection" | "path-traversal-injection" | "ldap-injection" | "xpath-injection" | "log-injection" | "xxe" | "xss" | "dos" | "ssrf" | "csrf" | "http-response-splitting" | "open-redirect" | "weak-cryptography" | "auth" | "insecure-conf" | "file-manipulation" | "encrypt-data" | "traceability" | "permission" | "others")[];
            export type Statuses = ("BETA" | "DEPRECATED" | "READY" | "REMOVED")[];
            export type Tags = string[];
            export type TemplateKey = string;
            export type Types = ("CODE_SMELL" | "BUG" | "VULNERABILITY" | "SECURITY_HOTSPOT")[];
        }
        export interface QueryParameters {
            activation?: Parameters.Activation;
            active_severities?: Parameters.ActiveSeverities;
            asc?: Parameters.Asc;
            available_since?: Parameters.AvailableSince;
            cleanCodeAttributeCategories?: Parameters.CleanCodeAttributeCategories;
            cwe?: Parameters.Cwe;
            f?: Parameters.F;
            facets?: Parameters.Facets;
            impactSeverities?: Parameters.ImpactSeverities;
            impactSoftwareQualities?: Parameters.ImpactSoftwareQualities;
            include_external?: Parameters.IncludeExternal;
            inheritance?: Parameters.Inheritance;
            is_template?: Parameters.IsTemplate;
            languages?: Parameters.Languages;
            owaspTop10?: Parameters.OwaspTop10;
            "owaspTop10-2021"?: Parameters.OwaspTop102021;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            qprofile?: Parameters.Qprofile;
            repositories?: Parameters.Repositories;
            rule_key?: Parameters.RuleKey;
            s?: Parameters.S;
            sansTop25?: Parameters.SansTop25;
            severities?: Parameters.Severities;
            sonarsourceSecurity?: Parameters.SonarsourceSecurity;
            statuses?: Parameters.Statuses;
            tags?: Parameters.Tags;
            template_key?: Parameters.TemplateKey;
            types?: Parameters.Types;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetRulesSearchResponse;
        }
    }
    namespace GetRulesShow {
        namespace Parameters {
            export type Actives = "true" | "false" | "yes" | "no";
            export type Key = string;
        }
        export interface QueryParameters {
            actives?: Parameters.Actives;
            key: Parameters.Key;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetRulesShowResponse;
        }
    }
    namespace GetRulesTags {
        namespace Parameters {
            export type Ps = number;
            export type Q = string;
        }
        export interface QueryParameters {
            ps?: Parameters.Ps;
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetRulesTagsResponse;
        }
    }
    namespace GetSettingsListDefinitions {
        namespace Parameters {
            export type Component = string;
        }
        export interface QueryParameters {
            component?: Parameters.Component;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetSettingsListDefinitionsResponse;
        }
    }
    namespace GetSettingsValues {
        namespace Parameters {
            export type Component = string;
            export type Keys = string;
        }
        export interface QueryParameters {
            component?: Parameters.Component;
            keys?: Parameters.Keys;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetSettingsValuesResponse;
        }
    }
    namespace GetSourcesRaw {
        namespace Parameters {
            export type Key = string;
        }
        export interface QueryParameters {
            key: Parameters.Key;
        }
    }
    namespace GetSourcesScm {
        namespace Parameters {
            export type CommitsByLine = "true" | "false" | "yes" | "no";
            export type From = number;
            export type Key = string;
            export type To = number;
        }
        export interface QueryParameters {
            commits_by_line?: Parameters.CommitsByLine;
            from?: Parameters.From;
            key: Parameters.Key;
            to?: Parameters.To;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetSourcesScmResponse;
        }
    }
    namespace GetSourcesShow {
        namespace Parameters {
            export type From = number;
            export type Key = string;
            export type To = number;
        }
        export interface QueryParameters {
            from?: Parameters.From;
            key: Parameters.Key;
            to?: Parameters.To;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetSourcesShowResponse;
        }
    }
    namespace GetSystemDbMigrationStatus {
        namespace Responses {
            export type $200 = Components.Schemas.GetSystemDbMigrationStatusResponse;
        }
    }
    namespace GetSystemHealth {
        namespace Responses {
            export type $200 = Components.Schemas.GetSystemHealthResponse;
        }
    }
    namespace GetSystemInfo {
        namespace Responses {
            export type $200 = Components.Schemas.GetSystemInfoResponse;
        }
    }
    namespace GetSystemLogs {
        namespace Parameters {
            export type Name = "access" | "app" | "ce" | "deprecation" | "es" | "web";
        }
        export interface QueryParameters {
            name?: Parameters.Name;
        }
    }
    namespace GetSystemStatus {
        namespace Responses {
            export type $200 = Components.Schemas.GetSystemStatusResponse;
        }
    }
    namespace GetSystemUpgrades {
        namespace Responses {
            export type $200 = Components.Schemas.GetSystemUpgradesResponse;
        }
    }
    namespace GetUserGroupsSearch {
        namespace Parameters {
            export type F = ("name" | "description" | "membersCount" | "managed")[];
            export type Managed = "true" | "false" | "yes" | "no";
            export type P = number;
            export type Ps = number;
            export type Q = string;
        }
        export interface QueryParameters {
            f?: Parameters.F;
            managed?: Parameters.Managed;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetUserGroupsSearchResponse;
        }
    }
    namespace GetUserGroupsUsers {
        namespace Parameters {
            export type Name = string;
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Selected = "all" | "deselected" | "selected";
        }
        export interface QueryParameters {
            name: Parameters.Name;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            selected?: Parameters.Selected;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetUserGroupsUsersResponse;
        }
    }
    namespace GetUserTokensSearch {
        namespace Parameters {
            export type Login = string;
        }
        export interface QueryParameters {
            login?: Parameters.Login;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetUserTokensSearchResponse;
        }
    }
    namespace GetUsersGroups {
        namespace Parameters {
            export type Login = string;
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type Selected = "all" | "deselected" | "selected";
        }
        export interface QueryParameters {
            login: Parameters.Login;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            selected?: Parameters.Selected;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetUsersGroupsResponse;
        }
    }
    namespace GetUsersSearch {
        namespace Parameters {
            export type Deactivated = "true" | "false" | "yes" | "no";
            export type ExternalIdentity = string;
            export type LastConnectedAfter = string;
            export type LastConnectedBefore = string;
            export type Managed = "true" | "false" | "yes" | "no";
            export type P = number;
            export type Ps = number;
            export type Q = string;
            export type SlLastConnectedAfter = string;
            export type SlLastConnectedBefore = string;
        }
        export interface QueryParameters {
            deactivated?: Parameters.Deactivated;
            externalIdentity?: Parameters.ExternalIdentity;
            lastConnectedAfter?: Parameters.LastConnectedAfter;
            lastConnectedBefore?: Parameters.LastConnectedBefore;
            managed?: Parameters.Managed;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            q?: Parameters.Q;
            slLastConnectedAfter?: Parameters.SlLastConnectedAfter;
            slLastConnectedBefore?: Parameters.SlLastConnectedBefore;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetUsersSearchResponse;
        }
    }
    namespace GetWebhooksDeliveries {
        namespace Parameters {
            export type CeTaskId = string;
            export type ComponentKey = string;
            export type P = number;
            export type Ps = number;
            export type Webhook = string;
        }
        export interface QueryParameters {
            ceTaskId?: Parameters.CeTaskId;
            componentKey?: Parameters.ComponentKey;
            p?: Parameters.P;
            ps?: Parameters.Ps;
            webhook?: Parameters.Webhook;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetWebhooksDeliveriesResponse;
        }
    }
    namespace GetWebhooksDelivery {
        namespace Parameters {
            export type DeliveryId = string;
        }
        export interface QueryParameters {
            deliveryId: Parameters.DeliveryId;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetWebhooksDeliveryResponse;
        }
    }
    namespace GetWebhooksList {
        namespace Parameters {
            export type Project = string;
        }
        export interface QueryParameters {
            project?: Parameters.Project;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetWebhooksListResponse;
        }
    }
    namespace GetWebservicesList {
        namespace Parameters {
            export type IncludeInternals = "true" | "false" | "yes" | "no";
        }
        export interface QueryParameters {
            include_internals?: Parameters.IncludeInternals;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetWebservicesListResponse;
        }
    }
    namespace GetWebservicesResponseExample {
        namespace Parameters {
            export type Action = string;
            export type Controller = string;
        }
        export interface QueryParameters {
            action: Parameters.Action;
            controller: Parameters.Controller;
        }
        namespace Responses {
            export type $200 = Components.Schemas.GetWebservicesResponseExampleResponse;
        }
    }
    namespace PostAlmIntegrationsImportAzureProject {
        export type RequestBody = Components.Schemas.PostAlmIntegrationsImportAzureProjectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmIntegrationsImportBitbucketcloudRepo {
        export type RequestBody = Components.Schemas.PostAlmIntegrationsImportBitbucketcloudRepoBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmIntegrationsImportBitbucketserverProject {
        export type RequestBody = Components.Schemas.PostAlmIntegrationsImportBitbucketserverProjectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmIntegrationsImportGithubProject {
        export type RequestBody = Components.Schemas.PostAlmIntegrationsImportGithubProjectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmIntegrationsImportGitlabProject {
        export type RequestBody = Components.Schemas.PostAlmIntegrationsImportGitlabProjectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmIntegrationsSetPat {
        export type RequestBody = Components.Schemas.PostAlmIntegrationsSetPatBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsCreateAzure {
        export type RequestBody = Components.Schemas.PostAlmSettingsCreateAzureBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsCreateBitbucket {
        export type RequestBody = Components.Schemas.PostAlmSettingsCreateBitbucketBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsCreateBitbucketcloud {
        export type RequestBody = Components.Schemas.PostAlmSettingsCreateBitbucketcloudBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsCreateGithub {
        export type RequestBody = Components.Schemas.PostAlmSettingsCreateGithubBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsCreateGitlab {
        export type RequestBody = Components.Schemas.PostAlmSettingsCreateGitlabBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsDelete {
        export type RequestBody = Components.Schemas.PostAlmSettingsDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsUpdateAzure {
        export type RequestBody = Components.Schemas.PostAlmSettingsUpdateAzureBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsUpdateBitbucket {
        export type RequestBody = Components.Schemas.PostAlmSettingsUpdateBitbucketBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsUpdateBitbucketcloud {
        export type RequestBody = Components.Schemas.PostAlmSettingsUpdateBitbucketcloudBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsUpdateGithub {
        export type RequestBody = Components.Schemas.PostAlmSettingsUpdateGithubBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAlmSettingsUpdateGitlab {
        export type RequestBody = Components.Schemas.PostAlmSettingsUpdateGitlabBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAuthenticationLogin {
        export type RequestBody = Components.Schemas.PostAuthenticationLoginBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostAuthenticationLogout {
        export type RequestBody = Components.Schemas.PostAuthenticationLogoutBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostFavoritesAdd {
        export type RequestBody = Components.Schemas.PostFavoritesAddBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostFavoritesRemove {
        export type RequestBody = Components.Schemas.PostFavoritesRemoveBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostHotspotsChangeStatus {
        export type RequestBody = Components.Schemas.PostHotspotsChangeStatusBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostIssuesAddComment {
        export type RequestBody = Components.Schemas.PostIssuesAddCommentBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesAddCommentResponse;
        }
    }
    namespace PostIssuesAssign {
        export type RequestBody = Components.Schemas.PostIssuesAssignBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesAssignResponse;
        }
    }
    namespace PostIssuesBulkChange {
        export type RequestBody = Components.Schemas.PostIssuesBulkChangeBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesBulkChangeResponse;
        }
    }
    namespace PostIssuesDeleteComment {
        export type RequestBody = Components.Schemas.PostIssuesDeleteCommentBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesDeleteCommentResponse;
        }
    }
    namespace PostIssuesDoTransition {
        export type RequestBody = Components.Schemas.PostIssuesDoTransitionBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesDoTransitionResponse;
        }
    }
    namespace PostIssuesEditComment {
        export type RequestBody = Components.Schemas.PostIssuesEditCommentBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesEditCommentResponse;
        }
    }
    namespace PostIssuesReindex {
        export type RequestBody = Components.Schemas.PostIssuesReindexBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostIssuesSetSeverity {
        export type RequestBody = Components.Schemas.PostIssuesSetSeverityBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesSetSeverityResponse;
        }
    }
    namespace PostIssuesSetTags {
        export type RequestBody = Components.Schemas.PostIssuesSetTagsBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesSetTagsResponse;
        }
    }
    namespace PostIssuesSetType {
        export type RequestBody = Components.Schemas.PostIssuesSetTypeBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostIssuesSetTypeResponse;
        }
    }
    namespace PostNewCodePeriodsSet {
        export type RequestBody = Components.Schemas.PostNewCodePeriodsSetBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostNewCodePeriodsUnset {
        export type RequestBody = Components.Schemas.PostNewCodePeriodsUnsetBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostNotificationsAdd {
        export type RequestBody = Components.Schemas.PostNotificationsAddBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostNotificationsRemove {
        export type RequestBody = Components.Schemas.PostNotificationsRemoveBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsAddGroup {
        export type RequestBody = Components.Schemas.PostPermissionsAddGroupBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsAddGroupToTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsAddGroupToTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsAddProjectCreatorToTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsAddProjectCreatorToTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsAddUser {
        export type RequestBody = Components.Schemas.PostPermissionsAddUserBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsAddUserToTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsAddUserToTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsApplyTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsApplyTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsBulkApplyTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsBulkApplyTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsCreateTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsCreateTemplateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostPermissionsCreateTemplateResponse;
        }
    }
    namespace PostPermissionsDeleteTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsDeleteTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsRemoveGroup {
        export type RequestBody = Components.Schemas.PostPermissionsRemoveGroupBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsRemoveGroupFromTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsRemoveGroupFromTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsRemoveProjectCreatorFromTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsRemoveProjectCreatorFromTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsRemoveUser {
        export type RequestBody = Components.Schemas.PostPermissionsRemoveUserBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsRemoveUserFromTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsRemoveUserFromTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsSetDefaultTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsSetDefaultTemplateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPermissionsUpdateTemplate {
        export type RequestBody = Components.Schemas.PostPermissionsUpdateTemplateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostPermissionsUpdateTemplateResponse;
        }
    }
    namespace PostPluginsCancelAll {
        export type RequestBody = Components.Schemas.PostPluginsCancelAllBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPluginsInstall {
        export type RequestBody = Components.Schemas.PostPluginsInstallBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPluginsUninstall {
        export type RequestBody = Components.Schemas.PostPluginsUninstallBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostPluginsUpdate {
        export type RequestBody = Components.Schemas.PostPluginsUpdateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectAnalysesCreateEvent {
        export type RequestBody = Components.Schemas.PostProjectAnalysesCreateEventBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostProjectAnalysesCreateEventResponse;
        }
    }
    namespace PostProjectAnalysesDelete {
        export type RequestBody = Components.Schemas.PostProjectAnalysesDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectAnalysesDeleteEvent {
        export type RequestBody = Components.Schemas.PostProjectAnalysesDeleteEventBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectAnalysesUpdateEvent {
        export type RequestBody = Components.Schemas.PostProjectAnalysesUpdateEventBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostProjectAnalysesUpdateEventResponse;
        }
    }
    namespace PostProjectBadgesRenewToken {
        export type RequestBody = Components.Schemas.PostProjectBadgesRenewTokenBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectBranchesDelete {
        export type RequestBody = Components.Schemas.PostProjectBranchesDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectBranchesRename {
        export type RequestBody = Components.Schemas.PostProjectBranchesRenameBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectBranchesSetAutomaticDeletionProtection {
        export type RequestBody = Components.Schemas.PostProjectBranchesSetAutomaticDeletionProtectionBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectBranchesSetMain {
        export type RequestBody = Components.Schemas.PostProjectBranchesSetMainBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectDumpExport {
        export type RequestBody = Components.Schemas.PostProjectDumpExportBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostProjectDumpExportResponse;
        }
    }
    namespace PostProjectLinksCreate {
        export type RequestBody = Components.Schemas.PostProjectLinksCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostProjectLinksCreateResponse;
        }
    }
    namespace PostProjectLinksDelete {
        export type RequestBody = Components.Schemas.PostProjectLinksDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectTagsSet {
        export type RequestBody = Components.Schemas.PostProjectTagsSetBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectsBulkDelete {
        export type RequestBody = Components.Schemas.PostProjectsBulkDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectsCreate {
        export type RequestBody = Components.Schemas.PostProjectsCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostProjectsCreateResponse;
        }
    }
    namespace PostProjectsDelete {
        export type RequestBody = Components.Schemas.PostProjectsDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectsUpdateKey {
        export type RequestBody = Components.Schemas.PostProjectsUpdateKeyBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostProjectsUpdateVisibility {
        export type RequestBody = Components.Schemas.PostProjectsUpdateVisibilityBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesAddGroup {
        export type RequestBody = Components.Schemas.PostQualitygatesAddGroupBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesAddUser {
        export type RequestBody = Components.Schemas.PostQualitygatesAddUserBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesCopy {
        export type RequestBody = Components.Schemas.PostQualitygatesCopyBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesCreate {
        export type RequestBody = Components.Schemas.PostQualitygatesCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostQualitygatesCreateResponse;
        }
    }
    namespace PostQualitygatesCreateCondition {
        export type RequestBody = Components.Schemas.PostQualitygatesCreateConditionBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostQualitygatesCreateConditionResponse;
        }
    }
    namespace PostQualitygatesDeleteCondition {
        export type RequestBody = Components.Schemas.PostQualitygatesDeleteConditionBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesDeselect {
        export type RequestBody = Components.Schemas.PostQualitygatesDeselectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesDestroy {
        export type RequestBody = Components.Schemas.PostQualitygatesDestroyBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesRemoveGroup {
        export type RequestBody = Components.Schemas.PostQualitygatesRemoveGroupBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesRemoveUser {
        export type RequestBody = Components.Schemas.PostQualitygatesRemoveUserBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesRename {
        export type RequestBody = Components.Schemas.PostQualitygatesRenameBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesSelect {
        export type RequestBody = Components.Schemas.PostQualitygatesSelectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesSetAsDefault {
        export type RequestBody = Components.Schemas.PostQualitygatesSetAsDefaultBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualitygatesUpdateCondition {
        export type RequestBody = Components.Schemas.PostQualitygatesUpdateConditionBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesActivateRule {
        export type RequestBody = Components.Schemas.PostQualityprofilesActivateRuleBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesActivateRules {
        export type RequestBody = Components.Schemas.PostQualityprofilesActivateRulesBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesAddProject {
        export type RequestBody = Components.Schemas.PostQualityprofilesAddProjectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesChangeParent {
        export type RequestBody = Components.Schemas.PostQualityprofilesChangeParentBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesCopy {
        export type RequestBody = Components.Schemas.PostQualityprofilesCopyBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostQualityprofilesCopyResponse;
        }
    }
    namespace PostQualityprofilesCreate {
        export type RequestBody = Components.Schemas.PostQualityprofilesCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostQualityprofilesCreateResponse;
        }
    }
    namespace PostQualityprofilesDeactivateRule {
        export type RequestBody = Components.Schemas.PostQualityprofilesDeactivateRuleBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesDeactivateRules {
        export type RequestBody = Components.Schemas.PostQualityprofilesDeactivateRulesBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesDelete {
        export type RequestBody = Components.Schemas.PostQualityprofilesDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesRemoveProject {
        export type RequestBody = Components.Schemas.PostQualityprofilesRemoveProjectBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesRename {
        export type RequestBody = Components.Schemas.PostQualityprofilesRenameBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesRestore {
        export type RequestBody = Components.Schemas.PostQualityprofilesRestoreBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostQualityprofilesSetDefault {
        export type RequestBody = Components.Schemas.PostQualityprofilesSetDefaultBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostRulesCreate {
        export type RequestBody = Components.Schemas.PostRulesCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostRulesCreateResponse;
        }
    }
    namespace PostRulesDelete {
        export type RequestBody = Components.Schemas.PostRulesDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostRulesUpdate {
        export type RequestBody = Components.Schemas.PostRulesUpdateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostRulesUpdateResponse;
        }
    }
    namespace PostSettingsReset {
        export type RequestBody = Components.Schemas.PostSettingsResetBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostSettingsSet {
        export type RequestBody = Components.Schemas.PostSettingsSetBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostSystemChangeLogLevel {
        export type RequestBody = Components.Schemas.PostSystemChangeLogLevelBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostSystemMigrateDb {
        export type RequestBody = Components.Schemas.PostSystemMigrateDbBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostSystemMigrateDbResponse;
        }
    }
    namespace PostSystemRestart {
        export type RequestBody = Components.Schemas.PostSystemRestartBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUserGroupsAddUser {
        export type RequestBody = Components.Schemas.PostUserGroupsAddUserBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUserGroupsCreate {
        export type RequestBody = Components.Schemas.PostUserGroupsCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostUserGroupsCreateResponse;
        }
    }
    namespace PostUserGroupsDelete {
        export type RequestBody = Components.Schemas.PostUserGroupsDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUserGroupsRemoveUser {
        export type RequestBody = Components.Schemas.PostUserGroupsRemoveUserBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUserGroupsUpdate {
        export type RequestBody = Components.Schemas.PostUserGroupsUpdateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUserTokensGenerate {
        export type RequestBody = Components.Schemas.PostUserTokensGenerateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostUserTokensGenerateResponse;
        }
    }
    namespace PostUserTokensRevoke {
        export type RequestBody = Components.Schemas.PostUserTokensRevokeBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUsersAnonymize {
        export type RequestBody = Components.Schemas.PostUsersAnonymizeBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUsersChangePassword {
        export type RequestBody = Components.Schemas.PostUsersChangePasswordBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUsersCreate {
        export type RequestBody = Components.Schemas.PostUsersCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostUsersCreateResponse;
        }
    }
    namespace PostUsersDeactivate {
        export type RequestBody = Components.Schemas.PostUsersDeactivateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostUsersDeactivateResponse;
        }
    }
    namespace PostUsersUpdate {
        export type RequestBody = Components.Schemas.PostUsersUpdateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostUsersUpdateResponse;
        }
    }
    namespace PostUsersUpdateIdentityProvider {
        export type RequestBody = Components.Schemas.PostUsersUpdateIdentityProviderBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostUsersUpdateLogin {
        export type RequestBody = Components.Schemas.PostUsersUpdateLoginBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostWebhooksCreate {
        export type RequestBody = Components.Schemas.PostWebhooksCreateBody;
        namespace Responses {
            export type $200 = Components.Schemas.PostWebhooksCreateResponse;
        }
    }
    namespace PostWebhooksDelete {
        export type RequestBody = Components.Schemas.PostWebhooksDeleteBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
    namespace PostWebhooksUpdate {
        export type RequestBody = Components.Schemas.PostWebhooksUpdateBody;
        namespace Responses {
            export interface $204 {
            }
        }
    }
}

export interface OperationMethods {
  /**
   * postAlmIntegrationsImportAzureProject - Create a SonarQube project with the information from the provided Azure DevOps project.<br/>Autoconfigure pull request decoration mechanism.<br/>Requires the 'Create Projects' permission
   */
  'postAlmIntegrationsImportAzureProject'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmIntegrationsImportAzureProject.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmIntegrationsImportAzureProject.Responses.$204>
  /**
   * postAlmIntegrationsImportBitbucketcloudRepo - Create a SonarQube project with the information from the provided Bitbucket Cloud repository.<br/>Autoconfigure pull request decoration mechanism.<br/>Requires the 'Create Projects' permission
   */
  'postAlmIntegrationsImportBitbucketcloudRepo'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmIntegrationsImportBitbucketcloudRepo.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmIntegrationsImportBitbucketcloudRepo.Responses.$204>
  /**
   * postAlmIntegrationsImportBitbucketserverProject - Create a SonarQube project with the information from the provided BitbucketServer project.<br/>Autoconfigure pull request decoration mechanism.<br/>Requires the 'Create Projects' permission
   */
  'postAlmIntegrationsImportBitbucketserverProject'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmIntegrationsImportBitbucketserverProject.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmIntegrationsImportBitbucketserverProject.Responses.$204>
  /**
   * postAlmIntegrationsImportGithubProject - Create a SonarQube project with the information from the provided GitHub repository.<br/>Autoconfigure pull request decoration mechanism. If Automatic Provisioning is enable for GitHub, it will also synchronize permissions from the repository.<br/>Requires the 'Create Projects' permission
   */
  'postAlmIntegrationsImportGithubProject'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmIntegrationsImportGithubProject.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmIntegrationsImportGithubProject.Responses.$204>
  /**
   * postAlmIntegrationsImportGitlabProject - Import a GitLab project to SonarQube, creating a new project and configuring MR decoration<br/>Requires the 'Create Projects' permission
   */
  'postAlmIntegrationsImportGitlabProject'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmIntegrationsImportGitlabProject.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmIntegrationsImportGitlabProject.Responses.$204>
  /**
   * getAlmIntegrationsListAzureProjects - List Azure projects<br/>Requires the 'Create Projects' permission
   */
  'getAlmIntegrationsListAzureProjects'(
    parameters?: Parameters<Paths.GetAlmIntegrationsListAzureProjects.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmIntegrationsListAzureProjects.Responses.$200>
  /**
   * getAlmIntegrationsListBitbucketserverProjects - List the Bitbucket Server projects<br/>Requires the 'Create Projects' permission
   */
  'getAlmIntegrationsListBitbucketserverProjects'(
    parameters?: Parameters<Paths.GetAlmIntegrationsListBitbucketserverProjects.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmIntegrationsListBitbucketserverProjects.Responses.$200>
  /**
   * getAlmIntegrationsSearchAzureRepos - Search the Azure repositories<br/>Requires the 'Create Projects' permission
   */
  'getAlmIntegrationsSearchAzureRepos'(
    parameters?: Parameters<Paths.GetAlmIntegrationsSearchAzureRepos.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmIntegrationsSearchAzureRepos.Responses.$200>
  /**
   * getAlmIntegrationsSearchBitbucketcloudRepos - Search the Bitbucket Cloud repositories<br/>Requires the 'Create Projects' permission
   */
  'getAlmIntegrationsSearchBitbucketcloudRepos'(
    parameters?: Parameters<Paths.GetAlmIntegrationsSearchBitbucketcloudRepos.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmIntegrationsSearchBitbucketcloudRepos.Responses.$200>
  /**
   * getAlmIntegrationsSearchBitbucketserverRepos - Search the Bitbucket Server repositories with REPO_ADMIN access<br/>Requires the 'Create Projects' permission
   */
  'getAlmIntegrationsSearchBitbucketserverRepos'(
    parameters?: Parameters<Paths.GetAlmIntegrationsSearchBitbucketserverRepos.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmIntegrationsSearchBitbucketserverRepos.Responses.$200>
  /**
   * getAlmIntegrationsSearchGitlabRepos - Search the GitLab projects.<br/>Requires the 'Create Projects' permission
   */
  'getAlmIntegrationsSearchGitlabRepos'(
    parameters?: Parameters<Paths.GetAlmIntegrationsSearchGitlabRepos.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmIntegrationsSearchGitlabRepos.Responses.$200>
  /**
   * postAlmIntegrationsSetPat - Set a Personal Access Token for the given DevOps Platform setting<br/>Requires the 'Create Projects' permission
   */
  'postAlmIntegrationsSetPat'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmIntegrationsSetPat.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmIntegrationsSetPat.Responses.$204>
  /**
   * getAlmSettingsCountBinding - Count number of project bound to an DevOps Platform setting.<br/>Requires the 'Administer System' permission
   */
  'getAlmSettingsCountBinding'(
    parameters?: Parameters<Paths.GetAlmSettingsCountBinding.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmSettingsCountBinding.Responses.$200>
  /**
   * postAlmSettingsCreateAzure - Create Azure instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsCreateAzure'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsCreateAzure.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsCreateAzure.Responses.$204>
  /**
   * postAlmSettingsCreateBitbucket - Create Bitbucket instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsCreateBitbucket'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsCreateBitbucket.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsCreateBitbucket.Responses.$204>
  /**
   * postAlmSettingsCreateBitbucketcloud - Configure a new instance of Bitbucket Cloud. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsCreateBitbucketcloud'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsCreateBitbucketcloud.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsCreateBitbucketcloud.Responses.$204>
  /**
   * postAlmSettingsCreateGithub - Create GitHub instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsCreateGithub'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsCreateGithub.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsCreateGithub.Responses.$204>
  /**
   * postAlmSettingsCreateGitlab - Create GitLab instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsCreateGitlab'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsCreateGitlab.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsCreateGitlab.Responses.$204>
  /**
   * postAlmSettingsDelete - Delete an DevOps Platform Setting.<br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsDelete.Responses.$204>
  /**
   * getAlmSettingsGetBinding - Get DevOps Platform binding of a given project.<br/>Requires the 'Browse' permission on the project
   */
  'getAlmSettingsGetBinding'(
    parameters?: Parameters<Paths.GetAlmSettingsGetBinding.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmSettingsGetBinding.Responses.$200>
  /**
   * getAlmSettingsList - List DevOps Platform setting available for a given project, sorted by DevOps Platform key<br/>Requires the 'Administer project' permission if the 'project' parameter is provided, requires the 'Create Projects' permission otherwise.
   */
  'getAlmSettingsList'(
    parameters?: Parameters<Paths.GetAlmSettingsList.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmSettingsList.Responses.$200>
  /**
   * getAlmSettingsListDefinitions - List DevOps Platform Settings, sorted by created date.<br/>Requires the 'Administer System' permission
   */
  'getAlmSettingsListDefinitions'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmSettingsListDefinitions.Responses.$200>
  /**
   * postAlmSettingsUpdateAzure - Update Azure instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsUpdateAzure'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsUpdateAzure.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsUpdateAzure.Responses.$204>
  /**
   * postAlmSettingsUpdateBitbucket - Update Bitbucket instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsUpdateBitbucket'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsUpdateBitbucket.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsUpdateBitbucket.Responses.$204>
  /**
   * postAlmSettingsUpdateBitbucketcloud - Update Bitbucket Cloud Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsUpdateBitbucketcloud'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsUpdateBitbucketcloud.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsUpdateBitbucketcloud.Responses.$204>
  /**
   * postAlmSettingsUpdateGithub - Update GitHub instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsUpdateGithub'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsUpdateGithub.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsUpdateGithub.Responses.$204>
  /**
   * postAlmSettingsUpdateGitlab - Update GitLab instance Setting. <br/>Requires the 'Administer System' permission
   */
  'postAlmSettingsUpdateGitlab'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAlmSettingsUpdateGitlab.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAlmSettingsUpdateGitlab.Responses.$204>
  /**
   * getAlmSettingsValidate - Validate an DevOps Platform Setting by checking connectivity and permissions<br/>Requires the 'Administer System' permission
   */
  'getAlmSettingsValidate'(
    parameters?: Parameters<Paths.GetAlmSettingsValidate.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAlmSettingsValidate.Responses.$200>
  /**
   * getAnalysisCacheGet - Get the scanner's cached data for a branch. Requires scan permission on the project. Data is returned gzipped if the corresponding 'Accept-Encoding' header is set in the request.
   */
  'getAnalysisCacheGet'(
    parameters?: Parameters<Paths.GetAnalysisCacheGet.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAnalysisCacheGet.Responses.$204>
  /**
   * postAuthenticationLogin - Authenticate a user.
   */
  'postAuthenticationLogin'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAuthenticationLogin.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAuthenticationLogin.Responses.$204>
  /**
   * postAuthenticationLogout - Logout a user.
   */
  'postAuthenticationLogout'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostAuthenticationLogout.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostAuthenticationLogout.Responses.$204>
  /**
   * getAuthenticationValidate - Check credentials.
   */
  'getAuthenticationValidate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetAuthenticationValidate.Responses.$200>
  /**
   * getCeActivity - Search for tasks.<br> Requires the system administration permission, or project administration permission if component is set.
   */
  'getCeActivity'(
    parameters?: Parameters<Paths.GetCeActivity.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCeActivity.Responses.$200>
  /**
   * getCeActivityStatus - Returns CE activity related metrics.<br>Requires 'Administer System' permission or 'Administer' rights on the specified project.
   */
  'getCeActivityStatus'(
    parameters?: Parameters<Paths.GetCeActivityStatus.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCeActivityStatus.Responses.$200>
  /**
   * getCeComponent - Get the pending tasks, in-progress tasks and the last executed task of a given component (usually a project).<br>Requires the following permission: 'Browse' on the specified component.
   */
  'getCeComponent'(
    parameters?: Parameters<Paths.GetCeComponent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCeComponent.Responses.$200>
  /**
   * getCeTask - Give Compute Engine task details such as type, status, duration and associated component.<br/>Requires one of the following permissions: <ul><li>'Administer' at global or project level</li><li>'Execute Analysis' at global or project level</li></ul>Since 6.1, field "logs" is deprecated and its value is always false.
   */
  'getCeTask'(
    parameters?: Parameters<Paths.GetCeTask.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetCeTask.Responses.$200>
  /**
   * getComponentsSearch - Search for components
   */
  'getComponentsSearch'(
    parameters?: Parameters<Paths.GetComponentsSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetComponentsSearch.Responses.$200>
  /**
   * getComponentsShow - Returns a component (file, directory, project, portfolio…) and its ancestors. The ancestors are ordered from the parent to the root project. Requires the following permission: 'Browse' on the project of the specified component.
   */
  'getComponentsShow'(
    parameters?: Parameters<Paths.GetComponentsShow.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetComponentsShow.Responses.$200>
  /**
   * getComponentsTree - Navigate through components based on the chosen strategy.<br>Requires the following permission: 'Browse' on the specified project.<br>When limiting search with the q parameter, directories are not returned.
   */
  'getComponentsTree'(
    parameters?: Parameters<Paths.GetComponentsTree.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetComponentsTree.Responses.$200>
  /**
   * getDuplicationsShow - Get duplications. Require Browse permission on file's project
   */
  'getDuplicationsShow'(
    parameters?: Parameters<Paths.GetDuplicationsShow.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetDuplicationsShow.Responses.$200>
  /**
   * postFavoritesAdd - Add a component (project, portfolio, etc.) as favorite for the authenticated user.<br>Only 100 components by qualifier can be added as favorite.<br>Requires authentication and the following permission: 'Browse' on the component.
   */
  'postFavoritesAdd'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostFavoritesAdd.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostFavoritesAdd.Responses.$204>
  /**
   * postFavoritesRemove - Remove a component (project, portfolio, application etc.) as favorite for the authenticated user.<br>Requires authentication.
   */
  'postFavoritesRemove'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostFavoritesRemove.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostFavoritesRemove.Responses.$204>
  /**
   * getFavoritesSearch - Search for the authenticated user favorites.<br>Requires authentication.
   */
  'getFavoritesSearch'(
    parameters?: Parameters<Paths.GetFavoritesSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetFavoritesSearch.Responses.$200>
  /**
   * postHotspotsChangeStatus - Change the status of a Security Hotpot.<br/>Requires the 'Administer Security Hotspot' permission.
   */
  'postHotspotsChangeStatus'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostHotspotsChangeStatus.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostHotspotsChangeStatus.Responses.$204>
  /**
   * getHotspotsSearch - Search for Security Hotpots. <br>Requires the 'Browse' permission on the specified project(s). <br>For applications, it also requires 'Browse' permission on its child projects. <br>When issue indexing is in progress returns 503 service unavailable HTTP code.
   */
  'getHotspotsSearch'(
    parameters?: Parameters<Paths.GetHotspotsSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetHotspotsSearch.Responses.$200>
  /**
   * getHotspotsShow - Provides the details of a Security Hotspot.
   */
  'getHotspotsShow'(
    parameters?: Parameters<Paths.GetHotspotsShow.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetHotspotsShow.Responses.$200>
  /**
   * postIssuesAddComment - Add a comment.<br/>Requires authentication and the following permission: 'Browse' on the project of the specified issue.
   */
  'postIssuesAddComment'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesAddComment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesAddComment.Responses.$200>
  /**
   * postIssuesAssign - Assign/Unassign an issue. Requires authentication and Browse permission on project
   */
  'postIssuesAssign'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesAssign.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesAssign.Responses.$200>
  /**
   * getIssuesAuthors - Search SCM accounts which match a given query.<br/>Requires authentication.<br/>When issue indexing is in progress returns 503 service unavailable HTTP code.
   */
  'getIssuesAuthors'(
    parameters?: Parameters<Paths.GetIssuesAuthors.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetIssuesAuthors.Responses.$200>
  /**
   * postIssuesBulkChange - Bulk change on issues. Up to 500 issues can be updated. <br/>Requires authentication.
   */
  'postIssuesBulkChange'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesBulkChange.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesBulkChange.Responses.$200>
  /**
   * getIssuesChangelog - Display changelog of an issue.<br/>Requires the 'Browse' permission on the project of the specified issue.
   */
  'getIssuesChangelog'(
    parameters?: Parameters<Paths.GetIssuesChangelog.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetIssuesChangelog.Responses.$200>
  /**
   * postIssuesDeleteComment - Delete a comment.<br/>Requires authentication and the following permission: 'Browse' on the project of the specified issue.
   */
  'postIssuesDeleteComment'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesDeleteComment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesDeleteComment.Responses.$200>
  /**
   * postIssuesDoTransition - Do workflow transition on an issue. Requires authentication and Browse permission on project.<br/>
   * The transitions 'accept', 'wontfix' and 'falsepositive' require the permission 'Administer Issues'.<br/>
   * The transitions involving security hotspots require the permission 'Administer Security Hotspot'.
   * 
   */
  'postIssuesDoTransition'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesDoTransition.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesDoTransition.Responses.$200>
  /**
   * postIssuesEditComment - Edit a comment.<br/>Requires authentication and the following permission: 'Browse' on the project of the specified issue.
   */
  'postIssuesEditComment'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesEditComment.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesEditComment.Responses.$200>
  /**
   * postIssuesReindex - Reindex issues for a project.<br> Require 'Administer System' permission.
   */
  'postIssuesReindex'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesReindex.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesReindex.Responses.$204>
  /**
   * getIssuesSearch - Search for issues.<br>Requires the 'Browse' permission on the specified project(s). <br>For applications, it also requires 'Browse' permission on its child projects.<br/>When issue indexing is in progress returns 503 service unavailable HTTP code.
   */
  'getIssuesSearch'(
    parameters?: Parameters<Paths.GetIssuesSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetIssuesSearch.Responses.$200>
  /**
   * postIssuesSetSeverity - Change severity.<br/>Requires the following permissions:<ul>  <li>'Authentication'</li>  <li>'Browse' rights on project of the specified issue</li>  <li>'Administer Issues' rights on project of the specified issue</li></ul>
   */
  'postIssuesSetSeverity'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesSetSeverity.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesSetSeverity.Responses.$200>
  /**
   * postIssuesSetTags - Set tags on an issue. <br/>Requires authentication and Browse permission on project
   */
  'postIssuesSetTags'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesSetTags.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesSetTags.Responses.$200>
  /**
   * postIssuesSetType - Change type of issue, for instance from 'code smell' to 'bug'.<br/>Requires the following permissions:<ul>  <li>'Authentication'</li>  <li>'Browse' rights on project of the specified issue</li>  <li>'Administer Issues' rights on project of the specified issue</li></ul>
   */
  'postIssuesSetType'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostIssuesSetType.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostIssuesSetType.Responses.$200>
  /**
   * getIssuesTags - List tags matching a given query
   */
  'getIssuesTags'(
    parameters?: Parameters<Paths.GetIssuesTags.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetIssuesTags.Responses.$200>
  /**
   * getLanguagesList - List supported programming languages
   */
  'getLanguagesList'(
    parameters?: Parameters<Paths.GetLanguagesList.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetLanguagesList.Responses.$200>
  /**
   * getMeasuresComponent - Return component with specified measures.<br>Requires the following permission: 'Browse' on the project of specified component.
   */
  'getMeasuresComponent'(
    parameters?: Parameters<Paths.GetMeasuresComponent.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetMeasuresComponent.Responses.$200>
  /**
   * getMeasuresComponentTree - Navigate through components based on the chosen strategy with specified measures.<br>Requires the following permission: 'Browse' on the specified project.<br>For applications, it also requires 'Browse' permission on its child projects. <br>When limiting search with the q parameter, directories are not returned.
   */
  'getMeasuresComponentTree'(
    parameters?: Parameters<Paths.GetMeasuresComponentTree.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetMeasuresComponentTree.Responses.$200>
  /**
   * getMeasuresSearchHistory - Search measures history of a component.<br>Measures are ordered chronologically.<br>Pagination applies to the number of measures for each metric.<br>Requires the following permission: 'Browse' on the specified component. <br>For applications, it also requires 'Browse' permission on its child projects.
   */
  'getMeasuresSearchHistory'(
    parameters?: Parameters<Paths.GetMeasuresSearchHistory.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetMeasuresSearchHistory.Responses.$200>
  /**
   * getMetricsSearch - Search for metrics
   */
  'getMetricsSearch'(
    parameters?: Parameters<Paths.GetMetricsSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetMetricsSearch.Responses.$200>
  /**
   * getMetricsTypes - List all available metric types.
   */
  'getMetricsTypes'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetMetricsTypes.Responses.$200>
  /**
   * getMonitoringMetrics - Return monitoring metrics in Prometheus format. 
   * Support content type 'text/plain' (default) and 'application/openmetrics-text'.
   * this endpoint can be access using a Bearer token, that needs to be defined in sonar.properties with the 'sonar.web.systemPasscode' key.
   */
  'getMonitoringMetrics'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getNewCodePeriodsList - Lists the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a> for all branches in a project.<br>Requires the permission to browse the project
   */
  'getNewCodePeriodsList'(
    parameters?: Parameters<Paths.GetNewCodePeriodsList.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetNewCodePeriodsList.Responses.$200>
  /**
   * postNewCodePeriodsSet - Updates the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a> on different levels:<br><ul><li>Not providing a project key and a branch key will update the default value at global level. Existing projects or branches having a specific new code definition will not be impacted</li><li>Project key must be provided to update the value for a project</li><li>Both project and branch keys must be provided to update the value for a branch</li></ul>Requires one of the following permissions: <ul><li>'Administer System' to change the global setting</li><li>'Administer' rights on the specified project to change the project setting</li></ul>
   */
  'postNewCodePeriodsSet'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostNewCodePeriodsSet.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostNewCodePeriodsSet.Responses.$204>
  /**
   * getNewCodePeriodsShow - Shows the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a>.<br> If the component requested doesn't exist or if no new code definition is set for it, a value is inherited from the project or from the global setting.Requires one of the following permissions if a component is specified: <ul><li>'Administer' rights on the specified component</li><li>'Execute analysis' rights on the specified component</li></ul>
   */
  'getNewCodePeriodsShow'(
    parameters?: Parameters<Paths.GetNewCodePeriodsShow.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetNewCodePeriodsShow.Responses.$200>
  /**
   * postNewCodePeriodsUnset - Unsets the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a> for a branch, project or global. It requires the inherited New Code Definition to be compatible with the Clean as You Code methodology, and one of the following permissions: <ul><li>'Administer System' to change the global setting</li><li>'Administer' rights for a specified component</li></ul>
   */
  'postNewCodePeriodsUnset'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostNewCodePeriodsUnset.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostNewCodePeriodsUnset.Responses.$204>
  /**
   * postNotificationsAdd - Add a notification for the authenticated user.<br>Requires one of the following permissions:<ul> <li>Authentication if no login is provided. If a project is provided, requires the 'Browse' permission on the specified project.</li> <li>System administration if a login is provided. If a project is provided, requires the 'Browse' permission on the specified project.</li></ul>
   */
  'postNotificationsAdd'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostNotificationsAdd.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostNotificationsAdd.Responses.$204>
  /**
   * getNotificationsList - List notifications of the authenticated user.<br>Requires one of the following permissions:<ul>  <li>Authentication if no login is provided</li>  <li>System administration if a login is provided</li></ul>
   */
  'getNotificationsList'(
    parameters?: Parameters<Paths.GetNotificationsList.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetNotificationsList.Responses.$200>
  /**
   * postNotificationsRemove - Remove a notification for the authenticated user.<br>Requires one of the following permissions:<ul>  <li>Authentication if no login is provided</li>  <li>System administration if a login is provided</li></ul>
   */
  'postNotificationsRemove'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostNotificationsRemove.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostNotificationsRemove.Responses.$204>
  /**
   * postPermissionsAddGroup - Add a permission to a group.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br /> The group name must be provided. <br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
   */
  'postPermissionsAddGroup'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsAddGroup.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsAddGroup.Responses.$204>
  /**
   * postPermissionsAddGroupToTemplate - Add a group to a permission template.<br /> The group name must be provided. <br />Requires the following permission: 'Administer System'.
   */
  'postPermissionsAddGroupToTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsAddGroupToTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsAddGroupToTemplate.Responses.$204>
  /**
   * postPermissionsAddProjectCreatorToTemplate - Add a project creator to a permission template.<br>Requires the following permission: 'Administer System'.
   */
  'postPermissionsAddProjectCreatorToTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsAddProjectCreatorToTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsAddProjectCreatorToTemplate.Responses.$204>
  /**
   * postPermissionsAddUser - Add permission to a user.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
   */
  'postPermissionsAddUser'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsAddUser.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsAddUser.Responses.$204>
  /**
   * postPermissionsAddUserToTemplate - Add a user to a permission template.<br /> Requires the following permission: 'Administer System'.
   */
  'postPermissionsAddUserToTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsAddUserToTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsAddUserToTemplate.Responses.$204>
  /**
   * postPermissionsApplyTemplate - Apply a permission template to one project.<br>The project id or project key must be provided.<br>The template id or name must be provided.<br>Requires the following permission: 'Administer System'.
   */
  'postPermissionsApplyTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsApplyTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsApplyTemplate.Responses.$204>
  /**
   * postPermissionsBulkApplyTemplate - Apply a permission template to several components. Managed projects will be ignored.<br />The template id or name must be provided.<br />Requires the following permission: 'Administer System'.
   */
  'postPermissionsBulkApplyTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsBulkApplyTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsBulkApplyTemplate.Responses.$204>
  /**
   * postPermissionsCreateTemplate - Create a permission template.<br />Requires the following permission: 'Administer System'.
   */
  'postPermissionsCreateTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsCreateTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsCreateTemplate.Responses.$200>
  /**
   * postPermissionsDeleteTemplate - Delete a permission template.<br />Requires the following permission: 'Administer System'.
   */
  'postPermissionsDeleteTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsDeleteTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsDeleteTemplate.Responses.$204>
  /**
   * postPermissionsRemoveGroup - Remove a permission from a group.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br /> The group name must be provided.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
   */
  'postPermissionsRemoveGroup'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsRemoveGroup.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsRemoveGroup.Responses.$204>
  /**
   * postPermissionsRemoveGroupFromTemplate - Remove a group from a permission template.<br /> The group name must be provided. <br />Requires the following permission: 'Administer System'.
   */
  'postPermissionsRemoveGroupFromTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsRemoveGroupFromTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsRemoveGroupFromTemplate.Responses.$204>
  /**
   * postPermissionsRemoveProjectCreatorFromTemplate - Remove a project creator from a permission template.<br>Requires the following permission: 'Administer System'.
   */
  'postPermissionsRemoveProjectCreatorFromTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsRemoveProjectCreatorFromTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsRemoveProjectCreatorFromTemplate.Responses.$204>
  /**
   * postPermissionsRemoveUser - Remove permission from a user.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br /> Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
   */
  'postPermissionsRemoveUser'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsRemoveUser.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsRemoveUser.Responses.$204>
  /**
   * postPermissionsRemoveUserFromTemplate - Remove a user from a permission template.<br /> Requires the following permission: 'Administer System'.
   */
  'postPermissionsRemoveUserFromTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsRemoveUserFromTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsRemoveUserFromTemplate.Responses.$204>
  /**
   * getPermissionsSearchTemplates - List permission templates.<br />Requires the following permission: 'Administer System'.
   */
  'getPermissionsSearchTemplates'(
    parameters?: Parameters<Paths.GetPermissionsSearchTemplates.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPermissionsSearchTemplates.Responses.$200>
  /**
   * postPermissionsSetDefaultTemplate - Set a permission template as default.<br />Requires the following permission: 'Administer System'.
   */
  'postPermissionsSetDefaultTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsSetDefaultTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsSetDefaultTemplate.Responses.$204>
  /**
   * postPermissionsUpdateTemplate - Update a permission template.<br />Requires the following permission: 'Administer System'.
   */
  'postPermissionsUpdateTemplate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPermissionsUpdateTemplate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPermissionsUpdateTemplate.Responses.$200>
  /**
   * getPluginsAvailable - Get the list of all the plugins available for installation on the SonarQube instance, sorted by plugin name.<br/>Plugin information is retrieved from Update Center. Date and time at which Update Center was last refreshed is provided in the response.<br/>Update status values are: <ul><li>COMPATIBLE: plugin is compatible with current SonarQube instance.</li><li>INCOMPATIBLE: plugin is not compatible with current SonarQube instance.</li><li>REQUIRES_SYSTEM_UPGRADE: plugin requires SonarQube to be upgraded before being installed.</li><li>DEPS_REQUIRE_SYSTEM_UPGRADE: at least one plugin on which the plugin is dependent requires SonarQube to be upgraded.</li></ul>Require 'Administer System' permission.
   */
  'getPluginsAvailable'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPluginsAvailable.Responses.$200>
  /**
   * postPluginsCancelAll - Cancels any operation pending on any plugin (install, update or uninstall)<br/>Requires user to be authenticated with Administer System permissions
   */
  'postPluginsCancelAll'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPluginsCancelAll.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPluginsCancelAll.Responses.$204>
  /**
   * postPluginsInstall - Installs the latest version of a plugin specified by its key.<br/>Plugin information is retrieved from Update Center.<br/>Fails if used on commercial editions or plugin risk consent has not been accepted.<br/>Requires user to be authenticated with Administer System permissions
   */
  'postPluginsInstall'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPluginsInstall.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPluginsInstall.Responses.$204>
  /**
   * getPluginsInstalled - Get the list of all the plugins installed on the SonarQube instance, sorted by plugin name.<br/>Requires authentication.
   */
  'getPluginsInstalled'(
    parameters?: Parameters<Paths.GetPluginsInstalled.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPluginsInstalled.Responses.$200>
  /**
   * getPluginsPending - Get the list of plugins which will either be installed or removed at the next startup of the SonarQube instance, sorted by plugin name.<br/>Require 'Administer System' permission.
   */
  'getPluginsPending'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPluginsPending.Responses.$200>
  /**
   * postPluginsUninstall - Uninstalls the plugin specified by its key.<br/>Requires user to be authenticated with Administer System permissions.
   */
  'postPluginsUninstall'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPluginsUninstall.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPluginsUninstall.Responses.$204>
  /**
   * postPluginsUpdate - Updates a plugin specified by its key to the latest version compatible with the SonarQube instance.<br/>Plugin information is retrieved from Update Center.<br/>Requires user to be authenticated with Administer System permissions
   */
  'postPluginsUpdate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostPluginsUpdate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostPluginsUpdate.Responses.$204>
  /**
   * getPluginsUpdates - Lists plugins installed on the SonarQube instance for which at least one newer version is available, sorted by plugin name.<br/>Each newer version is listed, ordered from the oldest to the newest, with its own update/compatibility status.<br/>Plugin information is retrieved from Update Center. Date and time at which Update Center was last refreshed is provided in the response.<br/>Update status values are: [COMPATIBLE, INCOMPATIBLE, REQUIRES_UPGRADE, DEPS_REQUIRE_UPGRADE].<br/>Require 'Administer System' permission.
   */
  'getPluginsUpdates'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetPluginsUpdates.Responses.$200>
  /**
   * postProjectAnalysesCreateEvent - Create a project analysis event.<br>Only event of category 'VERSION' and 'OTHER' can be created.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the specified project</li></ul>
   */
  'postProjectAnalysesCreateEvent'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectAnalysesCreateEvent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectAnalysesCreateEvent.Responses.$200>
  /**
   * postProjectAnalysesDelete - Delete a project analysis.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the project of the specified analysis</li></ul>
   */
  'postProjectAnalysesDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectAnalysesDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectAnalysesDelete.Responses.$204>
  /**
   * postProjectAnalysesDeleteEvent - Delete a project analysis event.<br>Only event of category 'VERSION' and 'OTHER' can be deleted.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the specified project</li></ul>
   */
  'postProjectAnalysesDeleteEvent'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectAnalysesDeleteEvent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectAnalysesDeleteEvent.Responses.$204>
  /**
   * getProjectAnalysesSearch - Search a project analyses and attached events.<br>Requires the following permission: 'Browse' on the specified project. <br>For applications, it also requires 'Browse' permission on its child projects.
   */
  'getProjectAnalysesSearch'(
    parameters?: Parameters<Paths.GetProjectAnalysesSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetProjectAnalysesSearch.Responses.$200>
  /**
   * postProjectAnalysesUpdateEvent - Update a project analysis event.<br>Only events of category 'VERSION' and 'OTHER' can be updated.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the specified project</li></ul>
   */
  'postProjectAnalysesUpdateEvent'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectAnalysesUpdateEvent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectAnalysesUpdateEvent.Responses.$200>
  /**
   * getProjectBadgesMeasure - Generate badge for project's measure as an SVG.<br/>Requires 'Browse' permission on the specified project.
   */
  'getProjectBadgesMeasure'(
    parameters?: Parameters<Paths.GetProjectBadgesMeasure.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getProjectBadgesQualityGate - Generate badge for project's quality gate as an SVG.<br/>Requires 'Browse' permission on the specified project.
   */
  'getProjectBadgesQualityGate'(
    parameters?: Parameters<Paths.GetProjectBadgesQualityGate.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * postProjectBadgesRenewToken - Creates new token replacing any existing token for project or application badge access for private projects and applications.<br/>This token can be used to authenticate with api/project_badges/quality_gate and api/project_badges/measure endpoints.<br/>Requires 'Administer' permission on the specified project or application.
   */
  'postProjectBadgesRenewToken'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectBadgesRenewToken.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectBadgesRenewToken.Responses.$204>
  /**
   * getProjectBadgesToken - Retrieve a token to use for project or application badge access for private projects or applications.<br/>This token can be used to authenticate with api/project_badges/quality_gate and api/project_badges/measure endpoints.<br/>Requires 'Browse' permission on the specified project or application.
   */
  'getProjectBadgesToken'(
    parameters?: Parameters<Paths.GetProjectBadgesToken.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetProjectBadgesToken.Responses.$200>
  /**
   * postProjectBranchesDelete - Delete a non-main branch of a project or application.<br/>Requires 'Administer' rights on the specified project or application.
   */
  'postProjectBranchesDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectBranchesDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectBranchesDelete.Responses.$204>
  /**
   * getProjectBranchesList - List the branches of a project or application.<br/>Requires 'Browse' or 'Execute analysis' rights on the specified project or application.
   */
  'getProjectBranchesList'(
    parameters?: Parameters<Paths.GetProjectBranchesList.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetProjectBranchesList.Responses.$200>
  /**
   * postProjectBranchesRename - Rename the main branch of a project or application.<br/>Requires 'Administer' permission on the specified project or application.
   */
  'postProjectBranchesRename'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectBranchesRename.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectBranchesRename.Responses.$204>
  /**
   * postProjectBranchesSetAutomaticDeletionProtection - Protect a specific branch from automatic deletion. Protection can't be disabled for the main branch.<br/>Requires 'Administer' permission on the specified project or application.
   */
  'postProjectBranchesSetAutomaticDeletionProtection'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectBranchesSetAutomaticDeletionProtection.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectBranchesSetAutomaticDeletionProtection.Responses.$204>
  /**
   * postProjectBranchesSetMain - Allow to set a new main branch.<br/>. Caution, only applicable on projects.<br>Requires 'Administer' rights on the specified project or application.
   */
  'postProjectBranchesSetMain'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectBranchesSetMain.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectBranchesSetMain.Responses.$204>
  /**
   * postProjectDumpExport - Triggers project dump so that the project can be imported to another SonarQube server (see api/project_dump/import, available in Enterprise Edition). Requires the 'Administer' permission.
   */
  'postProjectDumpExport'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectDumpExport.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectDumpExport.Responses.$200>
  /**
   * postProjectLinksCreate - Create a new project link.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
   */
  'postProjectLinksCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectLinksCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectLinksCreate.Responses.$200>
  /**
   * postProjectLinksDelete - Delete existing project link.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
   */
  'postProjectLinksDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectLinksDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectLinksDelete.Responses.$204>
  /**
   * getProjectLinksSearch - List links of a project.<br>The 'projectId' or 'projectKey' must be provided.<br>Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li><li>'Browse' on the specified project</li></ul>
   */
  'getProjectLinksSearch'(
    parameters?: Parameters<Paths.GetProjectLinksSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetProjectLinksSearch.Responses.$200>
  /**
   * getProjectTagsSearch - Search tags
   */
  'getProjectTagsSearch'(
    parameters?: Parameters<Paths.GetProjectTagsSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetProjectTagsSearch.Responses.$200>
  /**
   * postProjectTagsSet - Set tags on a project.<br>Requires the following permission: 'Administer' rights on the specified project
   */
  'postProjectTagsSet'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectTagsSet.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectTagsSet.Responses.$204>
  /**
   * postProjectsBulkDelete - Delete one or several projects.<br />Only the 1'000 first items in project filters are taken into account.<br />Requires 'Administer System' permission.<br />At least one parameter is required among analyzedBefore, projects and q
   */
  'postProjectsBulkDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectsBulkDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectsBulkDelete.Responses.$204>
  /**
   * postProjectsCreate - Create a project.<br/>If your project is hosted on a DevOps Platform, please use the import endpoint under api/alm_integrations, so it creates and properly configures the project.Requires 'Create Projects' permission.<br/>
   */
  'postProjectsCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectsCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectsCreate.Responses.$200>
  /**
   * postProjectsDelete - Delete a project.<br> Requires 'Administer System' permission or 'Administer' permission on the project.
   */
  'postProjectsDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectsDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectsDelete.Responses.$204>
  /**
   * getProjectsSearch - Search for projects or views to administrate them.
   * <ul>
   *   <li>The response field 'lastAnalysisDate' takes into account the analysis of all branches and pull requests, not only the main branch.</li>
   *   <li>The response field 'revision' takes into account the analysis of the main branch only.</li>
   * </ul>
   * Requires 'Administer System' permission
   */
  'getProjectsSearch'(
    parameters?: Parameters<Paths.GetProjectsSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetProjectsSearch.Responses.$200>
  /**
   * postProjectsUpdateKey - Update a project all its sub-components keys.<br>Requires one of the following permissions: <ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
   */
  'postProjectsUpdateKey'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectsUpdateKey.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectsUpdateKey.Responses.$204>
  /**
   * postProjectsUpdateVisibility - Updates visibility of a project, application or a portfolio.<br>Requires 'Project administer' permission on the specified entity
   */
  'postProjectsUpdateVisibility'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostProjectsUpdateVisibility.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostProjectsUpdateVisibility.Responses.$204>
  /**
   * postQualitygatesAddGroup - Allow a group of users to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
   */
  'postQualitygatesAddGroup'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesAddGroup.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesAddGroup.Responses.$204>
  /**
   * postQualitygatesAddUser - Allow a user to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
   */
  'postQualitygatesAddUser'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesAddUser.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesAddUser.Responses.$204>
  /**
   * postQualitygatesCopy - Copy a Quality Gate.<br>'sourceName' must be provided. Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesCopy'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesCopy.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesCopy.Responses.$204>
  /**
   * postQualitygatesCreate - Create a Quality Gate.<br>Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesCreate.Responses.$200>
  /**
   * postQualitygatesCreateCondition - Add a new condition to a quality gate.<br>Parameter 'gateName' must be provided. Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesCreateCondition'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesCreateCondition.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesCreateCondition.Responses.$200>
  /**
   * postQualitygatesDeleteCondition - Delete a condition from a quality gate.<br>Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesDeleteCondition'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesDeleteCondition.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesDeleteCondition.Responses.$204>
  /**
   * postQualitygatesDeselect - Remove the association of a project from a quality gate.<br>Requires one of the following permissions:<ul><li>'Administer Quality Gates'</li><li>'Administer' rights on the project</li></ul>
   */
  'postQualitygatesDeselect'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesDeselect.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesDeselect.Responses.$204>
  /**
   * postQualitygatesDestroy - Delete a Quality Gate.<br>Parameter 'name' must be specified. Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesDestroy'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesDestroy.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesDestroy.Responses.$204>
  /**
   * getQualitygatesGetByProject - Get the quality gate of a project.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li><li>'Browse' on the specified project</li></ul>
   */
  'getQualitygatesGetByProject'(
    parameters?: Parameters<Paths.GetQualitygatesGetByProject.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualitygatesGetByProject.Responses.$200>
  /**
   * getQualitygatesList - Get a list of quality gates
   */
  'getQualitygatesList'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualitygatesList.Responses.$200>
  /**
   * getQualitygatesProjectStatus - Get the quality gate status of a project or a Compute Engine task.<br />Either 'analysisId', 'projectId' or 'projectKey' must be provided <br />The different statuses returned are: OK, WARN, ERROR, NONE. The NONE status is returned when there is no quality gate associated with the analysis.<br />Returns an HTTP code 404 if the analysis associated with the task is not found or does not exist.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li><li>'Browse' on the specified project</li><li>'Execute Analysis' on the specified project</li></ul>
   */
  'getQualitygatesProjectStatus'(
    parameters?: Parameters<Paths.GetQualitygatesProjectStatus.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualitygatesProjectStatus.Responses.$200>
  /**
   * postQualitygatesRemoveGroup - Remove the ability from a group to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
   */
  'postQualitygatesRemoveGroup'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesRemoveGroup.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesRemoveGroup.Responses.$204>
  /**
   * postQualitygatesRemoveUser - Remove the ability from an user to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
   */
  'postQualitygatesRemoveUser'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesRemoveUser.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesRemoveUser.Responses.$204>
  /**
   * postQualitygatesRename - Rename a Quality Gate.<br>'currentName' must be specified. Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesRename'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesRename.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesRename.Responses.$204>
  /**
   * getQualitygatesSearch - Search for projects associated (or not) to a quality gate.<br/>Only authorized projects for the current user will be returned.
   */
  'getQualitygatesSearch'(
    parameters?: Parameters<Paths.GetQualitygatesSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualitygatesSearch.Responses.$200>
  /**
   * getQualitygatesSearchGroups - List the groups that are allowed to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
   */
  'getQualitygatesSearchGroups'(
    parameters?: Parameters<Paths.GetQualitygatesSearchGroups.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualitygatesSearchGroups.Responses.$200>
  /**
   * getQualitygatesSearchUsers - List the users that are allowed to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
   */
  'getQualitygatesSearchUsers'(
    parameters?: Parameters<Paths.GetQualitygatesSearchUsers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualitygatesSearchUsers.Responses.$200>
  /**
   * postQualitygatesSelect - Associate a project to a quality gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>'Administer' right on the specified project</li></ul>
   */
  'postQualitygatesSelect'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesSelect.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesSelect.Responses.$204>
  /**
   * postQualitygatesSetAsDefault - Set a quality gate as the default quality gate.<br>Parameter 'name' must be specified. Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesSetAsDefault'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesSetAsDefault.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesSetAsDefault.Responses.$204>
  /**
   * getQualitygatesShow - Display the details of a quality gate
   */
  'getQualitygatesShow'(
    parameters?: Parameters<Paths.GetQualitygatesShow.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualitygatesShow.Responses.$200>
  /**
   * postQualitygatesUpdateCondition - Update a condition attached to a quality gate.<br>Requires the 'Administer Quality Gates' permission.
   */
  'postQualitygatesUpdateCondition'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualitygatesUpdateCondition.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualitygatesUpdateCondition.Responses.$204>
  /**
   * postQualityprofilesActivateRule - Activate a rule on a Quality Profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
   */
  'postQualityprofilesActivateRule'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesActivateRule.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesActivateRule.Responses.$204>
  /**
   * postQualityprofilesActivateRules - Bulk-activate rules on one quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
   */
  'postQualityprofilesActivateRules'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesActivateRules.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesActivateRules.Responses.$204>
  /**
   * postQualityprofilesAddProject - Associate a project with a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Administer right on the specified project</li></ul>
   */
  'postQualityprofilesAddProject'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesAddProject.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesAddProject.Responses.$204>
  /**
   * getQualityprofilesBackup - Backup a quality profile in XML form. The exported profile can be restored through api/qualityprofiles/restore.
   */
  'getQualityprofilesBackup'(
    parameters?: Parameters<Paths.GetQualityprofilesBackup.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * postQualityprofilesChangeParent - Change a quality profile's parent.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
   */
  'postQualityprofilesChangeParent'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesChangeParent.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesChangeParent.Responses.$204>
  /**
   * getQualityprofilesChangelog - Get the history of changes on a quality profile: rule activation/deactivation, change in parameters/severity. Events are ordered by date in descending order (most recent first).
   */
  'getQualityprofilesChangelog'(
    parameters?: Parameters<Paths.GetQualityprofilesChangelog.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualityprofilesChangelog.Responses.$200>
  /**
   * postQualityprofilesCopy - Copy a quality profile.<br> Requires to be logged in and the 'Administer Quality Profiles' permission.
   */
  'postQualityprofilesCopy'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesCopy.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesCopy.Responses.$200>
  /**
   * postQualityprofilesCreate - Create a quality profile.<br>Requires to be logged in and the 'Administer Quality Profiles' permission.
   */
  'postQualityprofilesCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesCreate.Responses.$200>
  /**
   * postQualityprofilesDeactivateRule - Deactivate a rule on a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
   */
  'postQualityprofilesDeactivateRule'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesDeactivateRule.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesDeactivateRule.Responses.$204>
  /**
   * postQualityprofilesDeactivateRules - Bulk deactivate rules on Quality profiles.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
   */
  'postQualityprofilesDeactivateRules'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesDeactivateRules.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesDeactivateRules.Responses.$204>
  /**
   * postQualityprofilesDelete - Delete a quality profile and all its descendants. The default quality profile cannot be deleted.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
   */
  'postQualityprofilesDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesDelete.Responses.$204>
  /**
   * getQualityprofilesExport - Export a quality profile.
   */
  'getQualityprofilesExport'(
    parameters?: Parameters<Paths.GetQualityprofilesExport.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getQualityprofilesExporters - Lists available profile export formats.
   */
  'getQualityprofilesExporters'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualityprofilesExporters.Responses.$200>
  /**
   * getQualityprofilesImporters - List supported importers.
   */
  'getQualityprofilesImporters'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualityprofilesImporters.Responses.$200>
  /**
   * getQualityprofilesInheritance - Show a quality profile's ancestors and children.
   */
  'getQualityprofilesInheritance'(
    parameters?: Parameters<Paths.GetQualityprofilesInheritance.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualityprofilesInheritance.Responses.$200>
  /**
   * getQualityprofilesProjects - List projects with their association status regarding a quality profile. <br/>Only projects explicitly bound to the profile are returned, those associated with the profile because it is the default one are not. <br/>See api/qualityprofiles/search in order to get the Quality Profile Key. 
   */
  'getQualityprofilesProjects'(
    parameters?: Parameters<Paths.GetQualityprofilesProjects.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualityprofilesProjects.Responses.$200>
  /**
   * postQualityprofilesRemoveProject - Remove a project's association with a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li>  <li>Administer right on the specified project</li></ul>
   */
  'postQualityprofilesRemoveProject'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesRemoveProject.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesRemoveProject.Responses.$204>
  /**
   * postQualityprofilesRename - Rename a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
   */
  'postQualityprofilesRename'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesRename.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesRename.Responses.$204>
  /**
   * postQualityprofilesRestore - Restore a quality profile using an XML file. The restored profile name is taken from the backup file, so if a profile with the same name and language already exists, it will be overwritten.<br> Requires to be logged in and the 'Administer Quality Profiles' permission.
   */
  'postQualityprofilesRestore'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesRestore.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesRestore.Responses.$204>
  /**
   * getQualityprofilesSearch - Search quality profiles
   */
  'getQualityprofilesSearch'(
    parameters?: Parameters<Paths.GetQualityprofilesSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetQualityprofilesSearch.Responses.$200>
  /**
   * postQualityprofilesSetDefault - Select the default profile for a given language.<br> Requires to be logged in and the 'Administer Quality Profiles' permission.
   */
  'postQualityprofilesSetDefault'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostQualityprofilesSetDefault.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostQualityprofilesSetDefault.Responses.$204>
  /**
   * postRulesCreate - Create a custom rule.<br>Requires the 'Administer Quality Profiles' permission
   */
  'postRulesCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostRulesCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostRulesCreate.Responses.$200>
  /**
   * postRulesDelete - Delete custom rule.<br/>Requires the 'Administer Quality Profiles' permission
   */
  'postRulesDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostRulesDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostRulesDelete.Responses.$204>
  /**
   * getRulesRepositories - List available rule repositories
   */
  'getRulesRepositories'(
    parameters?: Parameters<Paths.GetRulesRepositories.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRulesRepositories.Responses.$200>
  /**
   * getRulesSearch - Search for a collection of relevant rules matching a specified query.<br/>
   */
  'getRulesSearch'(
    parameters?: Parameters<Paths.GetRulesSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRulesSearch.Responses.$200>
  /**
   * getRulesShow - Get detailed information about a rule<br>
   */
  'getRulesShow'(
    parameters?: Parameters<Paths.GetRulesShow.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRulesShow.Responses.$200>
  /**
   * getRulesTags - List rule tags
   */
  'getRulesTags'(
    parameters?: Parameters<Paths.GetRulesTags.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetRulesTags.Responses.$200>
  /**
   * postRulesUpdate - Update an existing rule.<br>Requires the 'Administer Quality Profiles' permission
   */
  'postRulesUpdate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostRulesUpdate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostRulesUpdate.Responses.$200>
  /**
   * getServerVersion - Version of SonarQube in plain text
   */
  'getServerVersion'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getSettingsListDefinitions - List settings definitions.<br>Requires 'Browse' permission when a component is specified<br/>To access licensed settings, authentication is required<br/>To access secured settings, one of the following permissions is required: <ul><li>'Execute Analysis'</li><li>'Administer System'</li><li>'Administer' rights on the specified component</li></ul>
   */
  'getSettingsListDefinitions'(
    parameters?: Parameters<Paths.GetSettingsListDefinitions.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSettingsListDefinitions.Responses.$200>
  /**
   * postSettingsReset - Remove a setting value.<br>The settings defined in conf/sonar.properties are read-only and can't be changed.<br/>Requires one of the following permissions: <ul><li>'Administer System'</li><li>'Administer' rights on the specified component</li></ul>
   */
  'postSettingsReset'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostSettingsReset.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostSettingsReset.Responses.$204>
  /**
   * postSettingsSet - Update a setting value.<br>Either 'value' or 'values' must be provided.<br> The settings defined in conf/sonar.properties are read-only and can't be changed.<br/>Requires one of the following permissions: <ul><li>'Administer System'</li><li>'Administer' rights on the specified component</li></ul>
   */
  'postSettingsSet'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostSettingsSet.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostSettingsSet.Responses.$204>
  /**
   * getSettingsValues - List settings values.<br>If no value has been set for a setting, then the default value is returned.<br>The settings from conf/sonar.properties are excluded from results.<br>Requires 'Browse' or 'Execute Analysis' permission when a component is specified.<br/>Secured settings values are not returned by the endpoint.<br/>
   */
  'getSettingsValues'(
    parameters?: Parameters<Paths.GetSettingsValues.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSettingsValues.Responses.$200>
  /**
   * getSourcesRaw - Get source code as raw text. Require 'See Source Code' permission on file
   */
  'getSourcesRaw'(
    parameters?: Parameters<Paths.GetSourcesRaw.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * getSourcesScm - Get SCM information of source files. Require See Source Code permission on file's project<br/>Each element of the result array is composed of:<ol><li>Line number</li><li>Author of the commit</li><li>Datetime of the commit (before 5.2 it was only the Date)</li><li>Revision of the commit (added in 5.2)</li></ol>
   */
  'getSourcesScm'(
    parameters?: Parameters<Paths.GetSourcesScm.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSourcesScm.Responses.$200>
  /**
   * getSourcesShow - Get source code. Requires See Source Code permission on file's project<br/>Each element of the result array is composed of:<ol><li>Line number</li><li>Content of the line</li></ol>
   */
  'getSourcesShow'(
    parameters?: Parameters<Paths.GetSourcesShow.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSourcesShow.Responses.$200>
  /**
   * postSystemChangeLogLevel - Temporarily changes level of logs. New level is not persistent and is lost when restarting server. Requires system administration permission.
   */
  'postSystemChangeLogLevel'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostSystemChangeLogLevel.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostSystemChangeLogLevel.Responses.$204>
  /**
   * getSystemDbMigrationStatus - Display the database migration status of SonarQube.<br/>State values are:<ul><li>NO_MIGRATION: DB is up to date with current version of SonarQube.</li><li>NOT_SUPPORTED: Migration is not supported on embedded databases.</li><li>MIGRATION_RUNNING: DB migration is under go.</li><li>MIGRATION_SUCCEEDED: DB migration has run and has been successful.</li><li>MIGRATION_FAILED: DB migration has run and failed. SonarQube must be restarted in order to retry a DB migration (optionally after DB has been restored from backup).</li><li>MIGRATION_REQUIRED: DB migration is required.</li></ul>
   */
  'getSystemDbMigrationStatus'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSystemDbMigrationStatus.Responses.$200>
  /**
   * getSystemHealth - Provide health status of SonarQube.<p>Although global health is calculated based on both application and search nodes, detailed information is returned only for application nodes.</p><p>  <ul> <li>GREEN: SonarQube is fully operational</li> <li>YELLOW: SonarQube is usable, but it needs attention in order to be fully operational</li> <li>RED: SonarQube is not operational</li> </ul></p><br>Requires the 'Administer System' permission or system passcode (see WEB_SYSTEM_PASS_CODE in sonar.properties).<br>When SonarQube is in safe mode (waiting or running a database upgrade), only the authentication with a system passcode is supported.
   */
  'getSystemHealth'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSystemHealth.Responses.$200>
  /**
   * getSystemInfo - Get detailed information about system configuration.<br/>Requires 'Administer' permissions.
   */
  'getSystemInfo'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSystemInfo.Responses.$200>
  /**
   * getSystemLogs - Get system logs in plain-text format. Requires system administration permission.
   */
  'getSystemLogs'(
    parameters?: Parameters<Paths.GetSystemLogs.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * postSystemMigrateDb - Migrate the database to match the current version of SonarQube.<br/>Sending a POST request to this URL starts the DB migration. It is strongly advised to <strong>make a database backup</strong> before invoking this WS.<br/>State values are:<ul><li>NO_MIGRATION: DB is up to date with current version of SonarQube.</li><li>NOT_SUPPORTED: Migration is not supported on embedded databases.</li><li>MIGRATION_RUNNING: DB migration is under go.</li><li>MIGRATION_SUCCEEDED: DB migration has run and has been successful.</li><li>MIGRATION_FAILED: DB migration has run and failed. SonarQube must be restarted in order to retry a DB migration (optionally after DB has been restored from backup).</li><li>MIGRATION_REQUIRED: DB migration is required.</li></ul>
   */
  'postSystemMigrateDb'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostSystemMigrateDb.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostSystemMigrateDb.Responses.$200>
  /**
   * getSystemPing - Answers "pong" as plain-text
   */
  'getSystemPing'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<any>
  /**
   * postSystemRestart - Restarts server. Requires 'Administer System' permission. Performs a full restart of the Web, Search and Compute Engine Servers processes. Does not reload sonar.properties.
   */
  'postSystemRestart'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostSystemRestart.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostSystemRestart.Responses.$204>
  /**
   * getSystemStatus - Get state information about SonarQube.<p>status: the running status <ul> <li>STARTING: SonarQube Web Server is up and serving some Web Services (eg. api/system/status) but initialization is still ongoing</li> <li>UP: SonarQube instance is up and running</li> <li>DOWN: SonarQube instance is up but not running because migration has failed (refer to WS /api/system/migrate_db for details) or some other reason (check logs).</li> <li>RESTARTING: SonarQube instance is still up but a restart has been requested (refer to WS /api/system/restart for details).</li> <li>DB_MIGRATION_NEEDED: database migration is required. DB migration can be started using WS /api/system/migrate_db.</li> <li>DB_MIGRATION_RUNNING: DB migration is running (refer to WS /api/system/migrate_db for details)</li> </ul></p>
   */
  'getSystemStatus'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSystemStatus.Responses.$200>
  /**
   * getSystemUpgrades - Lists available upgrades for the SonarQube instance (if any) and for each one, lists incompatible plugins and plugins requiring upgrade.<br/>Plugin information is retrieved from Update Center. Date and time at which Update Center was last refreshed is provided in the response.
   */
  'getSystemUpgrades'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetSystemUpgrades.Responses.$200>
  /**
   * postUserGroupsAddUser - Add a user to a group.<br />'name' must be provided.<br />Requires the following permission: 'Administer System'.
   */
  'postUserGroupsAddUser'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUserGroupsAddUser.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUserGroupsAddUser.Responses.$204>
  /**
   * postUserGroupsCreate - Create a group.<br>Requires the following permission: 'Administer System'.
   */
  'postUserGroupsCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUserGroupsCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUserGroupsCreate.Responses.$200>
  /**
   * postUserGroupsDelete - Delete a group. The default groups cannot be deleted.<br/>'name' must be provided.<br />Requires the following permission: 'Administer System'.
   */
  'postUserGroupsDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUserGroupsDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUserGroupsDelete.Responses.$204>
  /**
   * postUserGroupsRemoveUser - Remove a user from a group.<br />'name' must be provided.<br>Requires the following permission: 'Administer System'.
   */
  'postUserGroupsRemoveUser'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUserGroupsRemoveUser.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUserGroupsRemoveUser.Responses.$204>
  /**
   * getUserGroupsSearch - Search for user groups.<br>Requires the following permission: 'Administer System'.
   */
  'getUserGroupsSearch'(
    parameters?: Parameters<Paths.GetUserGroupsSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUserGroupsSearch.Responses.$200>
  /**
   * postUserGroupsUpdate - Update a group.<br>Requires the following permission: 'Administer System'.
   */
  'postUserGroupsUpdate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUserGroupsUpdate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUserGroupsUpdate.Responses.$204>
  /**
   * getUserGroupsUsers - Search for users with membership information with respect to a group.<br>Requires the following permission: 'Administer System'.
   */
  'getUserGroupsUsers'(
    parameters?: Parameters<Paths.GetUserGroupsUsers.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUserGroupsUsers.Responses.$200>
  /**
   * postUserTokensGenerate - Generate a user access token. <br />Please keep your tokens secret. They enable to authenticate and analyze projects.<br />It requires administration permissions to specify a 'login' and generate a token for another user. Otherwise, a token is generated for the current user.
   */
  'postUserTokensGenerate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUserTokensGenerate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUserTokensGenerate.Responses.$200>
  /**
   * postUserTokensRevoke - Revoke a user access token. <br/>It requires administration permissions to specify a 'login' and revoke a token for another user. Otherwise, the token for the current user is revoked.
   */
  'postUserTokensRevoke'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUserTokensRevoke.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUserTokensRevoke.Responses.$204>
  /**
   * getUserTokensSearch - List the access tokens of a user.<br>The login must exist and active.<br>Field 'lastConnectionDate' is only updated every hour, so it may not be accurate, for instance when a user is using a token many times in less than one hour.<br> It requires administration permissions to specify a 'login' and list the tokens of another user. Otherwise, tokens for the current user are listed. <br> Authentication is required for this API endpoint
   */
  'getUserTokensSearch'(
    parameters?: Parameters<Paths.GetUserTokensSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUserTokensSearch.Responses.$200>
  /**
   * postUsersAnonymize - Anonymize a deactivated user. Requires Administer System permission
   */
  'postUsersAnonymize'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUsersAnonymize.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUsersAnonymize.Responses.$204>
  /**
   * postUsersChangePassword - Update a user's password. Authenticated users can change their own password, provided that the account is not linked to an external authentication system. Administer System permission is required to change another user's password.
   */
  'postUsersChangePassword'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUsersChangePassword.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUsersChangePassword.Responses.$204>
  /**
   * postUsersCreate - Create a user.<br/>If a deactivated user account exists with the given login, it will be reactivated.<br/>Requires Administer System permission
   */
  'postUsersCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUsersCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUsersCreate.Responses.$200>
  /**
   * postUsersDeactivate - Deactivate a user. Requires Administer System permission
   */
  'postUsersDeactivate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUsersDeactivate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUsersDeactivate.Responses.$200>
  /**
   * getUsersGroups - Lists the groups a user belongs to. <br/>Requires Administer System permission.
   */
  'getUsersGroups'(
    parameters?: Parameters<Paths.GetUsersGroups.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUsersGroups.Responses.$200>
  /**
   * getUsersSearch - Get a list of users. By default, only active users are returned.<br/>The following fields are only returned when user has Administer System permission or for logged-in in user :<ul>   <li>'email'</li>   <li>'externalIdentity'</li>   <li>'externalProvider'</li>   <li>'groups'</li>   <li>'lastConnectionDate'</li>   <li>'sonarLintLastConnectionDate'</li>   <li>'tokensCount'</li></ul>Field 'lastConnectionDate' is only updated every hour, so it may not be accurate, for instance when a user authenticates many times in less than one hour.
   */
  'getUsersSearch'(
    parameters?: Parameters<Paths.GetUsersSearch.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetUsersSearch.Responses.$200>
  /**
   * postUsersUpdate - Update a user.<br/>Requires Administer System permission
   */
  'postUsersUpdate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUsersUpdate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUsersUpdate.Responses.$200>
  /**
   * postUsersUpdateIdentityProvider - Update identity provider information. <br/>It's only possible to migrate to an installed identity provider. Be careful that as soon as this information has been updated for a user, the user will only be able to authenticate on the new identity provider. It is not possible to migrate external user to local one.<br/>Requires Administer System permission.
   */
  'postUsersUpdateIdentityProvider'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUsersUpdateIdentityProvider.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUsersUpdateIdentityProvider.Responses.$204>
  /**
   * postUsersUpdateLogin - Update a user login. A login can be updated many times.<br/>Requires Administer System permission
   */
  'postUsersUpdateLogin'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostUsersUpdateLogin.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostUsersUpdateLogin.Responses.$204>
  /**
   * postWebhooksCreate - Create a Webhook.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
   */
  'postWebhooksCreate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostWebhooksCreate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostWebhooksCreate.Responses.$200>
  /**
   * postWebhooksDelete - Delete a Webhook.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
   */
  'postWebhooksDelete'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostWebhooksDelete.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostWebhooksDelete.Responses.$204>
  /**
   * getWebhooksDeliveries - Get the recent deliveries for a specified project or Compute Engine task.<br/>Require 'Administer' permission on the related project.<br/>Note that additional information are returned by api/webhooks/delivery.
   */
  'getWebhooksDeliveries'(
    parameters?: Parameters<Paths.GetWebhooksDeliveries.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWebhooksDeliveries.Responses.$200>
  /**
   * getWebhooksDelivery - Get a webhook delivery by its id.<br/>Require 'Administer System' permission.<br/>Note that additional information are returned by api/webhooks/delivery.
   */
  'getWebhooksDelivery'(
    parameters?: Parameters<Paths.GetWebhooksDelivery.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWebhooksDelivery.Responses.$200>
  /**
   * getWebhooksList - Search for global webhooks or project webhooks. Webhooks are ordered by name.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
   */
  'getWebhooksList'(
    parameters?: Parameters<Paths.GetWebhooksList.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWebhooksList.Responses.$200>
  /**
   * postWebhooksUpdate - Update a Webhook.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
   */
  'postWebhooksUpdate'(
    parameters?: Parameters<UnknownParamsObject> | null,
    data?: Paths.PostWebhooksUpdate.RequestBody,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.PostWebhooksUpdate.Responses.$204>
  /**
   * getWebservicesList - List web services
   */
  'getWebservicesList'(
    parameters?: Parameters<Paths.GetWebservicesList.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWebservicesList.Responses.$200>
  /**
   * getWebservicesResponseExample - Display web service response example
   */
  'getWebservicesResponseExample'(
    parameters?: Parameters<Paths.GetWebservicesResponseExample.QueryParameters> | null,
    data?: any,
    config?: AxiosRequestConfig  
  ): OperationResponse<Paths.GetWebservicesResponseExample.Responses.$200>
}

export interface PathsDictionary {
  ['/api/alm_integrations/import_azure_project']: {
    /**
     * postAlmIntegrationsImportAzureProject - Create a SonarQube project with the information from the provided Azure DevOps project.<br/>Autoconfigure pull request decoration mechanism.<br/>Requires the 'Create Projects' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmIntegrationsImportAzureProject.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmIntegrationsImportAzureProject.Responses.$204>
  }
  ['/api/alm_integrations/import_bitbucketcloud_repo']: {
    /**
     * postAlmIntegrationsImportBitbucketcloudRepo - Create a SonarQube project with the information from the provided Bitbucket Cloud repository.<br/>Autoconfigure pull request decoration mechanism.<br/>Requires the 'Create Projects' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmIntegrationsImportBitbucketcloudRepo.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmIntegrationsImportBitbucketcloudRepo.Responses.$204>
  }
  ['/api/alm_integrations/import_bitbucketserver_project']: {
    /**
     * postAlmIntegrationsImportBitbucketserverProject - Create a SonarQube project with the information from the provided BitbucketServer project.<br/>Autoconfigure pull request decoration mechanism.<br/>Requires the 'Create Projects' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmIntegrationsImportBitbucketserverProject.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmIntegrationsImportBitbucketserverProject.Responses.$204>
  }
  ['/api/alm_integrations/import_github_project']: {
    /**
     * postAlmIntegrationsImportGithubProject - Create a SonarQube project with the information from the provided GitHub repository.<br/>Autoconfigure pull request decoration mechanism. If Automatic Provisioning is enable for GitHub, it will also synchronize permissions from the repository.<br/>Requires the 'Create Projects' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmIntegrationsImportGithubProject.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmIntegrationsImportGithubProject.Responses.$204>
  }
  ['/api/alm_integrations/import_gitlab_project']: {
    /**
     * postAlmIntegrationsImportGitlabProject - Import a GitLab project to SonarQube, creating a new project and configuring MR decoration<br/>Requires the 'Create Projects' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmIntegrationsImportGitlabProject.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmIntegrationsImportGitlabProject.Responses.$204>
  }
  ['/api/alm_integrations/list_azure_projects']: {
    /**
     * getAlmIntegrationsListAzureProjects - List Azure projects<br/>Requires the 'Create Projects' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmIntegrationsListAzureProjects.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmIntegrationsListAzureProjects.Responses.$200>
  }
  ['/api/alm_integrations/list_bitbucketserver_projects']: {
    /**
     * getAlmIntegrationsListBitbucketserverProjects - List the Bitbucket Server projects<br/>Requires the 'Create Projects' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmIntegrationsListBitbucketserverProjects.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmIntegrationsListBitbucketserverProjects.Responses.$200>
  }
  ['/api/alm_integrations/search_azure_repos']: {
    /**
     * getAlmIntegrationsSearchAzureRepos - Search the Azure repositories<br/>Requires the 'Create Projects' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmIntegrationsSearchAzureRepos.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmIntegrationsSearchAzureRepos.Responses.$200>
  }
  ['/api/alm_integrations/search_bitbucketcloud_repos']: {
    /**
     * getAlmIntegrationsSearchBitbucketcloudRepos - Search the Bitbucket Cloud repositories<br/>Requires the 'Create Projects' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmIntegrationsSearchBitbucketcloudRepos.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmIntegrationsSearchBitbucketcloudRepos.Responses.$200>
  }
  ['/api/alm_integrations/search_bitbucketserver_repos']: {
    /**
     * getAlmIntegrationsSearchBitbucketserverRepos - Search the Bitbucket Server repositories with REPO_ADMIN access<br/>Requires the 'Create Projects' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmIntegrationsSearchBitbucketserverRepos.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmIntegrationsSearchBitbucketserverRepos.Responses.$200>
  }
  ['/api/alm_integrations/search_gitlab_repos']: {
    /**
     * getAlmIntegrationsSearchGitlabRepos - Search the GitLab projects.<br/>Requires the 'Create Projects' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmIntegrationsSearchGitlabRepos.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmIntegrationsSearchGitlabRepos.Responses.$200>
  }
  ['/api/alm_integrations/set_pat']: {
    /**
     * postAlmIntegrationsSetPat - Set a Personal Access Token for the given DevOps Platform setting<br/>Requires the 'Create Projects' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmIntegrationsSetPat.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmIntegrationsSetPat.Responses.$204>
  }
  ['/api/alm_settings/count_binding']: {
    /**
     * getAlmSettingsCountBinding - Count number of project bound to an DevOps Platform setting.<br/>Requires the 'Administer System' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmSettingsCountBinding.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmSettingsCountBinding.Responses.$200>
  }
  ['/api/alm_settings/create_azure']: {
    /**
     * postAlmSettingsCreateAzure - Create Azure instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsCreateAzure.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsCreateAzure.Responses.$204>
  }
  ['/api/alm_settings/create_bitbucket']: {
    /**
     * postAlmSettingsCreateBitbucket - Create Bitbucket instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsCreateBitbucket.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsCreateBitbucket.Responses.$204>
  }
  ['/api/alm_settings/create_bitbucketcloud']: {
    /**
     * postAlmSettingsCreateBitbucketcloud - Configure a new instance of Bitbucket Cloud. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsCreateBitbucketcloud.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsCreateBitbucketcloud.Responses.$204>
  }
  ['/api/alm_settings/create_github']: {
    /**
     * postAlmSettingsCreateGithub - Create GitHub instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsCreateGithub.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsCreateGithub.Responses.$204>
  }
  ['/api/alm_settings/create_gitlab']: {
    /**
     * postAlmSettingsCreateGitlab - Create GitLab instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsCreateGitlab.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsCreateGitlab.Responses.$204>
  }
  ['/api/alm_settings/delete']: {
    /**
     * postAlmSettingsDelete - Delete an DevOps Platform Setting.<br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsDelete.Responses.$204>
  }
  ['/api/alm_settings/get_binding']: {
    /**
     * getAlmSettingsGetBinding - Get DevOps Platform binding of a given project.<br/>Requires the 'Browse' permission on the project
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmSettingsGetBinding.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmSettingsGetBinding.Responses.$200>
  }
  ['/api/alm_settings/list']: {
    /**
     * getAlmSettingsList - List DevOps Platform setting available for a given project, sorted by DevOps Platform key<br/>Requires the 'Administer project' permission if the 'project' parameter is provided, requires the 'Create Projects' permission otherwise.
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmSettingsList.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmSettingsList.Responses.$200>
  }
  ['/api/alm_settings/list_definitions']: {
    /**
     * getAlmSettingsListDefinitions - List DevOps Platform Settings, sorted by created date.<br/>Requires the 'Administer System' permission
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmSettingsListDefinitions.Responses.$200>
  }
  ['/api/alm_settings/update_azure']: {
    /**
     * postAlmSettingsUpdateAzure - Update Azure instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsUpdateAzure.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsUpdateAzure.Responses.$204>
  }
  ['/api/alm_settings/update_bitbucket']: {
    /**
     * postAlmSettingsUpdateBitbucket - Update Bitbucket instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsUpdateBitbucket.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsUpdateBitbucket.Responses.$204>
  }
  ['/api/alm_settings/update_bitbucketcloud']: {
    /**
     * postAlmSettingsUpdateBitbucketcloud - Update Bitbucket Cloud Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsUpdateBitbucketcloud.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsUpdateBitbucketcloud.Responses.$204>
  }
  ['/api/alm_settings/update_github']: {
    /**
     * postAlmSettingsUpdateGithub - Update GitHub instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsUpdateGithub.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsUpdateGithub.Responses.$204>
  }
  ['/api/alm_settings/update_gitlab']: {
    /**
     * postAlmSettingsUpdateGitlab - Update GitLab instance Setting. <br/>Requires the 'Administer System' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAlmSettingsUpdateGitlab.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAlmSettingsUpdateGitlab.Responses.$204>
  }
  ['/api/alm_settings/validate']: {
    /**
     * getAlmSettingsValidate - Validate an DevOps Platform Setting by checking connectivity and permissions<br/>Requires the 'Administer System' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetAlmSettingsValidate.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAlmSettingsValidate.Responses.$200>
  }
  ['/api/analysis_cache/get']: {
    /**
     * getAnalysisCacheGet - Get the scanner's cached data for a branch. Requires scan permission on the project. Data is returned gzipped if the corresponding 'Accept-Encoding' header is set in the request.
     */
    'get'(
      parameters?: Parameters<Paths.GetAnalysisCacheGet.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAnalysisCacheGet.Responses.$204>
  }
  ['/api/authentication/login']: {
    /**
     * postAuthenticationLogin - Authenticate a user.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAuthenticationLogin.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAuthenticationLogin.Responses.$204>
  }
  ['/api/authentication/logout']: {
    /**
     * postAuthenticationLogout - Logout a user.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostAuthenticationLogout.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostAuthenticationLogout.Responses.$204>
  }
  ['/api/authentication/validate']: {
    /**
     * getAuthenticationValidate - Check credentials.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetAuthenticationValidate.Responses.$200>
  }
  ['/api/ce/activity']: {
    /**
     * getCeActivity - Search for tasks.<br> Requires the system administration permission, or project administration permission if component is set.
     */
    'get'(
      parameters?: Parameters<Paths.GetCeActivity.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCeActivity.Responses.$200>
  }
  ['/api/ce/activity_status']: {
    /**
     * getCeActivityStatus - Returns CE activity related metrics.<br>Requires 'Administer System' permission or 'Administer' rights on the specified project.
     */
    'get'(
      parameters?: Parameters<Paths.GetCeActivityStatus.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCeActivityStatus.Responses.$200>
  }
  ['/api/ce/component']: {
    /**
     * getCeComponent - Get the pending tasks, in-progress tasks and the last executed task of a given component (usually a project).<br>Requires the following permission: 'Browse' on the specified component.
     */
    'get'(
      parameters?: Parameters<Paths.GetCeComponent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCeComponent.Responses.$200>
  }
  ['/api/ce/task']: {
    /**
     * getCeTask - Give Compute Engine task details such as type, status, duration and associated component.<br/>Requires one of the following permissions: <ul><li>'Administer' at global or project level</li><li>'Execute Analysis' at global or project level</li></ul>Since 6.1, field "logs" is deprecated and its value is always false.
     */
    'get'(
      parameters?: Parameters<Paths.GetCeTask.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetCeTask.Responses.$200>
  }
  ['/api/components/search']: {
    /**
     * getComponentsSearch - Search for components
     */
    'get'(
      parameters?: Parameters<Paths.GetComponentsSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetComponentsSearch.Responses.$200>
  }
  ['/api/components/show']: {
    /**
     * getComponentsShow - Returns a component (file, directory, project, portfolio…) and its ancestors. The ancestors are ordered from the parent to the root project. Requires the following permission: 'Browse' on the project of the specified component.
     */
    'get'(
      parameters?: Parameters<Paths.GetComponentsShow.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetComponentsShow.Responses.$200>
  }
  ['/api/components/tree']: {
    /**
     * getComponentsTree - Navigate through components based on the chosen strategy.<br>Requires the following permission: 'Browse' on the specified project.<br>When limiting search with the q parameter, directories are not returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetComponentsTree.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetComponentsTree.Responses.$200>
  }
  ['/api/duplications/show']: {
    /**
     * getDuplicationsShow - Get duplications. Require Browse permission on file's project
     */
    'get'(
      parameters?: Parameters<Paths.GetDuplicationsShow.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetDuplicationsShow.Responses.$200>
  }
  ['/api/favorites/add']: {
    /**
     * postFavoritesAdd - Add a component (project, portfolio, etc.) as favorite for the authenticated user.<br>Only 100 components by qualifier can be added as favorite.<br>Requires authentication and the following permission: 'Browse' on the component.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostFavoritesAdd.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostFavoritesAdd.Responses.$204>
  }
  ['/api/favorites/remove']: {
    /**
     * postFavoritesRemove - Remove a component (project, portfolio, application etc.) as favorite for the authenticated user.<br>Requires authentication.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostFavoritesRemove.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostFavoritesRemove.Responses.$204>
  }
  ['/api/favorites/search']: {
    /**
     * getFavoritesSearch - Search for the authenticated user favorites.<br>Requires authentication.
     */
    'get'(
      parameters?: Parameters<Paths.GetFavoritesSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetFavoritesSearch.Responses.$200>
  }
  ['/api/hotspots/change_status']: {
    /**
     * postHotspotsChangeStatus - Change the status of a Security Hotpot.<br/>Requires the 'Administer Security Hotspot' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostHotspotsChangeStatus.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostHotspotsChangeStatus.Responses.$204>
  }
  ['/api/hotspots/search']: {
    /**
     * getHotspotsSearch - Search for Security Hotpots. <br>Requires the 'Browse' permission on the specified project(s). <br>For applications, it also requires 'Browse' permission on its child projects. <br>When issue indexing is in progress returns 503 service unavailable HTTP code.
     */
    'get'(
      parameters?: Parameters<Paths.GetHotspotsSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetHotspotsSearch.Responses.$200>
  }
  ['/api/hotspots/show']: {
    /**
     * getHotspotsShow - Provides the details of a Security Hotspot.
     */
    'get'(
      parameters?: Parameters<Paths.GetHotspotsShow.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetHotspotsShow.Responses.$200>
  }
  ['/api/issues/add_comment']: {
    /**
     * postIssuesAddComment - Add a comment.<br/>Requires authentication and the following permission: 'Browse' on the project of the specified issue.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesAddComment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesAddComment.Responses.$200>
  }
  ['/api/issues/assign']: {
    /**
     * postIssuesAssign - Assign/Unassign an issue. Requires authentication and Browse permission on project
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesAssign.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesAssign.Responses.$200>
  }
  ['/api/issues/authors']: {
    /**
     * getIssuesAuthors - Search SCM accounts which match a given query.<br/>Requires authentication.<br/>When issue indexing is in progress returns 503 service unavailable HTTP code.
     */
    'get'(
      parameters?: Parameters<Paths.GetIssuesAuthors.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetIssuesAuthors.Responses.$200>
  }
  ['/api/issues/bulk_change']: {
    /**
     * postIssuesBulkChange - Bulk change on issues. Up to 500 issues can be updated. <br/>Requires authentication.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesBulkChange.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesBulkChange.Responses.$200>
  }
  ['/api/issues/changelog']: {
    /**
     * getIssuesChangelog - Display changelog of an issue.<br/>Requires the 'Browse' permission on the project of the specified issue.
     */
    'get'(
      parameters?: Parameters<Paths.GetIssuesChangelog.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetIssuesChangelog.Responses.$200>
  }
  ['/api/issues/delete_comment']: {
    /**
     * postIssuesDeleteComment - Delete a comment.<br/>Requires authentication and the following permission: 'Browse' on the project of the specified issue.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesDeleteComment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesDeleteComment.Responses.$200>
  }
  ['/api/issues/do_transition']: {
    /**
     * postIssuesDoTransition - Do workflow transition on an issue. Requires authentication and Browse permission on project.<br/>
     * The transitions 'accept', 'wontfix' and 'falsepositive' require the permission 'Administer Issues'.<br/>
     * The transitions involving security hotspots require the permission 'Administer Security Hotspot'.
     * 
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesDoTransition.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesDoTransition.Responses.$200>
  }
  ['/api/issues/edit_comment']: {
    /**
     * postIssuesEditComment - Edit a comment.<br/>Requires authentication and the following permission: 'Browse' on the project of the specified issue.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesEditComment.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesEditComment.Responses.$200>
  }
  ['/api/issues/reindex']: {
    /**
     * postIssuesReindex - Reindex issues for a project.<br> Require 'Administer System' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesReindex.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesReindex.Responses.$204>
  }
  ['/api/issues/search']: {
    /**
     * getIssuesSearch - Search for issues.<br>Requires the 'Browse' permission on the specified project(s). <br>For applications, it also requires 'Browse' permission on its child projects.<br/>When issue indexing is in progress returns 503 service unavailable HTTP code.
     */
    'get'(
      parameters?: Parameters<Paths.GetIssuesSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetIssuesSearch.Responses.$200>
  }
  ['/api/issues/set_severity']: {
    /**
     * postIssuesSetSeverity - Change severity.<br/>Requires the following permissions:<ul>  <li>'Authentication'</li>  <li>'Browse' rights on project of the specified issue</li>  <li>'Administer Issues' rights on project of the specified issue</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesSetSeverity.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesSetSeverity.Responses.$200>
  }
  ['/api/issues/set_tags']: {
    /**
     * postIssuesSetTags - Set tags on an issue. <br/>Requires authentication and Browse permission on project
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesSetTags.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesSetTags.Responses.$200>
  }
  ['/api/issues/set_type']: {
    /**
     * postIssuesSetType - Change type of issue, for instance from 'code smell' to 'bug'.<br/>Requires the following permissions:<ul>  <li>'Authentication'</li>  <li>'Browse' rights on project of the specified issue</li>  <li>'Administer Issues' rights on project of the specified issue</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostIssuesSetType.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostIssuesSetType.Responses.$200>
  }
  ['/api/issues/tags']: {
    /**
     * getIssuesTags - List tags matching a given query
     */
    'get'(
      parameters?: Parameters<Paths.GetIssuesTags.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetIssuesTags.Responses.$200>
  }
  ['/api/languages/list']: {
    /**
     * getLanguagesList - List supported programming languages
     */
    'get'(
      parameters?: Parameters<Paths.GetLanguagesList.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetLanguagesList.Responses.$200>
  }
  ['/api/measures/component']: {
    /**
     * getMeasuresComponent - Return component with specified measures.<br>Requires the following permission: 'Browse' on the project of specified component.
     */
    'get'(
      parameters?: Parameters<Paths.GetMeasuresComponent.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetMeasuresComponent.Responses.$200>
  }
  ['/api/measures/component_tree']: {
    /**
     * getMeasuresComponentTree - Navigate through components based on the chosen strategy with specified measures.<br>Requires the following permission: 'Browse' on the specified project.<br>For applications, it also requires 'Browse' permission on its child projects. <br>When limiting search with the q parameter, directories are not returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetMeasuresComponentTree.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetMeasuresComponentTree.Responses.$200>
  }
  ['/api/measures/search_history']: {
    /**
     * getMeasuresSearchHistory - Search measures history of a component.<br>Measures are ordered chronologically.<br>Pagination applies to the number of measures for each metric.<br>Requires the following permission: 'Browse' on the specified component. <br>For applications, it also requires 'Browse' permission on its child projects.
     */
    'get'(
      parameters?: Parameters<Paths.GetMeasuresSearchHistory.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetMeasuresSearchHistory.Responses.$200>
  }
  ['/api/metrics/search']: {
    /**
     * getMetricsSearch - Search for metrics
     */
    'get'(
      parameters?: Parameters<Paths.GetMetricsSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetMetricsSearch.Responses.$200>
  }
  ['/api/metrics/types']: {
    /**
     * getMetricsTypes - List all available metric types.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetMetricsTypes.Responses.$200>
  }
  ['/api/monitoring/metrics']: {
    /**
     * getMonitoringMetrics - Return monitoring metrics in Prometheus format. 
     * Support content type 'text/plain' (default) and 'application/openmetrics-text'.
     * this endpoint can be access using a Bearer token, that needs to be defined in sonar.properties with the 'sonar.web.systemPasscode' key.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/new_code_periods/list']: {
    /**
     * getNewCodePeriodsList - Lists the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a> for all branches in a project.<br>Requires the permission to browse the project
     */
    'get'(
      parameters?: Parameters<Paths.GetNewCodePeriodsList.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetNewCodePeriodsList.Responses.$200>
  }
  ['/api/new_code_periods/set']: {
    /**
     * postNewCodePeriodsSet - Updates the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a> on different levels:<br><ul><li>Not providing a project key and a branch key will update the default value at global level. Existing projects or branches having a specific new code definition will not be impacted</li><li>Project key must be provided to update the value for a project</li><li>Both project and branch keys must be provided to update the value for a branch</li></ul>Requires one of the following permissions: <ul><li>'Administer System' to change the global setting</li><li>'Administer' rights on the specified project to change the project setting</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostNewCodePeriodsSet.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostNewCodePeriodsSet.Responses.$204>
  }
  ['/api/new_code_periods/show']: {
    /**
     * getNewCodePeriodsShow - Shows the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a>.<br> If the component requested doesn't exist or if no new code definition is set for it, a value is inherited from the project or from the global setting.Requires one of the following permissions if a component is specified: <ul><li>'Administer' rights on the specified component</li><li>'Execute analysis' rights on the specified component</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetNewCodePeriodsShow.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetNewCodePeriodsShow.Responses.$200>
  }
  ['/api/new_code_periods/unset']: {
    /**
     * postNewCodePeriodsUnset - Unsets the <a href="https://docs.sonarsource.com/sonarqube/10.4/project-administration/defining-new-code/" target="_blank" rel="noopener noreferrer">new code definition</a> for a branch, project or global. It requires the inherited New Code Definition to be compatible with the Clean as You Code methodology, and one of the following permissions: <ul><li>'Administer System' to change the global setting</li><li>'Administer' rights for a specified component</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostNewCodePeriodsUnset.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostNewCodePeriodsUnset.Responses.$204>
  }
  ['/api/notifications/add']: {
    /**
     * postNotificationsAdd - Add a notification for the authenticated user.<br>Requires one of the following permissions:<ul> <li>Authentication if no login is provided. If a project is provided, requires the 'Browse' permission on the specified project.</li> <li>System administration if a login is provided. If a project is provided, requires the 'Browse' permission on the specified project.</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostNotificationsAdd.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostNotificationsAdd.Responses.$204>
  }
  ['/api/notifications/list']: {
    /**
     * getNotificationsList - List notifications of the authenticated user.<br>Requires one of the following permissions:<ul>  <li>Authentication if no login is provided</li>  <li>System administration if a login is provided</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetNotificationsList.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetNotificationsList.Responses.$200>
  }
  ['/api/notifications/remove']: {
    /**
     * postNotificationsRemove - Remove a notification for the authenticated user.<br>Requires one of the following permissions:<ul>  <li>Authentication if no login is provided</li>  <li>System administration if a login is provided</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostNotificationsRemove.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostNotificationsRemove.Responses.$204>
  }
  ['/api/permissions/add_group']: {
    /**
     * postPermissionsAddGroup - Add a permission to a group.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br /> The group name must be provided. <br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsAddGroup.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsAddGroup.Responses.$204>
  }
  ['/api/permissions/add_group_to_template']: {
    /**
     * postPermissionsAddGroupToTemplate - Add a group to a permission template.<br /> The group name must be provided. <br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsAddGroupToTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsAddGroupToTemplate.Responses.$204>
  }
  ['/api/permissions/add_project_creator_to_template']: {
    /**
     * postPermissionsAddProjectCreatorToTemplate - Add a project creator to a permission template.<br>Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsAddProjectCreatorToTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsAddProjectCreatorToTemplate.Responses.$204>
  }
  ['/api/permissions/add_user']: {
    /**
     * postPermissionsAddUser - Add permission to a user.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsAddUser.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsAddUser.Responses.$204>
  }
  ['/api/permissions/add_user_to_template']: {
    /**
     * postPermissionsAddUserToTemplate - Add a user to a permission template.<br /> Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsAddUserToTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsAddUserToTemplate.Responses.$204>
  }
  ['/api/permissions/apply_template']: {
    /**
     * postPermissionsApplyTemplate - Apply a permission template to one project.<br>The project id or project key must be provided.<br>The template id or name must be provided.<br>Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsApplyTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsApplyTemplate.Responses.$204>
  }
  ['/api/permissions/bulk_apply_template']: {
    /**
     * postPermissionsBulkApplyTemplate - Apply a permission template to several components. Managed projects will be ignored.<br />The template id or name must be provided.<br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsBulkApplyTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsBulkApplyTemplate.Responses.$204>
  }
  ['/api/permissions/create_template']: {
    /**
     * postPermissionsCreateTemplate - Create a permission template.<br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsCreateTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsCreateTemplate.Responses.$200>
  }
  ['/api/permissions/delete_template']: {
    /**
     * postPermissionsDeleteTemplate - Delete a permission template.<br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsDeleteTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsDeleteTemplate.Responses.$204>
  }
  ['/api/permissions/remove_group']: {
    /**
     * postPermissionsRemoveGroup - Remove a permission from a group.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br /> The group name must be provided.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsRemoveGroup.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsRemoveGroup.Responses.$204>
  }
  ['/api/permissions/remove_group_from_template']: {
    /**
     * postPermissionsRemoveGroupFromTemplate - Remove a group from a permission template.<br /> The group name must be provided. <br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsRemoveGroupFromTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsRemoveGroupFromTemplate.Responses.$204>
  }
  ['/api/permissions/remove_project_creator_from_template']: {
    /**
     * postPermissionsRemoveProjectCreatorFromTemplate - Remove a project creator from a permission template.<br>Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsRemoveProjectCreatorFromTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsRemoveProjectCreatorFromTemplate.Responses.$204>
  }
  ['/api/permissions/remove_user']: {
    /**
     * postPermissionsRemoveUser - Remove permission from a user.<br /> This service defaults to global permissions, but can be limited to project permissions by providing project id or project key.<br /> Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsRemoveUser.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsRemoveUser.Responses.$204>
  }
  ['/api/permissions/remove_user_from_template']: {
    /**
     * postPermissionsRemoveUserFromTemplate - Remove a user from a permission template.<br /> Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsRemoveUserFromTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsRemoveUserFromTemplate.Responses.$204>
  }
  ['/api/permissions/search_templates']: {
    /**
     * getPermissionsSearchTemplates - List permission templates.<br />Requires the following permission: 'Administer System'.
     */
    'get'(
      parameters?: Parameters<Paths.GetPermissionsSearchTemplates.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPermissionsSearchTemplates.Responses.$200>
  }
  ['/api/permissions/set_default_template']: {
    /**
     * postPermissionsSetDefaultTemplate - Set a permission template as default.<br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsSetDefaultTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsSetDefaultTemplate.Responses.$204>
  }
  ['/api/permissions/update_template']: {
    /**
     * postPermissionsUpdateTemplate - Update a permission template.<br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPermissionsUpdateTemplate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPermissionsUpdateTemplate.Responses.$200>
  }
  ['/api/plugins/available']: {
    /**
     * getPluginsAvailable - Get the list of all the plugins available for installation on the SonarQube instance, sorted by plugin name.<br/>Plugin information is retrieved from Update Center. Date and time at which Update Center was last refreshed is provided in the response.<br/>Update status values are: <ul><li>COMPATIBLE: plugin is compatible with current SonarQube instance.</li><li>INCOMPATIBLE: plugin is not compatible with current SonarQube instance.</li><li>REQUIRES_SYSTEM_UPGRADE: plugin requires SonarQube to be upgraded before being installed.</li><li>DEPS_REQUIRE_SYSTEM_UPGRADE: at least one plugin on which the plugin is dependent requires SonarQube to be upgraded.</li></ul>Require 'Administer System' permission.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPluginsAvailable.Responses.$200>
  }
  ['/api/plugins/cancel_all']: {
    /**
     * postPluginsCancelAll - Cancels any operation pending on any plugin (install, update or uninstall)<br/>Requires user to be authenticated with Administer System permissions
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPluginsCancelAll.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPluginsCancelAll.Responses.$204>
  }
  ['/api/plugins/install']: {
    /**
     * postPluginsInstall - Installs the latest version of a plugin specified by its key.<br/>Plugin information is retrieved from Update Center.<br/>Fails if used on commercial editions or plugin risk consent has not been accepted.<br/>Requires user to be authenticated with Administer System permissions
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPluginsInstall.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPluginsInstall.Responses.$204>
  }
  ['/api/plugins/installed']: {
    /**
     * getPluginsInstalled - Get the list of all the plugins installed on the SonarQube instance, sorted by plugin name.<br/>Requires authentication.
     */
    'get'(
      parameters?: Parameters<Paths.GetPluginsInstalled.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPluginsInstalled.Responses.$200>
  }
  ['/api/plugins/pending']: {
    /**
     * getPluginsPending - Get the list of plugins which will either be installed or removed at the next startup of the SonarQube instance, sorted by plugin name.<br/>Require 'Administer System' permission.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPluginsPending.Responses.$200>
  }
  ['/api/plugins/uninstall']: {
    /**
     * postPluginsUninstall - Uninstalls the plugin specified by its key.<br/>Requires user to be authenticated with Administer System permissions.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPluginsUninstall.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPluginsUninstall.Responses.$204>
  }
  ['/api/plugins/update']: {
    /**
     * postPluginsUpdate - Updates a plugin specified by its key to the latest version compatible with the SonarQube instance.<br/>Plugin information is retrieved from Update Center.<br/>Requires user to be authenticated with Administer System permissions
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostPluginsUpdate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostPluginsUpdate.Responses.$204>
  }
  ['/api/plugins/updates']: {
    /**
     * getPluginsUpdates - Lists plugins installed on the SonarQube instance for which at least one newer version is available, sorted by plugin name.<br/>Each newer version is listed, ordered from the oldest to the newest, with its own update/compatibility status.<br/>Plugin information is retrieved from Update Center. Date and time at which Update Center was last refreshed is provided in the response.<br/>Update status values are: [COMPATIBLE, INCOMPATIBLE, REQUIRES_UPGRADE, DEPS_REQUIRE_UPGRADE].<br/>Require 'Administer System' permission.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetPluginsUpdates.Responses.$200>
  }
  ['/api/project_analyses/create_event']: {
    /**
     * postProjectAnalysesCreateEvent - Create a project analysis event.<br>Only event of category 'VERSION' and 'OTHER' can be created.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectAnalysesCreateEvent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectAnalysesCreateEvent.Responses.$200>
  }
  ['/api/project_analyses/delete']: {
    /**
     * postProjectAnalysesDelete - Delete a project analysis.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the project of the specified analysis</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectAnalysesDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectAnalysesDelete.Responses.$204>
  }
  ['/api/project_analyses/delete_event']: {
    /**
     * postProjectAnalysesDeleteEvent - Delete a project analysis event.<br>Only event of category 'VERSION' and 'OTHER' can be deleted.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectAnalysesDeleteEvent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectAnalysesDeleteEvent.Responses.$204>
  }
  ['/api/project_analyses/search']: {
    /**
     * getProjectAnalysesSearch - Search a project analyses and attached events.<br>Requires the following permission: 'Browse' on the specified project. <br>For applications, it also requires 'Browse' permission on its child projects.
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectAnalysesSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetProjectAnalysesSearch.Responses.$200>
  }
  ['/api/project_analyses/update_event']: {
    /**
     * postProjectAnalysesUpdateEvent - Update a project analysis event.<br>Only events of category 'VERSION' and 'OTHER' can be updated.<br>Requires one of the following permissions:<ul>  <li>'Administer System'</li>  <li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectAnalysesUpdateEvent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectAnalysesUpdateEvent.Responses.$200>
  }
  ['/api/project_badges/measure']: {
    /**
     * getProjectBadgesMeasure - Generate badge for project's measure as an SVG.<br/>Requires 'Browse' permission on the specified project.
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectBadgesMeasure.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/project_badges/quality_gate']: {
    /**
     * getProjectBadgesQualityGate - Generate badge for project's quality gate as an SVG.<br/>Requires 'Browse' permission on the specified project.
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectBadgesQualityGate.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/project_badges/renew_token']: {
    /**
     * postProjectBadgesRenewToken - Creates new token replacing any existing token for project or application badge access for private projects and applications.<br/>This token can be used to authenticate with api/project_badges/quality_gate and api/project_badges/measure endpoints.<br/>Requires 'Administer' permission on the specified project or application.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectBadgesRenewToken.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectBadgesRenewToken.Responses.$204>
  }
  ['/api/project_badges/token']: {
    /**
     * getProjectBadgesToken - Retrieve a token to use for project or application badge access for private projects or applications.<br/>This token can be used to authenticate with api/project_badges/quality_gate and api/project_badges/measure endpoints.<br/>Requires 'Browse' permission on the specified project or application.
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectBadgesToken.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetProjectBadgesToken.Responses.$200>
  }
  ['/api/project_branches/delete']: {
    /**
     * postProjectBranchesDelete - Delete a non-main branch of a project or application.<br/>Requires 'Administer' rights on the specified project or application.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectBranchesDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectBranchesDelete.Responses.$204>
  }
  ['/api/project_branches/list']: {
    /**
     * getProjectBranchesList - List the branches of a project or application.<br/>Requires 'Browse' or 'Execute analysis' rights on the specified project or application.
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectBranchesList.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetProjectBranchesList.Responses.$200>
  }
  ['/api/project_branches/rename']: {
    /**
     * postProjectBranchesRename - Rename the main branch of a project or application.<br/>Requires 'Administer' permission on the specified project or application.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectBranchesRename.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectBranchesRename.Responses.$204>
  }
  ['/api/project_branches/set_automatic_deletion_protection']: {
    /**
     * postProjectBranchesSetAutomaticDeletionProtection - Protect a specific branch from automatic deletion. Protection can't be disabled for the main branch.<br/>Requires 'Administer' permission on the specified project or application.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectBranchesSetAutomaticDeletionProtection.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectBranchesSetAutomaticDeletionProtection.Responses.$204>
  }
  ['/api/project_branches/set_main']: {
    /**
     * postProjectBranchesSetMain - Allow to set a new main branch.<br/>. Caution, only applicable on projects.<br>Requires 'Administer' rights on the specified project or application.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectBranchesSetMain.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectBranchesSetMain.Responses.$204>
  }
  ['/api/project_dump/export']: {
    /**
     * postProjectDumpExport - Triggers project dump so that the project can be imported to another SonarQube server (see api/project_dump/import, available in Enterprise Edition). Requires the 'Administer' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectDumpExport.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectDumpExport.Responses.$200>
  }
  ['/api/project_links/create']: {
    /**
     * postProjectLinksCreate - Create a new project link.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectLinksCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectLinksCreate.Responses.$200>
  }
  ['/api/project_links/delete']: {
    /**
     * postProjectLinksDelete - Delete existing project link.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectLinksDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectLinksDelete.Responses.$204>
  }
  ['/api/project_links/search']: {
    /**
     * getProjectLinksSearch - List links of a project.<br>The 'projectId' or 'projectKey' must be provided.<br>Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li><li>'Browse' on the specified project</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectLinksSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetProjectLinksSearch.Responses.$200>
  }
  ['/api/project_tags/search']: {
    /**
     * getProjectTagsSearch - Search tags
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectTagsSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetProjectTagsSearch.Responses.$200>
  }
  ['/api/project_tags/set']: {
    /**
     * postProjectTagsSet - Set tags on a project.<br>Requires the following permission: 'Administer' rights on the specified project
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectTagsSet.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectTagsSet.Responses.$204>
  }
  ['/api/projects/bulk_delete']: {
    /**
     * postProjectsBulkDelete - Delete one or several projects.<br />Only the 1'000 first items in project filters are taken into account.<br />Requires 'Administer System' permission.<br />At least one parameter is required among analyzedBefore, projects and q
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectsBulkDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectsBulkDelete.Responses.$204>
  }
  ['/api/projects/create']: {
    /**
     * postProjectsCreate - Create a project.<br/>If your project is hosted on a DevOps Platform, please use the import endpoint under api/alm_integrations, so it creates and properly configures the project.Requires 'Create Projects' permission.<br/>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectsCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectsCreate.Responses.$200>
  }
  ['/api/projects/delete']: {
    /**
     * postProjectsDelete - Delete a project.<br> Requires 'Administer System' permission or 'Administer' permission on the project.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectsDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectsDelete.Responses.$204>
  }
  ['/api/projects/search']: {
    /**
     * getProjectsSearch - Search for projects or views to administrate them.
     * <ul>
     *   <li>The response field 'lastAnalysisDate' takes into account the analysis of all branches and pull requests, not only the main branch.</li>
     *   <li>The response field 'revision' takes into account the analysis of the main branch only.</li>
     * </ul>
     * Requires 'Administer System' permission
     */
    'get'(
      parameters?: Parameters<Paths.GetProjectsSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetProjectsSearch.Responses.$200>
  }
  ['/api/projects/update_key']: {
    /**
     * postProjectsUpdateKey - Update a project all its sub-components keys.<br>Requires one of the following permissions: <ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectsUpdateKey.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectsUpdateKey.Responses.$204>
  }
  ['/api/projects/update_visibility']: {
    /**
     * postProjectsUpdateVisibility - Updates visibility of a project, application or a portfolio.<br>Requires 'Project administer' permission on the specified entity
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostProjectsUpdateVisibility.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostProjectsUpdateVisibility.Responses.$204>
  }
  ['/api/qualitygates/add_group']: {
    /**
     * postQualitygatesAddGroup - Allow a group of users to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesAddGroup.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesAddGroup.Responses.$204>
  }
  ['/api/qualitygates/add_user']: {
    /**
     * postQualitygatesAddUser - Allow a user to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesAddUser.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesAddUser.Responses.$204>
  }
  ['/api/qualitygates/copy']: {
    /**
     * postQualitygatesCopy - Copy a Quality Gate.<br>'sourceName' must be provided. Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesCopy.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesCopy.Responses.$204>
  }
  ['/api/qualitygates/create']: {
    /**
     * postQualitygatesCreate - Create a Quality Gate.<br>Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesCreate.Responses.$200>
  }
  ['/api/qualitygates/create_condition']: {
    /**
     * postQualitygatesCreateCondition - Add a new condition to a quality gate.<br>Parameter 'gateName' must be provided. Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesCreateCondition.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesCreateCondition.Responses.$200>
  }
  ['/api/qualitygates/delete_condition']: {
    /**
     * postQualitygatesDeleteCondition - Delete a condition from a quality gate.<br>Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesDeleteCondition.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesDeleteCondition.Responses.$204>
  }
  ['/api/qualitygates/deselect']: {
    /**
     * postQualitygatesDeselect - Remove the association of a project from a quality gate.<br>Requires one of the following permissions:<ul><li>'Administer Quality Gates'</li><li>'Administer' rights on the project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesDeselect.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesDeselect.Responses.$204>
  }
  ['/api/qualitygates/destroy']: {
    /**
     * postQualitygatesDestroy - Delete a Quality Gate.<br>Parameter 'name' must be specified. Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesDestroy.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesDestroy.Responses.$204>
  }
  ['/api/qualitygates/get_by_project']: {
    /**
     * getQualitygatesGetByProject - Get the quality gate of a project.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li><li>'Browse' on the specified project</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetQualitygatesGetByProject.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualitygatesGetByProject.Responses.$200>
  }
  ['/api/qualitygates/list']: {
    /**
     * getQualitygatesList - Get a list of quality gates
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualitygatesList.Responses.$200>
  }
  ['/api/qualitygates/project_status']: {
    /**
     * getQualitygatesProjectStatus - Get the quality gate status of a project or a Compute Engine task.<br />Either 'analysisId', 'projectId' or 'projectKey' must be provided <br />The different statuses returned are: OK, WARN, ERROR, NONE. The NONE status is returned when there is no quality gate associated with the analysis.<br />Returns an HTTP code 404 if the analysis associated with the task is not found or does not exist.<br />Requires one of the following permissions:<ul><li>'Administer System'</li><li>'Administer' rights on the specified project</li><li>'Browse' on the specified project</li><li>'Execute Analysis' on the specified project</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetQualitygatesProjectStatus.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualitygatesProjectStatus.Responses.$200>
  }
  ['/api/qualitygates/remove_group']: {
    /**
     * postQualitygatesRemoveGroup - Remove the ability from a group to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesRemoveGroup.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesRemoveGroup.Responses.$204>
  }
  ['/api/qualitygates/remove_user']: {
    /**
     * postQualitygatesRemoveUser - Remove the ability from an user to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesRemoveUser.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesRemoveUser.Responses.$204>
  }
  ['/api/qualitygates/rename']: {
    /**
     * postQualitygatesRename - Rename a Quality Gate.<br>'currentName' must be specified. Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesRename.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesRename.Responses.$204>
  }
  ['/api/qualitygates/search']: {
    /**
     * getQualitygatesSearch - Search for projects associated (or not) to a quality gate.<br/>Only authorized projects for the current user will be returned.
     */
    'get'(
      parameters?: Parameters<Paths.GetQualitygatesSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualitygatesSearch.Responses.$200>
  }
  ['/api/qualitygates/search_groups']: {
    /**
     * getQualitygatesSearchGroups - List the groups that are allowed to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetQualitygatesSearchGroups.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualitygatesSearchGroups.Responses.$200>
  }
  ['/api/qualitygates/search_users']: {
    /**
     * getQualitygatesSearchUsers - List the users that are allowed to edit a Quality Gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>Edit right on the specified quality gate</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetQualitygatesSearchUsers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualitygatesSearchUsers.Responses.$200>
  }
  ['/api/qualitygates/select']: {
    /**
     * postQualitygatesSelect - Associate a project to a quality gate.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Gates'</li>  <li>'Administer' right on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesSelect.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesSelect.Responses.$204>
  }
  ['/api/qualitygates/set_as_default']: {
    /**
     * postQualitygatesSetAsDefault - Set a quality gate as the default quality gate.<br>Parameter 'name' must be specified. Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesSetAsDefault.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesSetAsDefault.Responses.$204>
  }
  ['/api/qualitygates/show']: {
    /**
     * getQualitygatesShow - Display the details of a quality gate
     */
    'get'(
      parameters?: Parameters<Paths.GetQualitygatesShow.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualitygatesShow.Responses.$200>
  }
  ['/api/qualitygates/update_condition']: {
    /**
     * postQualitygatesUpdateCondition - Update a condition attached to a quality gate.<br>Requires the 'Administer Quality Gates' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualitygatesUpdateCondition.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualitygatesUpdateCondition.Responses.$204>
  }
  ['/api/qualityprofiles/activate_rule']: {
    /**
     * postQualityprofilesActivateRule - Activate a rule on a Quality Profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesActivateRule.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesActivateRule.Responses.$204>
  }
  ['/api/qualityprofiles/activate_rules']: {
    /**
     * postQualityprofilesActivateRules - Bulk-activate rules on one quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesActivateRules.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesActivateRules.Responses.$204>
  }
  ['/api/qualityprofiles/add_project']: {
    /**
     * postQualityprofilesAddProject - Associate a project with a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Administer right on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesAddProject.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesAddProject.Responses.$204>
  }
  ['/api/qualityprofiles/backup']: {
    /**
     * getQualityprofilesBackup - Backup a quality profile in XML form. The exported profile can be restored through api/qualityprofiles/restore.
     */
    'get'(
      parameters?: Parameters<Paths.GetQualityprofilesBackup.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/qualityprofiles/change_parent']: {
    /**
     * postQualityprofilesChangeParent - Change a quality profile's parent.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesChangeParent.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesChangeParent.Responses.$204>
  }
  ['/api/qualityprofiles/changelog']: {
    /**
     * getQualityprofilesChangelog - Get the history of changes on a quality profile: rule activation/deactivation, change in parameters/severity. Events are ordered by date in descending order (most recent first).
     */
    'get'(
      parameters?: Parameters<Paths.GetQualityprofilesChangelog.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualityprofilesChangelog.Responses.$200>
  }
  ['/api/qualityprofiles/copy']: {
    /**
     * postQualityprofilesCopy - Copy a quality profile.<br> Requires to be logged in and the 'Administer Quality Profiles' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesCopy.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesCopy.Responses.$200>
  }
  ['/api/qualityprofiles/create']: {
    /**
     * postQualityprofilesCreate - Create a quality profile.<br>Requires to be logged in and the 'Administer Quality Profiles' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesCreate.Responses.$200>
  }
  ['/api/qualityprofiles/deactivate_rule']: {
    /**
     * postQualityprofilesDeactivateRule - Deactivate a rule on a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesDeactivateRule.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesDeactivateRule.Responses.$204>
  }
  ['/api/qualityprofiles/deactivate_rules']: {
    /**
     * postQualityprofilesDeactivateRules - Bulk deactivate rules on Quality profiles.<br>Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesDeactivateRules.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesDeactivateRules.Responses.$204>
  }
  ['/api/qualityprofiles/delete']: {
    /**
     * postQualityprofilesDelete - Delete a quality profile and all its descendants. The default quality profile cannot be deleted.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesDelete.Responses.$204>
  }
  ['/api/qualityprofiles/export']: {
    /**
     * getQualityprofilesExport - Export a quality profile.
     */
    'get'(
      parameters?: Parameters<Paths.GetQualityprofilesExport.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/qualityprofiles/exporters']: {
    /**
     * getQualityprofilesExporters - Lists available profile export formats.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualityprofilesExporters.Responses.$200>
  }
  ['/api/qualityprofiles/importers']: {
    /**
     * getQualityprofilesImporters - List supported importers.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualityprofilesImporters.Responses.$200>
  }
  ['/api/qualityprofiles/inheritance']: {
    /**
     * getQualityprofilesInheritance - Show a quality profile's ancestors and children.
     */
    'get'(
      parameters?: Parameters<Paths.GetQualityprofilesInheritance.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualityprofilesInheritance.Responses.$200>
  }
  ['/api/qualityprofiles/projects']: {
    /**
     * getQualityprofilesProjects - List projects with their association status regarding a quality profile. <br/>Only projects explicitly bound to the profile are returned, those associated with the profile because it is the default one are not. <br/>See api/qualityprofiles/search in order to get the Quality Profile Key. 
     */
    'get'(
      parameters?: Parameters<Paths.GetQualityprofilesProjects.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualityprofilesProjects.Responses.$200>
  }
  ['/api/qualityprofiles/remove_project']: {
    /**
     * postQualityprofilesRemoveProject - Remove a project's association with a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li>  <li>Administer right on the specified project</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesRemoveProject.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesRemoveProject.Responses.$204>
  }
  ['/api/qualityprofiles/rename']: {
    /**
     * postQualityprofilesRename - Rename a quality profile.<br> Requires one of the following permissions:<ul>  <li>'Administer Quality Profiles'</li>  <li>Edit right on the specified quality profile</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesRename.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesRename.Responses.$204>
  }
  ['/api/qualityprofiles/restore']: {
    /**
     * postQualityprofilesRestore - Restore a quality profile using an XML file. The restored profile name is taken from the backup file, so if a profile with the same name and language already exists, it will be overwritten.<br> Requires to be logged in and the 'Administer Quality Profiles' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesRestore.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesRestore.Responses.$204>
  }
  ['/api/qualityprofiles/search']: {
    /**
     * getQualityprofilesSearch - Search quality profiles
     */
    'get'(
      parameters?: Parameters<Paths.GetQualityprofilesSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetQualityprofilesSearch.Responses.$200>
  }
  ['/api/qualityprofiles/set_default']: {
    /**
     * postQualityprofilesSetDefault - Select the default profile for a given language.<br> Requires to be logged in and the 'Administer Quality Profiles' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostQualityprofilesSetDefault.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostQualityprofilesSetDefault.Responses.$204>
  }
  ['/api/rules/create']: {
    /**
     * postRulesCreate - Create a custom rule.<br>Requires the 'Administer Quality Profiles' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostRulesCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostRulesCreate.Responses.$200>
  }
  ['/api/rules/delete']: {
    /**
     * postRulesDelete - Delete custom rule.<br/>Requires the 'Administer Quality Profiles' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostRulesDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostRulesDelete.Responses.$204>
  }
  ['/api/rules/repositories']: {
    /**
     * getRulesRepositories - List available rule repositories
     */
    'get'(
      parameters?: Parameters<Paths.GetRulesRepositories.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRulesRepositories.Responses.$200>
  }
  ['/api/rules/search']: {
    /**
     * getRulesSearch - Search for a collection of relevant rules matching a specified query.<br/>
     */
    'get'(
      parameters?: Parameters<Paths.GetRulesSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRulesSearch.Responses.$200>
  }
  ['/api/rules/show']: {
    /**
     * getRulesShow - Get detailed information about a rule<br>
     */
    'get'(
      parameters?: Parameters<Paths.GetRulesShow.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRulesShow.Responses.$200>
  }
  ['/api/rules/tags']: {
    /**
     * getRulesTags - List rule tags
     */
    'get'(
      parameters?: Parameters<Paths.GetRulesTags.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetRulesTags.Responses.$200>
  }
  ['/api/rules/update']: {
    /**
     * postRulesUpdate - Update an existing rule.<br>Requires the 'Administer Quality Profiles' permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostRulesUpdate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostRulesUpdate.Responses.$200>
  }
  ['/api/server/version']: {
    /**
     * getServerVersion - Version of SonarQube in plain text
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/settings/list_definitions']: {
    /**
     * getSettingsListDefinitions - List settings definitions.<br>Requires 'Browse' permission when a component is specified<br/>To access licensed settings, authentication is required<br/>To access secured settings, one of the following permissions is required: <ul><li>'Execute Analysis'</li><li>'Administer System'</li><li>'Administer' rights on the specified component</li></ul>
     */
    'get'(
      parameters?: Parameters<Paths.GetSettingsListDefinitions.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSettingsListDefinitions.Responses.$200>
  }
  ['/api/settings/reset']: {
    /**
     * postSettingsReset - Remove a setting value.<br>The settings defined in conf/sonar.properties are read-only and can't be changed.<br/>Requires one of the following permissions: <ul><li>'Administer System'</li><li>'Administer' rights on the specified component</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostSettingsReset.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostSettingsReset.Responses.$204>
  }
  ['/api/settings/set']: {
    /**
     * postSettingsSet - Update a setting value.<br>Either 'value' or 'values' must be provided.<br> The settings defined in conf/sonar.properties are read-only and can't be changed.<br/>Requires one of the following permissions: <ul><li>'Administer System'</li><li>'Administer' rights on the specified component</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostSettingsSet.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostSettingsSet.Responses.$204>
  }
  ['/api/settings/values']: {
    /**
     * getSettingsValues - List settings values.<br>If no value has been set for a setting, then the default value is returned.<br>The settings from conf/sonar.properties are excluded from results.<br>Requires 'Browse' or 'Execute Analysis' permission when a component is specified.<br/>Secured settings values are not returned by the endpoint.<br/>
     */
    'get'(
      parameters?: Parameters<Paths.GetSettingsValues.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSettingsValues.Responses.$200>
  }
  ['/api/sources/raw']: {
    /**
     * getSourcesRaw - Get source code as raw text. Require 'See Source Code' permission on file
     */
    'get'(
      parameters?: Parameters<Paths.GetSourcesRaw.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/sources/scm']: {
    /**
     * getSourcesScm - Get SCM information of source files. Require See Source Code permission on file's project<br/>Each element of the result array is composed of:<ol><li>Line number</li><li>Author of the commit</li><li>Datetime of the commit (before 5.2 it was only the Date)</li><li>Revision of the commit (added in 5.2)</li></ol>
     */
    'get'(
      parameters?: Parameters<Paths.GetSourcesScm.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSourcesScm.Responses.$200>
  }
  ['/api/sources/show']: {
    /**
     * getSourcesShow - Get source code. Requires See Source Code permission on file's project<br/>Each element of the result array is composed of:<ol><li>Line number</li><li>Content of the line</li></ol>
     */
    'get'(
      parameters?: Parameters<Paths.GetSourcesShow.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSourcesShow.Responses.$200>
  }
  ['/api/system/change_log_level']: {
    /**
     * postSystemChangeLogLevel - Temporarily changes level of logs. New level is not persistent and is lost when restarting server. Requires system administration permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostSystemChangeLogLevel.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostSystemChangeLogLevel.Responses.$204>
  }
  ['/api/system/db_migration_status']: {
    /**
     * getSystemDbMigrationStatus - Display the database migration status of SonarQube.<br/>State values are:<ul><li>NO_MIGRATION: DB is up to date with current version of SonarQube.</li><li>NOT_SUPPORTED: Migration is not supported on embedded databases.</li><li>MIGRATION_RUNNING: DB migration is under go.</li><li>MIGRATION_SUCCEEDED: DB migration has run and has been successful.</li><li>MIGRATION_FAILED: DB migration has run and failed. SonarQube must be restarted in order to retry a DB migration (optionally after DB has been restored from backup).</li><li>MIGRATION_REQUIRED: DB migration is required.</li></ul>
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSystemDbMigrationStatus.Responses.$200>
  }
  ['/api/system/health']: {
    /**
     * getSystemHealth - Provide health status of SonarQube.<p>Although global health is calculated based on both application and search nodes, detailed information is returned only for application nodes.</p><p>  <ul> <li>GREEN: SonarQube is fully operational</li> <li>YELLOW: SonarQube is usable, but it needs attention in order to be fully operational</li> <li>RED: SonarQube is not operational</li> </ul></p><br>Requires the 'Administer System' permission or system passcode (see WEB_SYSTEM_PASS_CODE in sonar.properties).<br>When SonarQube is in safe mode (waiting or running a database upgrade), only the authentication with a system passcode is supported.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSystemHealth.Responses.$200>
  }
  ['/api/system/info']: {
    /**
     * getSystemInfo - Get detailed information about system configuration.<br/>Requires 'Administer' permissions.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSystemInfo.Responses.$200>
  }
  ['/api/system/logs']: {
    /**
     * getSystemLogs - Get system logs in plain-text format. Requires system administration permission.
     */
    'get'(
      parameters?: Parameters<Paths.GetSystemLogs.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/system/migrate_db']: {
    /**
     * postSystemMigrateDb - Migrate the database to match the current version of SonarQube.<br/>Sending a POST request to this URL starts the DB migration. It is strongly advised to <strong>make a database backup</strong> before invoking this WS.<br/>State values are:<ul><li>NO_MIGRATION: DB is up to date with current version of SonarQube.</li><li>NOT_SUPPORTED: Migration is not supported on embedded databases.</li><li>MIGRATION_RUNNING: DB migration is under go.</li><li>MIGRATION_SUCCEEDED: DB migration has run and has been successful.</li><li>MIGRATION_FAILED: DB migration has run and failed. SonarQube must be restarted in order to retry a DB migration (optionally after DB has been restored from backup).</li><li>MIGRATION_REQUIRED: DB migration is required.</li></ul>
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostSystemMigrateDb.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostSystemMigrateDb.Responses.$200>
  }
  ['/api/system/ping']: {
    /**
     * getSystemPing - Answers "pong" as plain-text
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<any>
  }
  ['/api/system/restart']: {
    /**
     * postSystemRestart - Restarts server. Requires 'Administer System' permission. Performs a full restart of the Web, Search and Compute Engine Servers processes. Does not reload sonar.properties.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostSystemRestart.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostSystemRestart.Responses.$204>
  }
  ['/api/system/status']: {
    /**
     * getSystemStatus - Get state information about SonarQube.<p>status: the running status <ul> <li>STARTING: SonarQube Web Server is up and serving some Web Services (eg. api/system/status) but initialization is still ongoing</li> <li>UP: SonarQube instance is up and running</li> <li>DOWN: SonarQube instance is up but not running because migration has failed (refer to WS /api/system/migrate_db for details) or some other reason (check logs).</li> <li>RESTARTING: SonarQube instance is still up but a restart has been requested (refer to WS /api/system/restart for details).</li> <li>DB_MIGRATION_NEEDED: database migration is required. DB migration can be started using WS /api/system/migrate_db.</li> <li>DB_MIGRATION_RUNNING: DB migration is running (refer to WS /api/system/migrate_db for details)</li> </ul></p>
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSystemStatus.Responses.$200>
  }
  ['/api/system/upgrades']: {
    /**
     * getSystemUpgrades - Lists available upgrades for the SonarQube instance (if any) and for each one, lists incompatible plugins and plugins requiring upgrade.<br/>Plugin information is retrieved from Update Center. Date and time at which Update Center was last refreshed is provided in the response.
     */
    'get'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetSystemUpgrades.Responses.$200>
  }
  ['/api/user_groups/add_user']: {
    /**
     * postUserGroupsAddUser - Add a user to a group.<br />'name' must be provided.<br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUserGroupsAddUser.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUserGroupsAddUser.Responses.$204>
  }
  ['/api/user_groups/create']: {
    /**
     * postUserGroupsCreate - Create a group.<br>Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUserGroupsCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUserGroupsCreate.Responses.$200>
  }
  ['/api/user_groups/delete']: {
    /**
     * postUserGroupsDelete - Delete a group. The default groups cannot be deleted.<br/>'name' must be provided.<br />Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUserGroupsDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUserGroupsDelete.Responses.$204>
  }
  ['/api/user_groups/remove_user']: {
    /**
     * postUserGroupsRemoveUser - Remove a user from a group.<br />'name' must be provided.<br>Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUserGroupsRemoveUser.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUserGroupsRemoveUser.Responses.$204>
  }
  ['/api/user_groups/search']: {
    /**
     * getUserGroupsSearch - Search for user groups.<br>Requires the following permission: 'Administer System'.
     */
    'get'(
      parameters?: Parameters<Paths.GetUserGroupsSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUserGroupsSearch.Responses.$200>
  }
  ['/api/user_groups/update']: {
    /**
     * postUserGroupsUpdate - Update a group.<br>Requires the following permission: 'Administer System'.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUserGroupsUpdate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUserGroupsUpdate.Responses.$204>
  }
  ['/api/user_groups/users']: {
    /**
     * getUserGroupsUsers - Search for users with membership information with respect to a group.<br>Requires the following permission: 'Administer System'.
     */
    'get'(
      parameters?: Parameters<Paths.GetUserGroupsUsers.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUserGroupsUsers.Responses.$200>
  }
  ['/api/user_tokens/generate']: {
    /**
     * postUserTokensGenerate - Generate a user access token. <br />Please keep your tokens secret. They enable to authenticate and analyze projects.<br />It requires administration permissions to specify a 'login' and generate a token for another user. Otherwise, a token is generated for the current user.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUserTokensGenerate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUserTokensGenerate.Responses.$200>
  }
  ['/api/user_tokens/revoke']: {
    /**
     * postUserTokensRevoke - Revoke a user access token. <br/>It requires administration permissions to specify a 'login' and revoke a token for another user. Otherwise, the token for the current user is revoked.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUserTokensRevoke.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUserTokensRevoke.Responses.$204>
  }
  ['/api/user_tokens/search']: {
    /**
     * getUserTokensSearch - List the access tokens of a user.<br>The login must exist and active.<br>Field 'lastConnectionDate' is only updated every hour, so it may not be accurate, for instance when a user is using a token many times in less than one hour.<br> It requires administration permissions to specify a 'login' and list the tokens of another user. Otherwise, tokens for the current user are listed. <br> Authentication is required for this API endpoint
     */
    'get'(
      parameters?: Parameters<Paths.GetUserTokensSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUserTokensSearch.Responses.$200>
  }
  ['/api/users/anonymize']: {
    /**
     * postUsersAnonymize - Anonymize a deactivated user. Requires Administer System permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUsersAnonymize.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUsersAnonymize.Responses.$204>
  }
  ['/api/users/change_password']: {
    /**
     * postUsersChangePassword - Update a user's password. Authenticated users can change their own password, provided that the account is not linked to an external authentication system. Administer System permission is required to change another user's password.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUsersChangePassword.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUsersChangePassword.Responses.$204>
  }
  ['/api/users/create']: {
    /**
     * postUsersCreate - Create a user.<br/>If a deactivated user account exists with the given login, it will be reactivated.<br/>Requires Administer System permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUsersCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUsersCreate.Responses.$200>
  }
  ['/api/users/deactivate']: {
    /**
     * postUsersDeactivate - Deactivate a user. Requires Administer System permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUsersDeactivate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUsersDeactivate.Responses.$200>
  }
  ['/api/users/groups']: {
    /**
     * getUsersGroups - Lists the groups a user belongs to. <br/>Requires Administer System permission.
     */
    'get'(
      parameters?: Parameters<Paths.GetUsersGroups.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUsersGroups.Responses.$200>
  }
  ['/api/users/search']: {
    /**
     * getUsersSearch - Get a list of users. By default, only active users are returned.<br/>The following fields are only returned when user has Administer System permission or for logged-in in user :<ul>   <li>'email'</li>   <li>'externalIdentity'</li>   <li>'externalProvider'</li>   <li>'groups'</li>   <li>'lastConnectionDate'</li>   <li>'sonarLintLastConnectionDate'</li>   <li>'tokensCount'</li></ul>Field 'lastConnectionDate' is only updated every hour, so it may not be accurate, for instance when a user authenticates many times in less than one hour.
     */
    'get'(
      parameters?: Parameters<Paths.GetUsersSearch.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetUsersSearch.Responses.$200>
  }
  ['/api/users/update']: {
    /**
     * postUsersUpdate - Update a user.<br/>Requires Administer System permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUsersUpdate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUsersUpdate.Responses.$200>
  }
  ['/api/users/update_identity_provider']: {
    /**
     * postUsersUpdateIdentityProvider - Update identity provider information. <br/>It's only possible to migrate to an installed identity provider. Be careful that as soon as this information has been updated for a user, the user will only be able to authenticate on the new identity provider. It is not possible to migrate external user to local one.<br/>Requires Administer System permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUsersUpdateIdentityProvider.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUsersUpdateIdentityProvider.Responses.$204>
  }
  ['/api/users/update_login']: {
    /**
     * postUsersUpdateLogin - Update a user login. A login can be updated many times.<br/>Requires Administer System permission
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostUsersUpdateLogin.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostUsersUpdateLogin.Responses.$204>
  }
  ['/api/webhooks/create']: {
    /**
     * postWebhooksCreate - Create a Webhook.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostWebhooksCreate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostWebhooksCreate.Responses.$200>
  }
  ['/api/webhooks/delete']: {
    /**
     * postWebhooksDelete - Delete a Webhook.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostWebhooksDelete.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostWebhooksDelete.Responses.$204>
  }
  ['/api/webhooks/deliveries']: {
    /**
     * getWebhooksDeliveries - Get the recent deliveries for a specified project or Compute Engine task.<br/>Require 'Administer' permission on the related project.<br/>Note that additional information are returned by api/webhooks/delivery.
     */
    'get'(
      parameters?: Parameters<Paths.GetWebhooksDeliveries.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWebhooksDeliveries.Responses.$200>
  }
  ['/api/webhooks/delivery']: {
    /**
     * getWebhooksDelivery - Get a webhook delivery by its id.<br/>Require 'Administer System' permission.<br/>Note that additional information are returned by api/webhooks/delivery.
     */
    'get'(
      parameters?: Parameters<Paths.GetWebhooksDelivery.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWebhooksDelivery.Responses.$200>
  }
  ['/api/webhooks/list']: {
    /**
     * getWebhooksList - Search for global webhooks or project webhooks. Webhooks are ordered by name.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
     */
    'get'(
      parameters?: Parameters<Paths.GetWebhooksList.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWebhooksList.Responses.$200>
  }
  ['/api/webhooks/update']: {
    /**
     * postWebhooksUpdate - Update a Webhook.<br>Requires 'Administer' permission on the specified project, or global 'Administer' permission.
     */
    'post'(
      parameters?: Parameters<UnknownParamsObject> | null,
      data?: Paths.PostWebhooksUpdate.RequestBody,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.PostWebhooksUpdate.Responses.$204>
  }
  ['/api/webservices/list']: {
    /**
     * getWebservicesList - List web services
     */
    'get'(
      parameters?: Parameters<Paths.GetWebservicesList.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWebservicesList.Responses.$200>
  }
  ['/api/webservices/response_example']: {
    /**
     * getWebservicesResponseExample - Display web service response example
     */
    'get'(
      parameters?: Parameters<Paths.GetWebservicesResponseExample.QueryParameters> | null,
      data?: any,
      config?: AxiosRequestConfig  
    ): OperationResponse<Paths.GetWebservicesResponseExample.Responses.$200>
  }
}

export type Client = OpenAPIClient<OperationMethods, PathsDictionary>
