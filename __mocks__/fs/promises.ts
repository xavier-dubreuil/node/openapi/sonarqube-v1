import { Stats } from 'fs';

let stats: Record<string, Stats> = {};
let files: Record<string, string> = {};

export function __addStats (path: string, stat: Stats): void {
  stats[path] = stat;
}

export function __addFile (path: string, content: string): void {
  files[path] = content;
}

export function __getFile (path: string): string | null {
  return files[path] ?? null;
}

export async function mkdir (path: string): Promise<void> {
  const stat = new Stats();
  stat.isDirectory = () => true ;
  stats[path] = stat;
}

export async function writeFile (path: string, content: string): Promise<void> {
  return new Promise(resolve => {
    files[path] = content;
    resolve();
  });
}

export async function readFile (path: string): Promise<Buffer> {
  return new Promise(resolve => {
    resolve(Buffer.from(files[path]));
  });
}

export async function stat (path: string): Promise<Stats> {
  return new Promise((resolve, reject) => {
    if (stats[path] !== undefined) {
      resolve(stats[path]);
      return;
    }
    reject();
  })
}

export function reset(): void {
  files = {};
  stats = {};
}
