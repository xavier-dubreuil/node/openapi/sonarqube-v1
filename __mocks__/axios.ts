import mockAxios, { AxiosMockQueueItem, AxiosMockRequestCriteria } from 'jest-mock-axios';
import {deepStrictEqual} from 'assert';
export default mockAxios;

export function getRequest (config: AxiosMockRequestCriteria): Promise<AxiosMockQueueItem> {
  return new Promise(resolve => {
    const timeout = () => {
      const last = mockAxios.lastReqGet();
      try {
        const req = {m: last.method, u: last.url, p: last.config?.params ?? {}};
        const con = {m: config.method ?? 'get', u: config.url ?? '', p: config.params ?? {}};
        deepStrictEqual(req, con);
        resolve(last);
      } catch {
        setTimeout(timeout, 500);
      }
    }
    setTimeout(timeout, 0);
  });
}
