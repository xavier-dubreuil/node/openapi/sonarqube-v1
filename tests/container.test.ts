import { expect, describe, it } from '@jest/globals';
import mockAxios from 'jest-mock-axios';
import { paramsSerializer } from '../src/container';
import * as sonarqube from '../src';

describe('Container', () => {
  it('ParamSerializer', async () => {
    const check = paramsSerializer({ key: ['val1', 'val2'] });
    expect(check).toEqual('key=val1%2Cval2');
  });
  it('call', async () => {
    const container = await sonarqube.v99.Container.new('http://localhost', 'token');
    const promise = container.call({ url: 'api/system/ping' });
    expect(mockAxios.request).toHaveBeenCalledWith({
      url: 'api/system/ping',
      auth: {
        password: '',
        username: 'token',
      },
      baseURL: 'http://localhost',
      paramsSerializer: paramsSerializer
    });
    mockAxios.mockResponse({ data: '' });
    await promise;
  });
});
