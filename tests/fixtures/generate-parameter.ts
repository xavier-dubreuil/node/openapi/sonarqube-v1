import { OpenAPIV3 as OpenAPI } from 'openapi-types'
import { Parameter } from '../../src/sonarqube/api';

interface Check {
  from: Parameter
  to: OpenAPI.ParameterObject
}

export default [
  {
    from: {
      internal: false,
      key: 'param',
      required: false,
    },
    to: {
      in: 'query',
      name: 'param',
      schema: {
        type: 'string',
      }
    }
  },
  {
    from: {
      internal: false,
      key: 'param',
      required: true,
      description: 'desc',
      deprecatedSince: 5.6,
      defaultValue: 'content',
      possibleValues: [42, 24],
      exampleValue: 34
    },
    to: {
      in: 'query',
      name: 'param',
      required: true,
      deprecated: true,
      description: 'desc',
      schema: {
        type: 'integer',
        enum: [42, 24],
        default: 'content',
      },
    }
  },
  {
    from: {
      internal: false,
      key: 'status',
      required: false,
      description: 'Comma-separated list of statuses',
      exampleValue: 'example',
    },
    to: {
      in: 'query',
      name: 'status',
      description: 'Comma-separated list of statuses',
      schema: {
        type: 'array',
        items: {
          type: 'string',
        }
      }
    }
  },
] as Check[];
