import { OpenAPIV3 as OpenAPI } from 'openapi-types'

interface Check {
  from: string | number | object | string[] | number[] | object[]
  to: OpenAPI.SchemaObject
}

export default [
  {
    from: 'string',
    to: {
      type: 'string'
    }
  },
  {
    from: 42,
    to: {
      type: 'integer'
    }
  },
  {
    from: ['val-1', 'val-2'],
    to: {
      type: 'array',
      items: {
        type: 'string',
      }
    }
  },
  {
    from: [42, 24],
    to: {
      type: 'array',
      items: {
        type: 'integer',
      }
    }
  },
  {
    from: { one: 'two', three: 4 },
    to: {
      type: 'object',
      properties: {
        one: {
          type: 'string'
        },
        three: {
          type: 'integer',
        }
      }
    }
  },
  {
    from: { one: 'two', three: 4, array: [{ five: 'six' }] },
    to: {
      type: 'object',
      properties: {
        one: {
          type: 'string'
        },
        three: {
          type: 'integer',
        },
        array: {
          type: 'array',
          items: {
            type: 'object',
            properties: {
              five: {
                type: 'string',
              }
            }
          }
        }
      }
    }
  },
] as Check[];
