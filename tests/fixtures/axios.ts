export const requests = [
  {
    request: {
      url: '/api/webservices/list',
    },
    response: {
      "webServices": [
        {
          "path": "api/system",
          "since": "8.2",
          "description": "System",
          "actions": [
            {
              "key": "status",
              "description": "status",
              "since": "8.5",
              "internal": false,
              "post": false,
              "hasResponseExample": true,
              "changelog": [],
              "params": [
                {
                  "key": "extended",
                  "description": "Extend response",
                  "required": true,
                  "internal": false
                }
              ]
            },
            {
              "key": "ping",
              "description": "ping",
              "since": "8.5",
              "internal": true,
              "post": false,
              "hasResponseExample": false,
              "changelog": [],
            },
            {
              "key": "update",
              "description": "ping",
              "since": "8.5",
              "internal": true,
              "post": true,
              "deprecatedSince": "5.2",
              "hasResponseExample": false,
              "changelog": [],
              "params": [
                {
                  "key": "reason",
                  "description": "Reason",
                  "required": true,
                  "internal": false
                }
              ]
            },
          ]
        }
      ]
    }
  },
  {
    request: {
      url: '/api/webservices/response_example',
      params: {
        action: 'status',
        controller: 'api/system'
      }
    },
    response: {
      format: 'json',
      example: JSON.stringify({
        "id": "20150504120436",
        "version": "5.1",
        "status": "UP"
      }),
    }
  },
];