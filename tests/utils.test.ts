import { expect, it } from '@jest/globals';
import { toCamel } from '../src/utils';

describe('Generator', () => {
  it('toCamel', () => {
    expect(toCamel('TO_CAMEL_CASE')).toEqual('toCamelCase');
    expect(toCamel('to_camel_case')).toEqual('toCamelCase');
    expect(toCamel('tO_caMel_Case')).toEqual('toCamelCase');
  });
});
