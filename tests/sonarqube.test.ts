import { jest, describe, expect, it, beforeEach } from '@jest/globals';
import mockAxios from 'jest-mock-axios';
import { SonarQube, Action, WebService } from '../src/sonarqube';
import * as mockFsPromise from '../__mocks__/fs/promises';

jest.mock('fs/promises');
const mockFs: typeof mockFsPromise = jest.requireMock('fs/promises');

beforeEach(() => {
  mockAxios.reset();
  mockFs.reset();
})

export const defaultCalledWith = {
  auth: { password: '', username: 'token' },
  baseURL: 'http://localhost',
  headers: { Accept: 'application/json' },
};

function instantiate (cache: string | null = null): SonarQube {
  return SonarQube.newFromToken('http://localhost', 'token', cache);
}

async function test (
  caller: (sonarqube: SonarQube) => Promise<unknown>,
  url: string | null,
  params: object,
  data: unknown,
  check: unknown
): Promise<void> {
  const sonarqube = instantiate(null);
  const promise = caller(sonarqube);
  if (url !== null) {
    expect(mockAxios.get).toHaveBeenCalledWith(url, { ...defaultCalledWith, ...params });
    mockAxios.mockResponse({ data });
  }
  const response = await promise;
  expect(response).toEqual(check);
}

describe('Sonarqube', () => {
  it ('newFromToken', () => {
    const sonarqube = SonarQube.newFromToken('', '');
    expect(sonarqube).toBeInstanceOf(SonarQube);
  })
  it ('newFromToken and cache', () => {
    const sonarqube = SonarQube.newFromToken('', '', '');
    expect(sonarqube).toBeInstanceOf(SonarQube);
  })
  it ('newFromPassword', () => {
    const sonarqube = SonarQube.newFromPassword('', '', '');
    expect(sonarqube).toBeInstanceOf(SonarQube);
  })
  it ('newFromPassword and cache', () => {
    const sonarqube = SonarQube.newFromPassword('', '', '', '');
    expect(sonarqube).toBeInstanceOf(SonarQube);
  })
  it('GetSystemInfo', async () => {
    const data = { System: { Version: '0.0.0' } }
    test(
      sonarqube => sonarqube.getSystemInfo(),
      '/api/system/info',
      {},
      data,
      data
    );
  });
  it('getWebServices', async () => {
    const webServices = [{ description: 'description', actions: [], path: 'api/system', since: '5.6' }];
    test(
      sonarqube => sonarqube.getWebServices(),
      '/api/webservices/list',
      { params: { 'include_internals': 'no' } },
      { webServices },
      webServices,
    );
  });
  it('getWebServices With Internals', async () => {
    const webServices = [{ description: 'description', actions: [], path: 'api/system', since: '5.6' }];
    test(
      sonarqube => sonarqube.getWebServices(true),
      '/api/webservices/list',
      { params: { 'include_internals': 'yes' } },
      { webServices },
      webServices,
    );
  });
  it('getExample', async () => {
    const action: Action = { key: 'info', hasResponseExample: true, internal: false, post: false, since: '', deprecatedSince: '' };
    const webService: WebService = { description: 'description', path: 'api/system', since: '5.6', actions: [action] };
    test(
      sonarqube => sonarqube.getExample(webService, action),
      '/api/webservices/response_example',
      { params: { controller: webService.path, action: action.key } },
      { format: 'json', example: '{}' },
      { format: 'json', example: '{}' }
    );
  });
  it('getExample Null', async () => {
    const action: Action = { key: 'test', hasResponseExample: false, internal: false, post: false, since: '', deprecatedSince: '' };
    const webService: WebService = { description: 'description', path: 'api/system', since: '5.6', actions: [action] };
    const sonarqube = instantiate();
    const response = await sonarqube.getExample(webService, action);
    expect(response).toBeNull();
  });
  it('Cache existent', async () => {
    const content = { content: 'test' };
    mockFs.__addFile('/cache/test.json', JSON.stringify(content));
    const sonarqube = instantiate('/cache')
    const response = await sonarqube.call('test', async () => {
      return {};
    })
    expect(response).toEqual(content);
  });
  it('Cache non existent', async () => {
    const content = { description: 'toto' };
    const sonarqube = instantiate(`/cache`);
    const response = await sonarqube.call('test', async () => {
      return content;
    });
    expect(response).toEqual(content);
    expect(mockFs.__getFile('/cache/test.json')).toEqual(JSON.stringify(content));
  });
});
