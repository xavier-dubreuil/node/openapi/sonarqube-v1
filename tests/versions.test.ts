import { expect, describe, it } from '@jest/globals';
import mockAxios from 'jest-mock-axios';
import * as sonarqube from '../src';
import { AxiosResponse } from 'axios';

const content = 'content';
const axiosRequest = {
  method: 'get',
  url: '/api/system/ping',
  data: undefined,
  headers: {},
  params: {}
};

async function test(promise: Promise<AxiosResponse<string>>): Promise<void> {
  expect(mockAxios.request).toHaveBeenCalledWith(axiosRequest);
  const response = { data: content };
  mockAxios.mockResponse(response);
  const { data } = await promise;
  expect(data).toEqual(content);
}

describe('Versions', () => {
  it('v9.9', async () => {
    const container = await sonarqube.v99.Container.new('http://localhost', 'token');
    test(container.client.getSystemPing() as Promise<AxiosResponse<string>>);
  });
  it('v10.0', async () => {
    const container = await sonarqube.v100.Container.new('http://localhost', 'token');
    test(container.client.getSystemPing() as Promise<AxiosResponse<string>>);
  });
  it('v10.1', async () => {
    const container = await sonarqube.v101.Container.new('http://localhost', 'token');
    test(container.client.getSystemPing() as Promise<AxiosResponse<string>>);
  });
  it('v10.2', async () => {
    const container = await sonarqube.v102.Container.new('http://localhost', 'token');
    test(container.client.getSystemPing() as Promise<AxiosResponse<string>>);
  });
  it('v10.3', async () => {
    const container = await sonarqube.v103.Container.new('http://localhost', 'token');
    test(container.client.getSystemPing() as Promise<AxiosResponse<string>>);
  });
  it('v10.4', async () => {
    const container = await sonarqube.v104.Container.new('http://localhost', 'token');
    test(container.client.getSystemPing() as Promise<AxiosResponse<string>>);
  });
});
