import { expect, it } from '@jest/globals';
import mockAxios from 'jest-mock-axios';
import { SonarQubeGenerator } from '../src/generator';
import { SonarQube } from '../src/sonarqube/sonarqube';
import { Action, Parameter, WebService } from '../src/sonarqube/api';
import { OpenAPIV3 } from 'openapi-types';
import { getRequest } from '../__mocks__/axios';

beforeEach(() => {
  mockAxios.reset();
})

afterEach(() => {
  mockAxios.reset();
})

export const defaultCalledWith = {
  auth: { password: '', username: '' },
  baseURL: '',
  headers: { Accept: 'application/json' },
};
async function instantiate (version: string): Promise<SonarQubeGenerator> {
  const sonarqube = SonarQube.newFromToken('', '');
  const promise = SonarQubeGenerator.new(sonarqube);
  const req = await getRequest({ method: 'get', url: '/api/system/info' });
  mockAxios.mockResponse({ data: { System: { Version: version } } }, req);
  return await promise;
}

describe('Generator', () => {
  it('instantiate', async () => {
    const generator = await instantiate('10.4.3.1234');
    const openapi = generator.getOpenApi();
    expect(openapi.info.version).toEqual('10.4')
  });
  it('generateParameter', async () => {
    const generator = await instantiate('10.4.3.1234');
    const parameter: Parameter = {
      key: 'env',
      deprecatedSince: '8.1',
      exampleValue: 'dev',
      possibleValues: ['production', 'dev'],
      defaultValue: 'production',
      internal: false,
      required: true,
      description: 'test',
    };
    const openapi = generator.generateParameter(parameter);
    expect(openapi.name).toEqual('env');
    expect(openapi.in).toEqual('query');
    const schema = openapi.schema as OpenAPIV3.SchemaObject;
    expect(schema.default).toEqual('production');
    expect(schema.type).toEqual('string');
  });
  it('generateParameter array', async () => {
    const generator = await instantiate('10.4.3.1234');
    const parameter: Parameter = {
      key: 'ids',
      exampleValue: '42',
      internal: false,
      required: true,
      description: 'Comma-separated list of ids'
    };
    const openapi = generator.generateParameters({ params: [parameter], internal: false, key: '', hasResponseExample: false, post: false, since: '8.2' });
    const param = openapi[0];
    expect(param.name).toEqual('ids');
    expect(param.in).toEqual('query');
    const schema = param.schema as OpenAPIV3.ArraySchemaObject;
    expect(schema.type).toEqual('array');
    const subSchema = schema.items as OpenAPIV3.SchemaObject;
    expect(subSchema.type).toEqual('number');
  });
  it('generate post', async () => {
    const generator = await instantiate('10.4.3.1234');
    const action: Action = {
      key: 'update',
      post: true,
      since: '8.1',
      internal: false,
      hasResponseExample: false,
      params: [{
        key: 'value',
        exampleValue: 'test',
        internal: false,
        required: true,
      }]
    };
    const webservice: WebService = {
      description: '',
      path: 'api/project',
      since: '8.1',
      actions: [action],
    }
    const promise = generator.generate();
    expect(mockAxios.get).toHaveBeenCalledWith('/api/webservices/list', { ...defaultCalledWith, params: { include_internals: "no" } });
    mockAxios.mockResponse({ data: { webServices: [webservice] } });
    const openapi = await promise;
    expect(openapi.info.version).toEqual('10.4');
    expect(openapi.components?.schemas).toHaveProperty('PostProjectUpdateBody');
    const postProjectUpdateBody = openapi.components?.schemas?.PostProjectUpdateBody as OpenAPIV3.SchemaObject;
    expect(postProjectUpdateBody.type).toEqual('object');
    expect(postProjectUpdateBody.properties).toHaveProperty('update');
    expect(openapi.paths).toHaveProperty('/api/project/update');
    const path = openapi.paths['/api/project/update'] as OpenAPIV3.PathItemObject;
    expect(path).toHaveProperty('post');
    const method = path.post as OpenAPIV3.OperationObject;
    expect(method.operationId).toEqual('postProjectUpdate');
    expect(method.responses).toHaveProperty('204');
  });
  it('generate delete', async () => {
    const generator = await instantiate('10.4.3.1234');
    const action: Action = {
      key: 'delete',
      post: true,
      since: '8.1',
      internal: false,
      hasResponseExample: false,
    };
    const webservice: WebService = {
      description: '',
      path: 'api/project',
      since: '8.1',
      actions: [action],
    }
    const promise = generator.generate();
    expect(mockAxios.get).toHaveBeenCalledWith('/api/webservices/list', { ...defaultCalledWith, params: { include_internals: "no" } });
    mockAxios.mockResponse({ data: { webServices: [webservice] } });
    const openapi = await promise;
    expect(openapi.info.version).toEqual('10.4');
    expect(openapi.components?.schemas).toHaveProperty('PostProjectUpdateBody');
    const postProjectUpdateBody = openapi.components?.schemas?.PostProjectUpdateBody as OpenAPIV3.SchemaObject;
    expect(postProjectUpdateBody.type).toEqual('object');
    expect(postProjectUpdateBody.properties).toHaveProperty('update');
    expect(openapi.paths).toHaveProperty('/api/project/update');
    const path = openapi.paths['/api/project/update'] as OpenAPIV3.PathItemObject;
    expect(path).toHaveProperty('post');
  });
  it('generate get', async () => {
    const generator = await instantiate('10.3.1.1234');
    const action: Action = {
      key: 'get',
      post: false,
      since: '8.1',
      internal: false,
      hasResponseExample: true,
      deprecatedSince: '9.1',
      description: 'Project list',
      params: [{
        key: 'id',
        exampleValue: '42',
        internal: false,
        required: true,
      }]
    };
    const webservice: WebService = {
      description: '',
      path: 'api/project',
      since: '8.1',
      actions: [action],
    }
    const example = { projects: [{ id: 23, name: 'name' }] };
    const promise = generator.generate();
    const reqWebservice = await getRequest({ method: 'get', url: '/api/webservices/list', params: { include_internals: "no" } });
    mockAxios.mockResponse({ data: { webServices: [webservice] } }, reqWebservice);
    const reqExample = await getRequest({ method: 'get', url: '/api/webservices/response_example', params: { controller: webservice.path, action: action.key } });
    mockAxios.mockResponse({ data: { format: 'json', example: JSON.stringify(example) } }, reqExample);
    const openapi = await promise;
    expect(openapi.info.version).toEqual('10.3');
    expect(openapi.paths).toHaveProperty('/api/project/get');
    const path = openapi.paths['/api/project/get'] as OpenAPIV3.PathItemObject;
    expect(path).toHaveProperty('get');
    const method = path.get as OpenAPIV3.OperationObject;
    expect(method.operationId).toEqual('getProjectGet');
    expect(method.responses).toHaveProperty('200');
  });
  it('generate list', async () => {
    const generator = await instantiate('10.3.1.1234');
    const action: Action = {
      key: 'list',
      post: false,
      since: '8.1',
      internal: false,
      hasResponseExample: true,
      deprecatedSince: '9.1',
      description: 'Project list',
    };
    const webservice: WebService = {
      description: '',
      path: 'api/project',
      since: '8.1',
      actions: [action],
    }
    const example = { projects: [{ id: 23, name: 'name' }] };
    const promise = generator.generate();
    const reqWebservice = await getRequest({ method: 'get', url: '/api/webservices/list', params: { include_internals: "no" } });
    mockAxios.mockResponse({ data: { webServices: [webservice] } }, reqWebservice);
    const reqExample = await getRequest({ method: 'get', url: '/api/webservices/response_example', params: { controller: webservice.path, action: action.key } });
    mockAxios.mockResponse({ data: { format: 'json', example: JSON.stringify(example) } }, reqExample);
    const openapi = await promise;
    expect(openapi.info.version).toEqual('10.3');
    expect(openapi.paths).toHaveProperty('/api/project/list');
    const path = openapi.paths['/api/project/list'] as OpenAPIV3.PathItemObject;
    expect(path).toHaveProperty('get');
    const method = path.get as OpenAPIV3.OperationObject;
    expect(method.operationId).toEqual('getProjectList');
    expect(method.responses).toHaveProperty('200');
  });
});
