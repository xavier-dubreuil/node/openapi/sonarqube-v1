for v in 9-9 10-0 10-1 10-2 10-3 10-4; do
  npx ts-node src/generator.ts $v
  npx openapicmd typegen assets/openapi-$v.json >src/versions/v$v/openapi.d.ts
  sed -i '' 's/declare/export declare/g' src/versions/v$v/openapi.d.ts
done
